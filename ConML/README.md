### ConML
This is an implementation of the constructivist machine learning paradigm. For more information, see:
> Schmid, Thomas. "Deconstructing the Final Frontier of Artificial Intelligence: Five Theses for a Constructivist Machine Learning." AAAI Spring Symposium: Combining Machine Learning with Knowledge Engineering. 2019.

## Next Steps

# Immediate
- ~~update usage example~~
- Deconstruction
  - ~~Write recursive DeleteDependentModels (for model disposal) EDIT: names purgefromKB~~
  - ~~make sure that existing temporalClusterSearch and Reconstruction can be reused~~
  - ~~Write 4(3) specialized Deconstruction methods: TΣZ, ΣZ, TΣ~~

# Next
- ~~Testing of Deconstruction and Interface again~~

# Long Term
- employ Lazy Iterators everywhere to reduce strain on RAM
- fix Deconstruction Krippendorff Class-Permutation Bug
- ~~save and load functionality for ParameterStruct~~
- ~~think about reasonable processes for Procedural Knowledge and add those~~ ==> out of project, gone to master thesis
- reproduce CFS and replace Replacement
- ~~DocStrings~~
- ~~move Krippendorff to own GitLab project, change dependencies to Distances.jl and clean up for registration~~
- thorough Testing and Profiling, optimize if necessary
- ~~serialize KnowledgeDomain similar to python version~~
- ~~TΣ (and TZ?) time matching tolerance parameter similar to that for full deconstruction~~
- Parameter to enable multiple matching ΣZ deconstructions at the same time, instead of one after another
- Lernblöcke haben im Log eine Relationship wenn sie aus vektoriellen Modellen bestehen, und ein Origin, wenn Sie aus der Dekonstruktion kommen.=> log anpassen

## Usage
...give some intro into the package
- for now, see usage in ~~base folder Readme~~ the Wiki
