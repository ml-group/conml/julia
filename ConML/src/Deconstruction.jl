#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConML.jl and
# may not behave as expected when used on it's own

#short cut if reconstruction failed (or anything before)
function deconstruct!(kb::KnowledgeBase, ::ParametersConML, ::Construct, ::Reconstruct, ::Nothing) #last thing is the output from Reconstruction
    # TODO may or may not log aborting here
    return kb
end

# helper to check if one models t is enclosed by the other or vice versa
@inline _tmatch(left::NTuple{2,T}, right::NTuple{2,T}) where {T} = (first(left) < first(right))===(last(left) > last(right))
@inline function _tmatch(left::NTuple{2,T}, right::NTuple{2,T}, tolerance::Float64) where {T}
    leftextend = (last(left)-first(left))*tolerance
    rightextend = (last(right)-first(right))*tolerance
    return ((first(right) >= first(left)-leftextend) && (last(right) <= last(left)+leftextend)) || ((first(left) >= first(right)-rightextend) && (last(left) <= last(right)+rightextend))
end

# queue purging function was here but this has been replaced by
# lazily checking if a models source models are still existent
# before deconstruction of a new model.
# The new approach is likely more efficient because purgequeues
# would need to check each model in the queue each time a model is
# purged, whereas the new approach does a few haskey(kb[...], sourcemodel)
# calls at every deconstruction attempt, but haskey is in O(n)

"""
    deconstruct!(KnowledgeBase, Parameters, Construct, Reconstruct, reconstructedmodels)

Deconstruction of a candidate model to insert into a KnowledgeBase at level 1 in the hierarchy.
Searches for related models in the KnowledgeBase and delegates specific deconstruction to full_decon / σz_decon / tσ_decon functions.
"""
function deconstruct!(kb::KnowledgeBase, par::ParametersConML, construction::Construct, reconstruction::Reconstruct, reconstructedmodels::AbstractVector{<:AbstractPragmaticModel})
    if par.verbose println("Started Deconstruction") end
    queues = Dict{Int, Vector{AbstractPragmaticModel}}()
    for i in 1:par.highestLevel
        queues[i] = Vector{AbstractPragmaticModel}()
    end
    append!(queues[1], reconstructedmodels)

    while length(kb.higherLevels) < par.highestLevel
        push!(kb.higherLevels, Dict{String,AbstractPragmaticModel}())
    end # makes sure that when fetching an empty level in the KB, no error is thrown

    for lvl in 1:par.highestLevel
        if par.verbose && !isempty(kb[lvl]) println("Started Deconstruction queue for level $(lvl)") end
        newmodels = queues[lvl]

        while !isempty(newmodels)
            model = popfirst!(newmodels)
            # check if the new models source models are still in the kb or if they
            # were removed in the mean time. If so, abort silently. This replaces
            # actively purging the deconstruction queues whenever a model is deleted
            if lvl>1 && !all(sourcemodel -> haskey(kb[lvl-1], sourcemodel), model.source)
                if par.verbose println("New model $(model.ID) has been purged because a souce model was removed from the knowledge base.") end
                break
            end
            if par.verbose println("Deconstructing model $(model.ID)") end
            # get related models and relations
            tσz_relatives = Vector{AbstractPragmaticModel}()
            tσ_relatives = Vector{AbstractPragmaticModel}()
            σz_relatives = Vector{AbstractPragmaticModel}()
            tz_relatives = Vector{AbstractPragmaticModel}()
            for oldmodel in values(kb[lvl])
                if model.Σ == oldmodel.Σ
                    if model.Z == oldmodel.Z
                        if _tmatch(model.T, oldmodel.T, par.FullTolerance) # check with tolerance for full
                            # full match
                            if par.verbose println("Found TΣZ Relationship: $(model.ID) => $(oldmodel.ID)") end
                            push!(tσz_relatives, oldmodel)
                            # TODO make user choice if ΣZ, TΣ and TZ should be tried on the same model if full fails
                        else
                            # ΣZ match
                            if par.verbose println("Found ΣZ Relationship: $(model.ID) => $(oldmodel.ID)") end
                            push!(σz_relatives, oldmodel)
                        end
                    else
                        if _tmatch(model.T, oldmodel.T, par.TΣTolerance) # check with separate tolerance for TΣ?
                            # TΣ match
                            if par.verbose println("Found TΣ Relationship: $(model.ID) => $(oldmodel.ID)") end
                            push!(tσ_relatives, oldmodel)
                        end
                    end
                else
                    if model.Z == oldmodel.Z
                        if _tmatch(model.T, oldmodel.T) # TZ checks without tolerance
                            # TZ match
                            # @warn "TZ related model identified, this was not expected."
                            if par.verbose println("Found TZ Relationship: $(model.ID) => $(oldmodel.ID)") end
                            push!(tz_relatives, oldmodel)
                        end
                    end
                end
            end
            # all related models identified, deconstruct consecutively
            global_success::Bool = false

            # TΣZ
            for rel in tσz_relatives
                success::Bool = full_decon(kb, par, model, rel, lvl, reconstruction, queues)
                global_success = global_success | success # can only switch to true, not back to false
                if success # if a full deconstruction was successful, don't do anything else with the consumed model
                    break # this inner loop
                end
            end
            if global_success # in a full deconstruction
                continue # to next new model
            end

            # ΣZ
            # NOTE: hier setzt FORCE_TIME_EXPANSION bei Thomas an
            # NOTE: sortieren nach ID
            for rel in σz_relatives
                success = σz_decon(kb, par, model, rel, lvl, construction, reconstruction, queues)
                global_success = global_success | success
                if success
                    break # this inner loop
                end
            end
            if global_success # in a SigmaZ deconstruction
                continue # to next new model
            end

            # TΣ
            for rel in tσ_relatives
                success = tσ_decon(kb, par, model, rel, lvl, construction, reconstruction, queues)
                global_success = global_success | success
                if success && par.deconstructionMode === :minimal
                    break # this inner loop
                end
            end
            if global_success && par.deconstructionMode === :minimal
                continue # to next new model
            end

            # nothing for TZ
            for rel in tz_relatives
                @info string("Skipped TZ Deconstruction of model: ",model.ID," with existing model: ",rel.ID)
                println("New model: $((model.ID, model.T, model.Σ, model.Z, model.source))")
                println("Old model: $((rel.ID, rel.T, rel.Σ, rel.Z, rel.source))")
            end

            if !global_success
                # not a single successful deconstruction, so save model
                kb[lvl][model.ID] = model
            end
        end
    end
    # clean possibly unused vms on lvl 0
    levelzerogc(kb)
    return kb
end

# full deconstruction
function full_decon(kb::KnowledgeBase, par::ParametersConML, newmodel::AbstractPragmaticModel, oldmodel::AbstractPragmaticModel, currentlevel::Int, reconstruction::Reconstruct, deconqueues)
    if par.verbose println("Started TΣZ Deconstruction: $((newmodel.ID, newmodel.T, newmodel.Σ, newmodel.Z, newmodel.source)) => $((oldmodel.ID, oldmodel.T, oldmodel.Σ, oldmodel.Z, oldmodel.source))") end
    kb.workdone[:deconstruction] += 1
    success::Bool = false
    # calculate feature matches
    featurematches = [feat for feat in newmodel.source if feat in oldmodel.source] # inherits order from newmodel

    # get sourcedata (input features) of new and old model + timestamps + targets/predictions
    newT, newSource, newTargets = getsourceandtargets(newmodel,kb)
    oldT, oldSource, oldTargets = getsourceandtargets(oldmodel,kb)

    # calculate intersecting time points, keep fusion learnblock variables in scope
    interT = [ t for t in newT if t in oldT]
    lbT = [] #TODO type this probably
    lbSource = []
    lbTargets = []
    newfeaturenames = []

    # if feature intersection big enough
    if length(featurematches) >= 2
        if par.verbose println("Doing Feature Intersection: $(featurematches)") end
        # do feature intersection and time join

        # subset features
        newfeatsmask = convert(Array{Int64,1} ,indexin(featurematches, newmodel.source)) # note: convert removes the Union{Nothing,Int} from indexin and fails if indexin returns something unexpected, so it's a cheap sanity check
        oldfeatsmask = convert(Array{Int64,1} ,indexin(featurematches, oldmodel.source)) # indexin instead of in enables ordering and subsection of features in one step
        newSource = newSource[:,newfeatsmask]
        oldSource = oldSource[:,oldfeatsmask]

        # if there is a time intersection, too, it needs to be resolved
        # NOTE most of this copied from σz-decon, look there too for bugs
        nointersection = isempty(interT)
        # if intersection not empty
        if !nointersection
            if par.verbose println("Overlapping timestamps identified.") end
            # calculate maximum allowed time conflicts
            maxconflicts = par.MaxTimeConflicts isa Int ? par.MaxTimeConflicts : par.MaxTimeConflicts*(length(newT)+length(oldT)-length(interT)) # count intersection ony once

            timemask = in(interT)
            # delete conflicts from new and old and resolve to interT/Source/Predictions
            newmask = timemask.(newT)
            newTConflicts, newT = separate(newmask, newT)
            newSourceConflicts, newSource = separate(newmask, newSource, dim=1)
            newTargetsConflicts, newTargets = separate(newmask, newTargets)

            oldmask = timemask.(oldT)
            oldTConflicts, oldT = separate(oldmask, oldT)
            oldSourceConflicts, oldSource = separate(oldmask, oldSource, dim=1)
            oldTargetsConflicts, oldTargets = separate(oldmask, oldTargets)

            #sanity check
            @assert (newTConflicts == oldTConflicts) && (newTConflicts == interT) "In Complete Deconstruction: Conflict detection produced unexpected output! \nNew model timestamps sorted: $(issorted(newTConflicts)), Old model timestamps sorted: $(issorted(oldTConflicts))"

            numberofconflicts = 0
            # TODO might turn this into a function
            # TODO this should have a tolerance parameter, especially in the procedural domain
            if par.deconstructionStrategy == :conservative
                for i in eachindex(interT)
                    if oldTargetsConflicts[i] != newTargetsConflicts[i]
                        numberofconflicts += 1
                    end
                end
                interSource = oldSourceConflicts
                interTargets = oldTargetsConflicts
            elseif par.deconstructionStrategy == :integrative
                for i in eachindex(interT)
                    if oldTargetsConflicts[i] != newTargetsConflicts[i]
                        numberofconflicts += 1
                    end
                end
                interSource = newSourceConflicts
                interTargets = newTargetsConflicts
            else # par.deconstructionStrategy == :opportunistic
                # make new arrays for interT, interSource and interTargets so we can delete entries
                interT = collect(newTConflicts)
                interSource = collect(newSourceConflicts)
                interTargets = collect(newTargetsConflicts)
                # throw out conflicting value completely
                keepmask = trues(size(interT))
                for i in eachindex(interT)
                    # NOTE this deletes samples with the same time stamps and conflicting features, too
                    # could be change as necessary. E.g. to always keep the features from the bigger model in this case
                    if (newTargetsConflicts[i] != oldTargetsConflicts[i]) || all(newSourceConflicts[i,:] .!= oldSourceConflicts[i,:])
                        keepmask[i] = false
                    end
                end
                # apply mask
                interT = interT[keepmask]
                interSource = interSource[keepmask,:]
                interTargets = interTargets[keepmask]
            end

            if numberofconflicts > maxconflicts
                # if number of conflicts bigger than allowed: not resolved, abort
                # behaves as if not intersections were possible, continuing with other deconstruction strategies
                # TODO verbose output and log
                if par.verbose println("Too many conflicting timepoints: $(numberofconflicts) conflicts. Aborting.") end
                return success
            else
                if (length(newT)+length(oldT)+length(interT)) < par.LearnBlockMinimum # newT, oldT and interT are exclusive now, so just add up
                    # if resolving conflicts shrunk model below LearnBlockMinimum (should only happen with opportunistic mode, but check always)
                    # behaves as if not intersections were possible, continuing with other deconstruction strategies
                    # TODO verbose output and log
                    if par.verbose println("Redundancy check reduced valid time points below learn block minimum. Aborting.") end
                    return success
                # else resolved successfully
                end
            end
        end
        # got here iff intersection empty or conflicts successfully resolved

        # concatenate all timepoints, incorporate resolved redundancies if any
        lbT = nointersection ? vcat(newT,oldT) : vcat(newT,interT,oldT)
        lbSource = nointersection ? vcat(newSource,oldSource) : vcat(newSource,interSource,oldSource)
        lbTargets = nointersection ? vcat(newTargets,oldTargets) : vcat(newTargets,interTargets,oldTargets)
        newfeaturenames = featurematches
        # feature intersection and time join done
    else
        if par.verbose println("Doing Time Intersection: length $(length(interT))") end
        # if no feature intersection but time intersection big enough
        if length(interT) >= par.LearnBlockMinimum
            # subset targets for krippendorff
            interTmask = in(interT)
            comparetargets = hcat(newTargets[interTmask.(newT)], oldTargets[interTmask.(oldT)])
            R = par.learningDomain === Conceptual() ? UnitRange(extrema(comparetargets)...) : :continuous
            krippscore = Krippendorff.alpha(comparetargets, metric = par.KrippendorffMetric, R = R)
            # if kripp > threshold -> targets are considered matching
            if par.verbose println("Krippendorff-α: $(krippscore)") end
            if krippscore > par.MinReliability
                if par.verbose println("Reliability test passed.") end
                # NOTE until things are clarified, features of time intersection are symmetric difference
                # do time intersection and feature join
                selectedFeatures = symdiff(newmodel.source, oldmodel.source) #.source are the feature vectors in the model objects
                featuremask = in(selectedFeatures) # in is okay here since order doesn't matter
                # Targets already subset in last step, combine to canonical target vector
                # chooses which model to take targets from on integrative/conservative/opportunistic
                lbTargets = ( par.deconstructionStrategy == :integrative || (par.deconstructionStrategy == :opportunistic && length(newT) > length(oldT)) ) ? newTargets[interTmask.(newT)] : oldTargets[interTmask.(oldT)]
                # subset Source blocks, [timesteps, features]
                interSourcenew = @view newSource[interTmask.(newT), featuremask.(newmodel.source)]
                interSoureceold = @view oldSource[interTmask.(oldT), featuremask.(oldmodel.source)]
                # cat block to combine
                lbSource = hcat(interSourcenew, interSoureceold)
                newfeaturenames = append!(newmodel.source[featuremask.(newmodel.source)],oldmodel.source[featuremask.(oldmodel.source)])
                # learnblock T are the intersection Ts here
                lbT = interT
                # time intersection and feature join done
            else
                if par.verbose println("Reliability test failed. Entering model disposal.") end
                # model disposal (1 of 2)
                if par.deconstructionStrategy == :conservative
                    # keep old model, trash new
                    if par.verbose println("Disposed of new model: $(newmodel.ID)") end
                    success = true # stops further deconstruction
                elseif par.deconstructionStrategy == :integrative
                    # trash old model, keep new (goes into other deconstruction strategies)
                    if par.verbose println("Disposed of old model: $(oldmodel.ID)") end
                    purgefromKB(kb, oldmodel; par.enablewaste)
                    # success = false to enable further deconstruction of new model
                else #par.deconstructionStrategy == :opportunistic
                    # keep biggest model and trash the other
                    if length(newT) > length(oldT)
                        # new model bigger
                        if par.verbose println("Disposed of old model: $(oldmodel.ID)") end
                        purgefromKB(kb, oldmodel; par.enablewaste)
                        # success = false to enable further deconstruction of new model
                    else
                        # old model bigger or same size
                        if par.verbose println("Disposed of new model: $(newmodel.ID)") end
                        success = true # stops further deconstruction
                    end
                end
                # exit from model disposal
                return success
            end
        else
            # neither feature not time intersection possible, continue with deconstruction and another relative
            if par.verbose println("Neither time nor feature intersection possible. Aborting full deconstruction.") end
            return success
        end
    end

    # 1 of both intersections done here
    # make proper learnblock, sort if not already, apply sorting permutation to targets, too
    lb = sort!(VMS(lbT, repeat([newmodel.Σ],length(lbT)), repeat([newmodel.Z],length(lbT)), lbSource, newfeaturenames), targets = lbTargets )
    # delete both old and new, only fusion remains
    success = true # stops further deconstruction
    purgefromKB(kb, oldmodel; par.enablewaste)

    # initiate reconstruction/differentiation/save loop
    if par.verbose println("Initiate model Reconstruction") end
    reconstructedmodels = _full_decon_reconstruction_differentiation(lb, lbTargets, kb, par, currentlevel, reconstruction, deconqueues)
    if par.verbose println("$(reconstructedmodels) (sub)models successfully reconstructed.") end
    return success
end

function _full_decon_reconstruction_differentiation(lb::VMS, targets, kb::KnowledgeBase, par::ParametersConML, currentlevel::Int, reconstruction::Reconstruct, deconqueues)
    @assert length(lb.T) == length(targets)
    # push through reconstruction
    # Z for constructionlikeformatter is the same that was written into all Z of the LearnBlock, take from there
    fusionModels = reconstruction(kb, par, constructionlikeformatter(lb, targets, lb.Z[1]),
                    KnowledgeLevel = currentlevel, winnerselectionoverwrite = true)
    if reconstructionsuccessful(fusionModels)
        # there's only one candidate here from the constructionlikeformatter, so this is valid
        fusionModel = only(fusionModels)
        if par.verbose println("Successful deconstruction: $(fusionModel.ID)") end
        # if successful: save fusion (with or without decon depending on minimal/maximal)
        if par.deconstructionMode === :minimal
            savetoKB(kb, fusionModel)
        else #par.deconstructionMode === :maximal
            push!(deconqueues[currentlevel], fusionModel)
        end
        # return 1 successfully reconstructed model
        return 1
    else
        if par.verbose println("Failed deconstruction: model differentiation initiated") end
        # decon not successful, try differentiation
        # temporal clustering (differentiation), use _processBlocks function from LearnBlockSearch to get submodel indices
        submodelindices = _processBlocks(lb.T, lb.Σ, lb.Z, par)
        if par.verbose println("Number of identified time clusters: $(length(submodelindices))") end
        if length(submodelindices) > 1
            # subset lb and targets according to clustering
            submodels = [ lb[inds] for inds in submodelindices ]
            subtargets = [ targets[inds] for inds in submodelindices ]
            nsuccessfulchildren = 0 # how many submodels could successfully be reconstructed
            if par.verbose println("Reconstructing submodels.") end
            for (submodel, targetsubset) in zip(submodels, subtargets)
                nsuccessfulchildren += _full_decon_reconstruction_differentiation(submodel, targetsubset, kb, par, currentlevel, reconstruction, deconqueues)
                kb.workdone[:deconstruction] += 1
            end
            return nsuccessfulchildren
        # else dispose is just do nothing with the model and exit
        end
    end
    return 0 # nothing reconstructed successfully
end

# ΣZ deconstruction
function σz_decon(kb::KnowledgeBase, par::ParametersConML, newmodel::AbstractPragmaticModel, oldmodel::AbstractPragmaticModel, currentlevel::Int, construction::Construct, reconstruction::Reconstruct, deconqueues)
    if par.verbose println("Started ΣZ Deconstruction: $((newmodel.ID, newmodel.T, newmodel.Σ, newmodel.Z, newmodel.source)) => $((oldmodel.ID, oldmodel.T, oldmodel.Σ, oldmodel.Z, oldmodel.source))") end
    kb.workdone[:deconstruction] += 1
    # consider putting time-check into main deconstruction method for faster checks
    success = false

    # dependent on settings, get an optimized method for comparing T distance
    σz_timecheck = _get_σz_timecheck_method(par.MaxDistanceT)

    if par.verbose println("Checking for temporal distance: $((newmodel.ID, newmodel.T)) => $((oldmodel.ID, oldmodel.T))") end
    # check if distance is in allowed range
    if σz_timecheck(newmodel.T, oldmodel.T)

        if par.verbose println("Temporal distance allows deconstruction.") end
        # try to create feature intersection
        featurematches = [feat for feat in newmodel.source if feat in oldmodel.source] # inherits order from newmodel
        if par.verbose println("Number of matching features: $(length(featurematches))") end
        if length(featurematches) >= 2

            # get sourcedata (input features) of new and old model + timestamps + targets/predictions
            newT, newSource, newTargets = getsourceandtargets(newmodel,kb)
            oldT, oldSource, oldTargets = getsourceandtargets(oldmodel,kb)

            # calculate overlapping timestamps
            interT = [t for t in newT if t in oldT]
            interSource = []
            interTargets = []
            if par.verbose println("Number of overlapping timestamps: $(length(interT))") end

            # subset features to be comparable
            newfeatsmask = convert(Array{Int64,1} ,indexin(featurematches, newmodel.source)) # note: convert removes the Union{Nothing,Int} from indexin and fails if indexin returns something unexpected, so it's a cheap sanity check
            oldfeatsmask = convert(Array{Int64,1} ,indexin(featurematches, oldmodel.source)) # indexin instead of in enables ordering and subsection of features in one step
            newSource = newSource[:,newfeatsmask]
            oldSource = oldSource[:,oldfeatsmask]

            resolved = false
            nointersection = isempty(interT)
            # if intersection not empty
            if !nointersection
                # calculate maximum allowed time conflicts
                maxconflicts = par.MaxTimeConflicts isa Int ? par.MaxTimeConflicts : MaxTimeConflicts*(length(newT)+length(oldT)-length(interT)) # count intersection ony once

                timemask = in(interT)
                # delete conflicts from new and old and resolve to interT/Source/Predictions
                newmask = timemask.(newT)
                newTConflicts, newT = separate(newmask, newT)
                newSourceConflicts, newSource = separate(newmask, newSource, dim=1)
                newTargetsConflicts, newTargets = separate(newmask, newTargets)

                oldmask = timemask.(oldT)
                oldTConflicts, oldT = separate(oldmask, oldT)
                oldSourceConflicts, oldSource = separate(oldmask, oldSource, dim=1)
                oldTargetsConflicts, oldTargets = separate(oldmask, oldTargets)

                #sanity check
                @assert (newTConflicts == oldTConflicts) && (newTConflicts == interT) "In ΣZ Deconstruction: Conflict detection produced unexpected output! \nNew model timestamps sorted: $(issorted(newTConflicts)), Old model timestamps sorted: $(issorted(oldTConflicts))"

                if par.verbose println("Resolving time conflicts.") end
                numberofconflicts = 0
                if par.deconstructionStrategy == :conservative
                    for i in eachindex(interT)
                        if oldTargetsConflicts[i] != newTargetsConflicts[i]
                            numberofconflicts += 1
                        end
                    end
                    interSource = oldSourceConflicts
                    interTargets = oldTargetsConflicts
                elseif par.deconstructionStrategy == :integrative
                    for i in eachindex(interT)
                        if oldTargetsConflicts[i] != newTargetsConflicts[i]
                            numberofconflicts += 1
                        end
                    end
                    interSource = newSourceConflicts
                    interTargets = newTargetsConflicts
                else # par.deconstructionStrategy == :opportunistic
                    # make new arrays for interT, interSource and interTargets so we can delete entries
                    interT = collect(newTConflicts)
                    interSource = collect(newSourceConflicts)
                    interTargets = collect(newTargetsConflicts)
                    # throw out conflicting value completely
                    keepmask = trues(size(interT))
                    for i in eachindex(interT)
                        # NOTE this deletes samples with the same time stamps and conflictiong features, too
                        # could be change as necessary. E.g. to always keep the features from the bigger model in this case
                        if (newTargetsConflicts[i] != oldTargetsConflicts[i]) || all(newSourceConflicts[i,:] .!= oldSourceConflicts[i,:])
                            keepmask[i] = false
                        end
                    end
                    # apply mask
                    interT = interT[keepmask]
                    interSource = interSource[keepmask,:]
                    interTargets = interTargets[keepmask]
                end

                if numberofconflicts > maxconflicts
                    # if number of conflicts bigger than allowed: not resolved, abort
                    # TODO verbose output and log
                    if par.verbose println("Too many conflicting timestamps: $(numberofconflicts). Aborting.") end
                    # since resolved is still false method stops naturally
                else
                    if (length(newT)+length(oldT)+length(interT)) < par.LearnBlockMinimum # newT, oldT and interT are exclusive now, so just add up
                        # if resolving conflicts shrunk model below LearnBlockMinimum (should only happen with opportunistic mode, but check always)
                        if par.verbose println("Redundancy check reduced valid time points below learn block minimum. Aborting.") end
                        # TODO verbose output and log
                    else
                        if par.verbose println("Conflicts successfully resolved.") end
                        resolved = true
                    end
                end
            end
            # if intersection empty or conflicts successfully resolved
            if nointersection || resolved
                # concatenate all timepoints, incorporate resolved redundancies if any
                lbT = nointersection ? vcat(newT,oldT) : vcat(newT,interT,oldT)
                lbSource = nointersection ? vcat(newSource,oldSource) : vcat(newSource,interSource,oldSource)
                lbTargets = nointersection ? vcat(newTargets,oldTargets) : vcat(newTargets,interTargets,oldTargets)
                # make proper learnblock
                lb = sort!(VMS(lbT, repeat([newmodel.Σ],length(lbT)), repeat([newmodel.Z],length(lbT)), lbSource, featurematches))
                # push through reconstruction
                if par.verbose println("Creating extended model.") end
                #TODO !!!specialize for upcomming multi-output reconstruction
                extendedModels = reconstruction(kb, par, constructionlikeformatter(lb, lbTargets, newmodel.Z),
                                    KnowledgeLevel = currentlevel, winnerselectionoverwrite = true)
                #XXX quick and dirty
                if reconstructionsuccessful(extendedModels)
                    extendedModel = length(extendedModels)==1 ? extendedModels[1] : error("Multiple reconstruction output not implemented for deconstruction")
                    # if successful: save fusion and delete old model
                    if par.verbose println("Reconstruction of extended model successful.") end
                    success = true
                    if par.deconstructionMode === :minimal
                        savetoKB(kb, extendedModel)
                    else #par.deconstructionMode === :maximal
                        push!(deconqueues[currentlevel], extendedModel)
                    end
                    purgefromKB(kb, oldmodel; par.enablewaste)
                else
                    if par.verbose println("Reconstruction unsuccessful. Aborting ΣZ deconstruction.") end
                end
                # TODO optional: create something similar to try_to_rebuild_abstraction
            end
        else
            if par.verbose println("Insufficient number of matchng features. Aborting.") end
        end
    end
    return success
end

# TΣ deconstruction
function tσ_decon(kb::KnowledgeBase, par::ParametersConML, newmodel::AbstractPragmaticModel, oldmodel::AbstractPragmaticModel, currentlevel::Int, construction::Construct, reconstruction::Reconstruct, deconqueues)
    if par.verbose println("Started TΣ Deconstruction: $((newmodel.ID, newmodel.T, newmodel.Σ, newmodel.Z, newmodel.source)) => $((oldmodel.ID, oldmodel.T, oldmodel.Σ, oldmodel.Z, oldmodel.source))") end
    kb.workdone[:deconstruction] += 1
    success = false
    if currentlevel >= par.highestLevel
        # already at highest level, abort
        if par.verbose println("Already at highest level in KnowledgeBase. Aborting.") end
        # TODO log or not
    else
        # get target data and time stamps
        newT, newS = modelpredictions(newmodel,kb)
        oldT, oldS = modelpredictions(oldmodel,kb)
        # create T intersection of new and old model
        if par.verbose println("Searching for overlapping timestamps.") end
        T, Source = _timeintersect(newT, newS, oldT, oldS)
        # if sufficient intersecting timestamps found
        if length(T) ≥ par.LearnBlockMinimum
            if par.verbose println("Sufficient number of shared timestamps found: $(length(T))") end
            # check if krippendorffs-α is not too high
            R = par.learningDomain === Conceptual() ? UnitRange(extrema(Source)...) : :continuous
            krippscore = Krippendorff.alpha(Source, metric = par.KrippendorffMetric, R = R)
            if par.verbose println("Krippendorff-α on shared timestamps: $(krippscore)") end
            if krippscore<par.MinReliability && (krippscore>=0. || par.AllowWeakReliability)
                if par.verbose println("Abstraction possible. Forming new LearnBlock.") end
                # make learnblock
                sourcestring = String[newmodel.ID, oldmodel.ID]
                lb = VMS(T, ["" for i in T], ["" for i in T], Source, sourcestring) #blanks for Σ and Z will be filled by reconstruction
                # push through construction and reconstruction
                # TODO fix logging for those methods, they currently log as regular construction/reconstruction
                abstractedModels = reconstruction(kb, par, construction(kb, par, lb),
                                    KnowledgeLevel = currentlevel+1, winnerselectionoverwrite = true) # without feature selection here
                # if construction and reconstruction worked
                if reconstructionsuccessful(abstractedModels)
                    for abstractedModel in abstractedModels
                        # success!
                        if par.verbose println("Model successfully abstracted: $(abstractedModel.ID)") end
                        success = true
                        # save newmodel if not already in KnowledgeBase
                        if !haskey(kb[currentlevel], newmodel.ID)
                            savetoKB(kb, newmodel)
                        end
                        # put abstractedModel into deconstruction queue
                        push!(deconqueues[currentlevel+1], abstractedModel)
                        # TODO logging
                    end
                else
                    if par.verbose println("Abstracted model could not be deconstructed.") end
                end
            else
                if par.verbose println("Blocks are too similar so no knowledge can be abstracted. Aborting.") end
            end
        else
            if par.verbose println("Insufficient number of shared timestamps. Aborting.") end
        end
    end
    return success
end

# ΣZ helper methods for checking max time distance
_get_σz_timecheck_method(MaxDistanceT::Float64) = MaxDistanceT==1. ? _forcetimeexpansion : ( MaxDistanceT==0. ? _notolerance : _timecheckwithtolerancewrapper(MaxDistanceT))

_forcetimeexpansion(args...) = true
_notolerance((lmin, lmax)::NTuple{2,Int}, (rmin, rmax)::NTuple{2,Int}) = (lmax >= rmin) || (rmax >= lmin)
_timecheckwithtolerancewrapper(max_dist_t::Float64) = ((lmin,lmax),(rmin,rmax)) -> _timecheckwithtolerance(lmin,lmax,rmin,rmax) ≤ max_dist_t
@inline function _timecheckwithtolerance(lmin, lmax, rmin, rmax)
    if lmin<rmin
        range = rmax-lmin
        dist = ifelse( rmin>lmax, rmin-lmax, 0)
    else
        range = lmax-rmin
        dist = ifelse( lmin>rmax, lmin-rmax, 0)
    end
    return dist/range
end

"""
    separate(p, itr)

Separate elements in `itr` by the return value of predicate `p`.
Returns a tuple of 2 Vectors, the first one containing all elements for which `p(element)===true` and the second containing all other values.
If the input is ordered, both output vectors respect that element order. Flattens multidimensional inputs.
"""
function separate(f, itr)
    truepart = Vector{eltype(itr)}(undef,0)
    falsepart = Vector{eltype(itr)}(undef,0)
    n = length(itr)÷2
    sizehint!(truepart, n)
    sizehint!(falsepart, n)
    for e in itr
        v = f(e)
        if v
            push!(truepart, e)
        else
            push!(falsepart, e)
        end
    end
    return (truepart, falsepart)
end

"""
    separate(b::BitArray, arr::AbstractArray)

Returns (arr[b], arr[!b]) as views.
"""
separate(b::AbstractArray{Bool,I}, arr::AbstractArray; dim::Union{Integer,Colon} = :) where {I} = _separate(b, arr, dim)
_separate(b, arr, ::Colon) = ( view(arr, b), view(arr, map(!,b)) )
_separate(b, arr, i::Integer) = (selectdim(arr, i, b), selectdim(arr, i, map(!,b)))

"""
    reconstructionsuccessful(output)

Return false if ´output´ indicates that reconstruction (or anything before that)
failed and true otherwise.
"""
function reconstructionsuccessful(output)
    isnothing(output) && return false
    output isa Tuple{Nothing, Any} && return false
    return true
end

"""
    levelzerogc(kb)

Clean up models in the 0th level of the knowledge domain that are not referenced from any higher model.
"""
function levelzerogc(kb)
    zerolevels = collect(keys(kb[0]))
    for key in zerolevels
        if !haskey(kb[1], key)
            delete!(kb[0], key)
        end
    end
end
