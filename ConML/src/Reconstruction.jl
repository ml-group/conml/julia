#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConML.jl and
# may not behave as expected when used on it's own
struct QCdefault end

"""
	Reconstruct(estimators, names; winnerselection = select_best_or_none) ... make this simpler
	Reconstruct(pairs(estimator, name))
	Reconstruct(pairs(name, estimator))
	Reconstruct(dictionary(estimator,name))
	Reconstruct(dictionary(name, estimator))

Create a callable object for Reconstruction. Estimators must be subtypes of scikit-learn BaseEstimator and implement the scikit-learn API for supervised methods (namely, fit!(method, Xs, Ys) and predict(method, Xs) are expected).
"""
struct Reconstruct{T}
    methods::T
	names::Vector{String}
	qualitycontrol
	winnerselection

    function Reconstruct(cs, names::AbstractVector{String};
							qualitycontrol = QCdefault(), winnerselection = select_best_or_none)
        if length(cs)<=1
            throw(AssertionError(string("Two or more predictors needed for Reconstruct, got: ", length(cs))))
        end
		if length(cs) != length(names)
			throw(AssertionError("Number of methods and assigned names for Reconstruct must match!"))
		end
		ct = Tuple(collect(cs)) # tuple for types
		namesvector = [string(name) for name in names] #enforce type Vector{String}
		new{typeof(ct)}(ct, namesvector, qualitycontrol, winnerselection)
    end
end
# TODO docstring, mention shortdefaultnames
# TODO simplify this, make inner constructor use whatever Base.pairs yields and make outer
# constructor call pairs on the input if it's one or combine and make pairs if it's 2 vectors
Reconstruct(cs; shortdefaultnames::Bool = false, short::Bool = false, kwargs...) = Reconstruct(cs, generateNamesForEstimators(cs, :reconstruction, (short | shortdefaultnames)); kwargs...)
Reconstruct(pairsStringEst::AbstractVector{Pair{String,E}}; kwargs...) where {E} = Reconstruct(last.(pairsStringEst), first.(pairsStringEst); kwargs...)
Reconstruct(pairsEstString::AbstractVector{Pair{E,String}}; kwargs...) where {E} = Reconstruct(first.(pairsEstString), last.(pairsEstString); kwargs...)
Reconstruct(dictStringEst::AbstractDict{String,E}; kwargs...) where {E} = Reconstruct([(key => value) for (key,value) in dictStringEst]; kwargs...)
Reconstruct(dictEstString::AbstractDict{E,String}; kwargs...) where {E} = Reconstruct([(key => value) for (key,value) in dictEstString]; kwargs...)

# TODO docstring again, fix types
"""
    myReconstructor = Reconstruct([m1,m2,...])
    successfully_reconstructed = myReconstructor(KnowledgeBase, Parameters, (learnblocks, targets, names) )

Execute Reconstruction process with the specified configuration for a given (list of learnblocks, list of target predictions, list of classifier names) tuple.
Return the winner as tuple (learnblock, estimators) or nothing if Reconstruction failed.
"""
function (r::Reconstruct)( kb::KnowledgeBase, par::ParametersConML, learnblocktuple::Tuple{VMS,Any,Any}; kwargs...)
	# for the case that something skips feature selection (e.g. Deconstruction),
	# we don't get ([learnblocks],[targets]) but (common_learnblock,[targets])
	# solution: repeat learnblock for all target predictions and call the normal function
	lb, targets, names = learnblocktuple
	newtuple = ([lb for _ in 1:length(targets)], targets, names)
	r(kb,par,newtuple; kwargs...)
end

# shortcut if no classifier passed construction
function (r::Reconstruct)(::KnowledgeBase, ::ParametersConML, ::Nothing; kwargs...)
	# TODO is there anything loggable here?
	return nothing
end

# main reconstruction function
function (r::Reconstruct)(kb::KnowledgeBase, par::ParametersConML, learnblockstuple::Tuple{AbstractVector{<:VMS},Any,Any};
							KnowledgeLevel::Int = 1, winnerselectionoverwrite = false)
							# winnerselectionoverwrite means that if the reconstruction is called
							# from deconstruction, we expect no or only one winning model, so there
							# should be no elaborate winner selection scheme here. This is true
							# as long as we only ever deconstruct 2 models at a time. If more models are
							# deconstructed (abstracted) in one step, we would expect more than one
							# winner to be possible and this would have to be disabled again

	# destructure input
	if par.verbose println("Called Reconstruction.") end
	(learnblocks, targets, connames) = learnblockstuple

	# iterate all candidate models (learnblock, targets_from_construction, construction_candidate_name)
	# get a list of tuples: krippendorff-α score plus an index concerning the most
	# representative reconstruction model (indices into r.methods and r.names)
	scores, representatives = _reconstruction_scores_and_reps(learnblocks, targets, connames,
															par, r.methods, r.names, r.qualitycontrol)

	winnerindices::Vector{Int} = _recon_winner_selection(winnerselectionoverwrite ? select_best_or_none : r.winnerselection,
													par.learningDomain, par.MinReliability, scores,
													map(featureNumber, learnblocks),
													targets, par.verbose, logger = par.Logger)

	kb.workdone[:reconstruction] += sum(!isnan, scores)
	# if no winner could be determined, winnerindices will be an empty array
	if isempty(winnerindices)
		if par.verbose println("Reconstruction unsuccessful.") end
		#writelog(par.Logger, ReconstructionFailedLog())
		# potentially append one of the failed learnblocks to waste
		if par.enablewaste
			push!(kb.waste, learnblocks[firstindex(learnblocks)])
		end
		return nothing
	else
		# logging moved to winner selection for specialization
		if par.verbose println("Reconstruction successful.\nWinner candidate$(length(winnerindices)==1 ? "s" : ""): ", connames[winnerindices]) end

		# retrain winner(s) on whole learnblock without train/test split + make proper MachineModel
		# the MachineModel constructor registers the associated Learnblock automatically if on level 1
		MM = par.usePredictionsAsTargets ? MinimalMachineModel : PragmaticMachineModel
		return 	[ MM(	kb, par.learningDomain, KnowledgeLevel,
						learnblocks[i], r.names[representatives[i]], connames[i],
						fit!(clone(r.methods[representatives[i]]), learnblocks[i].vals, targets[i]),
						targets[i]	)
				for i in winnerindices ]
	end
end

function _reconstruction_scores_and_reps(learnblocks, targets, connames, par::ParametersConML,
										recmethods, recnames::Vector{String}, qualitycontrol)
	scores = Float64[]
	representatives = Int[]
	# replace QCdefault with appropriate qualitycontrol method for conceptual/procedural or
	# leave unchanged if user-supplied
	qcalgo = (qualitycontrol isa QCdefault) ? ( par.learningDomain == Conceptual() ? conceptualQCmaker(par.MinTestAccuracy) : proceduralQCmaker(par.MaxTestErrorAvg, par.MaxTestErrorMax))	: qualitycontrol
	for (lb, classes, conname) in zip(learnblocks, targets, connames)
		s,r = _reconstruction_iteration(qcalgo, lb, classes, conname, par, recmethods, recnames)
		push!(scores, s) # scores are the reliabilities computed in _compute_reliability_and_representative
		push!(representatives, r) # representative is always the method with the highest score according to qualitycontrol
	end
	return (scores, representatives)
end

function _reconstruction_iteration(qcalgo, lb, classes, conname, par::ParametersConML, recmethods, recnames)
	# get appropriately featureselected (or not) datablock
	data = lb.vals
	if par.verbose println("Candidate ",conname) end

	#TODO make a parameter for stratification
	# Train/Test split and learn each predictor on the train set
	x_train, y_train, x_test, y_test = train_test_split_views(data, classes, testsize = par.testSetPercentage)
	regressors = map(c -> fit!(clone(c), x_train, y_train), recmethods)
	if par.verbose println("applied ", length(regressors), " Reconstruction methods") end

	# apply all predictors to the train set and concatenate the resulting predictions
	predictions = [predict(reg, x_test) for reg in regressors]

	return _compute_reliability_and_representative(qcalgo, y_test, predictions, par, conname, recnames)
end

second(collection) = collection[2]

function _compute_reliability_and_representative(qcalgo, y_test, predictions, par::ParametersConML, conname, recnames)
	# The previous implementation would select the one with the highest
	# score but I realized that this is only useful if "high score == good" which is the case
	# for the conceptual domain (accuracy) but not the procedural domain (MAPE/MaxAPE).
	# Since arbitrary scoring schemes can be used here, this is undecidable at the moment
	# and this section needs a complete rework.
	# The current way is rather hacky for the procedural domain. If the score is a tuple,
	# it's scores get a reverse ranking preprended (in _reconstruction_quality_control) that is extracted again here.
	# In the longer term, we want to drop the
	# notion of "a" representative altogether and keep all the models that are marked as "keep".
	# This needs a change to the way that "Sigma" is stored. Ideally, this should be the set
	# of all the reconstruction methods that passed QC, so that Sigma-relationship amounts
	# to a simple non-empty set intersection. (Or if LOOIRR is ever enabled the set of
	# reconstruction methods with the highest reliability)
	mask, scores = _reconstruction_quality_control(qcalgo, y_test, predictions, par, recnames)
	rankingscore, displayscore = isa(scores, Tuple) ? (length(first(scores))>1 ? (first.(scores),second.(scores)) : (first.(scores),first.(scores))) : (scores, scores)
	selectable = eachindex(rankingscore)[mask]
	representative = length(selectable)==0 ? first(eachindex(scores)) : selectable[argmax(rankingscore[mask])]
	# filter for quality first. If 2 or more classifiers are okay, compute reliability over those
	reliability::Float64 = NaN
	if length(selectable) < 2 # less than 2 classifiers show sufficient accuracy
		if par.verbose println("Insufficient number of classifiers left to continue. Skip candidate.") end
	else
		#TODO enable "LOOIRR" if 3 or more classifiers persist (compute best subset)
		# calculate intersubjectivity metric
		R = par.learningDomain === Conceptual() ? UnitRange(extrema(y_test)...) : :continuous
		reliability = Krippendorff.alpha( hcat(predictions[mask]...),
						metric = par.KrippendorffMetric,
						R = R)
		if par.verbose println("calculated intersubjectivity metric: ", reliability) end
		writelog(par.Logger, ReconstructionProgressLog(), conname, recnames[mask], displayscore[mask], reliability) # TODO missing uids and aims
	end # if sufficient classifiers passed accuracy test
	return (reliability, representative)
end

# general quality control framework
function _reconstruction_quality_control(qcalgo::Q, truth, predictions, par, recnames) where {Q}
	firstprediction, predictionstail = Iterators.peel(predictions)
	ret = qcalgo(truth, firstprediction)
	if ret isa NamedTuple
		haskey(ret, :keep) || error("""
			If you incorporate your own quality control scheme, the return value must either
			be a single Bool or a NamedTuple that contains at least a field `keep` of type Bool and possibly `score` and `name`""")
		keeps = [ret.keep]
		# TODO check this again for correctness
		sizehint!(keeps, length(predictions))
		scores = []
		sizehint!(scores, length(predictions))
		if haskey(ret, :score)
			push!(scores, ret.score)
			for p in predictionstail
				qcp = qcalgo(truth, p)
				push!(keeps, qcp.keep)
				push!(scores, qcp.score)
			end
		else
			scores .= float.(keeps)
			for p in predictionstail
				push!(keeps, qcalgo(truth, p))
			end
		end
		name = get(ret, :name, "Score")
	else # use the returned values as-is
		keeps = map(p->qcalgo(truth, p), predictionstail)
		pushfirst!(keeps, ret.keep)
		scores = keeps
		name = "Score"
	end
	if par.verbose println("$(name): ",[k => v for (k,v) in zip(recnames isa AbstractString ? (recnames,) : recnames, scores)]) end
	# TODO this is super hacky, see further above
	if scores isa Tuple
		scores = (sortperm(first(score), rev=true),scores...)
	end
	if par.verbose println(sum(keeps)," of ", length(keeps), " predictions passed quality control") end
	return (keeps, scores)
end

# these will be set as default for conceptual and procedural domain if not overwritten
function conceptualQCmaker(MinTestAccuracy)
	let accuracythreshold = MinTestAccuracy
		function accuracyQC(truth, predictions)
			accuracy = mean(truth .== predictions)
			keep = accuracy >= accuracythreshold
			return (keep = keep, score = accuracy, name = "Accuracy")
		end
		return accuracyQC
	end
end

function proceduralQCmaker(MaxTestErrorAvg, MaxTestErrorMax)
	let averagetreshold = MaxTestErrorAvg, maxtreshold = MaxTestErrorMax
		function mapeQC(truth, predictions)
			# TODO this always yields Inf when truth has a value that is very lose or at 0. Do something!
			apes = abs.((truth .- predictions) ./ truth)
			mape = mean(apes)
			maxape = maximum(apes)
			keep = mape ≤ averagetreshold && maxape ≤ maxtreshold
			return (keep = keep, score = (mape,maxape), name = "(MAPE, MaxAPE)")
		end
		return mapeQC
	end
end

function _recon_winner_selection(selector, learningDomain::KnowledgeType,
								minReliability::Float64, reliabilities::AbstractVector{Float64},
								featurenumbers::AbstractVector{Int}, targets, verbose; logger )
	# input: krippendorff score, complexity (feature number) for each candidate
	# return: vector with indices of winner(s) (or empty vector if no winner)

	# this more or less defines the API, selector can choose what to use and what to drop
	winneridx = selector(; learningDomain, minReliability, reliabilities,
							featurenumbers, targets, verbose)

	# TODO missing uid
	#BUG winnerReconstructionMethod, bestAccuracy, bestIntersubjectivityValue not defined here
	#writelog(logger, ReconstructionWinnerLog(), winnerReconstructionMethod, bestAccuracy, bestIntersubjectivityValue)

	if isnothing(winneridx)
		# return an empty vector if no winner was found
		return Int[]
	elseif winneridx isa Int
		return Int[winneridx,]
	else
		return Int[winneridx...]
	end
end


# decided to do a small train_test_split myself
function train_test_split_views(X, y=nothing; shuffle::Bool = true, stratify = nothing, trainsize = nothing, testsize = nothing)
	# using views
	splitfraction = _getsplitfraction(trainsize, testsize)
	traininds, testinds = _getsplitindices(stratify, axes(X,1), splitfraction, shuffle)
	return _create_views(X, y, traininds, testinds)
end

function _getsplitfraction(trainsize, testsize)
	if isnothing(trainsize)
		if isnothing(testsize)
			# default
			0.7
		else
			@assert 0. ≤ testsize ≤ 1.
			float(1. - testsize)
		end
	else
		@assert 0. ≤ trainsize ≤ 1.
		isnothing(testsize) || @assert trainsize ≈ 1. - testsize
		float(trainsize)
	end
end

function _create_views(X, ::Nothing, traininds, testinds)
	return _view_slice_first_dim(X, traininds, testinds)
end

function _create_views(X, y, traininds, testinds)
	@assert axes(X,1) == axes(y,1) # assert matching first dimension
	x_train, x_test = _view_slice_first_dim(X, traininds, testinds)
	y_train, y_test = _view_slice_first_dim(y, traininds, testinds)
	return (x_train, y_train, x_test, y_test)
end

#fallback, likely unnecessary since we don't use high-dimensional arrays anyways
_view_slice_first_dim(arr, traininds, testinds) = (view(arr, traininds, repeat([:], ndims(X)-1)...), view(arr, testinds, repeat([:], ndims(X)-1)...))
# 1D to 3D
_view_slice_first_dim(arr::AbstractArray{T,1}, traininds, testinds) where {T} = (view(arr, traininds), view(arr, testinds))
_view_slice_first_dim(arr::AbstractArray{T,2}, traininds, testinds) where {T} = (view(arr, traininds, :), view(arr, testinds, :))
_view_slice_first_dim(arr::AbstractArray{T,3}, traininds, testinds) where {T} = (view(arr, traininds, :, :), view(arr, testinds, :, :))

function _staticsplit(axis, splitfraction::Float64)
	splitind = round(Int, length(axis)*splitfraction)
	return (axis[1:splitind], axis[splitind+1:end])
end

# non-stratified split
_getsplitindices(::Nothing, axis, splitfraction::Float64, shuffle::Bool) = shuffle ? _staticsplit(Random.shuffle(axis), splitfraction) : _staticsplit(axis, splitfraction)

# stratified split
function _getsplitindices(stratify, axis, splitfraction::Float64, shuffle::Bool)
	@assert axes(stratify,1) == axis "Stratify must be of the same shape as the first dimension of the input."
	traininds = Vector{Int}(undef, 0)
	sizehint!(traininds, ceil(Int, length(axis)*splitfraction))
	testinds = Vector{Int}(undef, 0)
	sizehint!(testinds, ceil(Int, length(axis)*(1. - splitfraction)))

	uniquevalues = unique(stratify)
	for uval in uniquevalues
		groupinds = findall(==(uval), stratify)
		grouptrain, grouptest = shuffle ? _staticsplit(Random.shuffle(groupinds), splitfraction) : _staticsplit(groupinds, splitfraction)
		append!(traininds, grouptrain)
		append!(testinds, grouptest)
	end
	return (traininds, testinds)
end

#end of Reconstruct methods
#easiest winner selection method:

#TODO docstring
function select_best_or_none(; reliabilities::Vector{Float64}, featurenumbers::Vector{Int}, minReliability::Float64, kwargs...)
	# conceptual domain winner selection:
	# 	choose best if any > MinReliability
	#	choose smallest complexity in case of multiple best
	#	choose randomly if above still yields more than 1
	# 	return empty vector in case of no winner

	# loop all candidates and update best
	# Note: all scores that are NaN always evaluate the comparisons to false,
	# so they can never be selected as winner here even without special precautions
	bestIntersubjectivityValue = minReliability # threshold for accepting models
	winnerindex = nothing
	for (i, score) in enumerate(reliabilities)
		if score > bestIntersubjectivityValue
			winnerindex = i
			bestIntersubjectivityValue = score
		elseif score == bestIntersubjectivityValue
			if isnothing(winnerindex) || featurenumbers[i] < featurenumbers[winnerindex]
				winnerindex = i
				bestIntersubjectivityValue = score
			else featurenumbers[i] == featurenumbers[winnerindex]
				# same reliability and same number of features, choice is arbitrary TODO is this so?
				@warn "Two best models with equal reliability and complexity identified. An arbitrary choice is being made here."
			end
		end
	end
	#NOTE this would be easier with just an argmax, checking if the value is unique and returning that or nothing, but it's no bottleneck anyways
	return winnerindex
end
