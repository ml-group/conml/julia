#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#

# see logging_structure.md for the structure of the abstract logs here
# Logging goes into a new namespace
module LogCML

    export AbstractLogger, NullLogger, IOLogger, InternalLogger,
    querylog, writelog,
    InitDataBlockLog, AdvanceLevelLog, ConstructionLog, ReconstructionProgressLog, ReconstructionWinnerLog, FeatureSelectionLog, DeconstructionLog

    """
    Abstract supertype for all CML loggers.
    """
    abstract type AbstractLogger end

    """
        NullLogger() <: AbstractLogger

    Convenience type that just discards all log messages. Yields 'nothing' upon querying. Practically equivalent to IOLogger(devnull)
    """
    struct NullLogger<:AbstractLogger end

    """
        InternalLogger(start::Int = 0) <: AbstractLogger

    A logger that keeps an internal datastructure that is filled with information, instead of writing to a file. Querying returns the structure.
    'start' keyword enables beginning to count input blocks at a different index than 1. (start + 1) will be the first index in the log.
    """
    mutable struct InternalLogger<:AbstractLogger
        # structure:
        # { InputNr :
        #   { "BLOCK" :
        #       { "rows" : ...,
        #         "columns" : ...,
        #         "LEVEL" : <===
        #           { LevelNr :
        #               { "LEARNBLOCKS" : <===
        #                   [
        #                       #LBs#
        #                   ]
        #               }
        #           }
        #       }
        #   }
        # }

        inputs::Dict{Int, Dict{String, Dict{String, Any} } }
        currentInputNumber::Int
        currentLevelDict::Dict{Int, Dict{String, Vector } }
        currentLevel::Int
        currentLearnblockVector::Vector #TODO specialize
        currentLearnblock::Int

        function InternalLogger(;start::Int = 0)
            # forces creation as empty structure
            return new(Dict{Int, Dict{String, Dict{String, Any} } }(), start)
            # note that currentLevelDict and currentLearnblockVector (and others) are undefined for now and may not be accessed
            # XXX this may be prone to produce bugs
        end
    end

    """
    IOLogger(io::IO; frequency::Symbol = :input, start::Int = 0) <: AbstractLogger

    A logger that keeps an InternalLogger and additionally writes the current state to a specified IO (e.g. a file or buffer).
    'frequency' may be one of:
    - ':input' once after an input block is processed
    - ':learnblock' at the end of initial reconstruction at level 0 and after all higher levels have been processed by deconstruction
    - ':level' whenever processing finalized a level in the knowledge base (highest frequency)
    'start' keyword enables beginning to count input blocks at a different index than 1. (start + 1) will be the first index in the log.
    """
    struct IOLogger<:AbstractLogger
        io::IO
        frequency::Symbol
        internalLogger::InternalLogger
        function IOLogger(io::IO; frequency::Symbol = :input, start::Int = 0)
            internal = InternalLogger(start = start)
            if frequency ∉ (:input, :learnblock, :level)
                throw(ArgumentError("frequency must be one of :input, :learnblock or :level"))
            end
            return new(io, frequency, internal)
        end
    end


    """
        AbstractLogLevel

    Carries information about what type of event is logged (InitialLearnblock/Construction/...)
    """
    abstract type AbstractLogLevel end
    struct InitDataBlockLog<:AbstractLogLevel end
    struct AdvanceLevelLog<:AbstractLogLevel end
    struct ConstructionLog<:AbstractLogLevel end
    struct ReconstructionProgressLog<:AbstractLogLevel end
    struct ReconstructionWinnerLog<:AbstractLogLevel end
    struct FeatureSelectionLog<:AbstractLogLevel end
    struct DeconstructionLog<:AbstractLogLevel end

    #TODO das hier vervollständigen wenn alle AbstractLogLevels stehen
    const input_LogLevels = (DeconstructionLog,) # once after an input block is processed
    const learnblock_LogLevels = (ReconstructionWinnerLog, DeconstructionLog) # at the end of initial reconstruction at level 0 and after all higher levels have been processed by deconstruction
    const level_LogLevels = (AdvanceLevelLog,) # whenever processing finalized a level in the knowledge base (highest frequency)

    """
        querylog(<:AbstractLogger)

    Returns an entry point for the representation of a specific Logger.
    Return type depends on the concrete type of Logger being queried, look there.
    """
    querylog(::AbstractLogger) = return nothing # default
    querylog(log::InternalLogger) = return log.inputs
    querylog(log::IOLogger) = return log.io

    """
        writelog(<:AbstractLogger, level::Symbol, contents...)

    Write 'contents' to a logger at the specified 'loglevel' (e.g. :Construction).
    """
    writelog(logger::AbstractLogger, level::AbstractLogLevel, contents...) = error(string("Logger of type ",typeof(logger)," has no specialized writelog method for log level: ",level))
    function writelog(::NullLogger, ::AbstractLogLevel, contents...) end # skip

    #define how things will be logged per level

    # for IOLogger, default to writing to the included InternalLogger and then call a method to check if a temporary log should be written
    function writelog(l::IOLogger, level::AbstractLogLevel, contents...)
        writelog(l.internalLogger, level, contents...)
        maketemplogfile(io, level, l.frequency, querylog(l.internalLogger))
    end

    function maketemplogfile(io::IO, level::AbstractLogLevel, frequency::Symbol, contentStructure)
        if (frequency === :input && level in input_LogLevels) ||
        (frequency === :learnblock && level in learnblock_LogLevels) ||
        (frequency === :level && level in level_LogLevels)
            #TODO write contentStructure to JSON into io stream
        end
    end

    """
        writelog(logger, InitDataBlockLog(), n_features, n_samples)

    Called when a datablock is first passed to LearnerConML.
    Set up a new block in the logging structure and write number of features and number of samples.
    """
    function writelog(l::InternalLogger, ::InitDataBlockLog, nfeatures::Int, nsamples::Int)
        l.currentInputNumber += 1
        # create empty level dict and set as current, reset level counter
        newleveldict = Dict{Int, Dict{String, Vector } }()
        l.currentLevel = 0
        l.currentLevelDict = newleveldict
        l.inputs[l.currentInputNumber] = Dict{String, Dict{String, Any}}(
                                            "BLOCK" => Dict{String, Any}(
                                                "rows" => nfeatures,
                                                "columns" => nsamples,
                                                "LEVEL" => newleveldict
                                                )
                                            )
    end

    """
        writelog(logger, AdvanceLevelLog())

    Called on entry to initialize logging structure to the first level in the KnowledgeBase (to process the input block).
    Subsequently called when deconstruction process queue enters a higher abstraction level to initialize a level.
    """
    function writelog(l::InternalLogger, ::AdvanceLevelLog)
        l.currentLevel += 1
        newlearnblockvector = Vector{Any}() #TODO specialize
        l.currentLearnblock = 0
        l.currentLearnblockVector = newlearnblockvector
        l.currentLevelDict[l.currentLevel] = Dict{String, Vector}("LEARNBLOCKS" => newlearnblockvector)  #TODO specialize
    end

    #LEVEL: Construction (before Reconstruction)
    function writelog(l::InternalLogger, ::ConstructionLog, nfeatures::Int, nsamples::Int, classifierAbbreviations::Vector{String})
        # dicts are easier to create in reverse order
        # dict of construction classifiers in a learnblock
        newconstructiondict::Dict{String, Dict{String, Any}} = Dict{String, Dict{String, Any}}()
        for abbrev in classifierAbbreviations
            newconstructiondict[abbrev] = Dict{String, Any}(
                                            "status" => "State.CONSTRUCTED" # TODO only when construction worked (min class size), else: state.DISCARDED
                                            )
        end
        # put this into general learnblock structure
        newlearnblock = Dict{String, Any}( #TODO specialize if possible
                            "rows" => nfeatures,
                            "columns" => nsamples,
                            "relationship" => "('Sigma', 'Z')", # XXX should this be a tuple or a string???
                            "CONSTRUCTION" => newconstructiondict
                            )
        # push to current learnblocks
        push!(l.currentLearnblockVector, newlearnblock)
        l.currentLearnblock += 1
    end

    #LEVEL: Feature Selection
    function writelog(l::InternalLogger, ::FeatureSelectionLog, classifiername::String, nfeatures::Int, nsamples::Int)
        learnblockdict = l.currentLearnblockVector[l.currentLearnblock]
        learnblockdict["CONSTRUCTION"][classifiername]["SELECTION"] = Dict{String,Int}(
                                                                        "rows" => nfeatures,
                                                                        "columns" => nsamples
                                                                        )
    end

    #LEVEL: Reconstruction Progress
    function writelog(l::InternalLogger, ::ReconstructionProgressLog, classifiername::String, reconstruction_classifiernames::AbstractVector{String}, accuracies::AbstractVector{<:Real}, reliability::Real)
        learnblockdict = l.currentLearnblockVector[l.currentLearnblock]
        learnblockdict["CONSTRUCTION"][classifiername]["RECONSTRUCTION"] = [ Dict{String,Any}(
                                                                        "uid" => "placeholder", #TODO
                                                                        "subject" => recname,
                                                                        "aim" => "placeholder", #TODO
                                                                        "accuracy" => acc,
                                                                        "reliability" => reliability
                                                                        ) for (recname, acc) in zip(reconstruction_classifiernames, accuracies) ]
    end

    #LEVEL: Reconstruction Winner
    function writelog(l::InternalLogger, ::ReconstructionWinnerLog, recname::String, accuracy::Real, reliability::Real)
        learnblockdict = l.currentLearnblockVector[l.currentLearnblock]
        learnblockdict["WINNER"] = Dict{String,Any}(
                                    "uid" => "placeholder", #TODO
                                    "subject" => tuple(recname), # XXX should this be a tuple or a string?
                                    "accuracy" => accuracy,
                                    "reliability" => reliability
                                    )
    end

    #LEVEL: Reconstruction Failed

end #module
