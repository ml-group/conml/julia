#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#

module ConML

	import Base.sort
	import Base.empty!
	import KernelDensity
	#import Interpolations
	using Krippendorff
	#using StaticArrays
	using Random
	using Statistics: mean
	using StatsBase: counts
	using ScikitLearnBase
	using Requires

	function __init__()
    	@require DataFrames="a93c6f00-e57d-5684-b7b6-d8193f3e46c0" begin
			using .DataFrames: DataFrame, Not
			function VMS(df::DataFrame; T = "T", Sigma = "Sigma", Z = "Z")
				if !(eltype(df[!,T]) <: Union{Signed,Unsigned})
					@warn "Type of T column should be <: Integer"
				end
				if !(eltype(df[!,Z]) <: AbstractString && eltype(df[!,Sigma]) <: AbstractString)
					@warn "Type of Sigma and Z columns should be <: AbstractString"
				end
				data = df[!,Not([T, Sigma, Z])]
				VMS(df[!,T], df[!,Sigma], df[!,Z], Matrix(data), names(data))
			end
		end
	end

	#Logger gets a separate module to keep namespace a little cleaner
	include("LogCML.jl")
	using .LogCML # TODO repeated: check if necessary things in the Logger are exported
	include("Datastructures.jl")
	include("Reconstruction.jl")
	include("FeatureSelection.jl")
	include("Construction.jl")
	include("LearnBlockSearch.jl")
	include("Deconstruction.jl")
	include("JLSOsaveload.jl")


	# things that will be made accessible via using ConML
	export KnowledgeBase, ParametersConML, VMS, # Datastructures
			featureNumber, sampleNumber, # helper functions
	        LearnerConML, FeatureSelector, Construct, Reconstruct # Pipeline

	function generateNamesForEstimators(cs, estimatortype::Symbol, shortdefaultnames::Bool)
		prefix = "Est"
		if estimatortype === :construction
			prefix = "Con"
		elseif estimatortype === :reconstruction
			prefix = "Rec"
		end
		postfixes = [ifelse(shortdefaultnames, filter(isuppercase, string(typeof(classif))), # short
					string(typeof(classif))) for classif in cs] # long
		return ["$(prefix)$(i)-$(postfix)" for (i,postfix) in enumerate(postfixes)]
	end

	"""
	    TODO
	"""
	struct LearnerConML # TODO make this pretty and a kwdef
	    knowledgeBase::KnowledgeBase
	    parameters::ParametersConML
	    construction::Construct
	    featureselection::Union{Nothing, FeatureSelector}
	    reconstruction::Reconstruct
	    #deconstruction::Deconstruct as method not object, since its defined by construct and reconstruct
	    skipLearnblockSearch::Bool
	end
	LearnerConML(kb, par, con, fs, rec; skipLearnblockSearch = false) = LearnerConML(kb, par, con, fs, rec, skipLearnblockSearch)

	(cl::LearnerConML)(data::Any) = error("LearnerConML expects data in form of a VMS object. Run ?LearnerConML or ?VMS for more info.")
	function (cl::LearnerConML)(data::VMS{D,I,S,F}) where {D,I,S,F}
	    logger = cl.parameters.Logger
	    learnblocks::Vector{VMS{D,I,S,F}} = preprocessLearnBlock(cl.parameters, cl.knowledgeBase, data, skipLearnblockSearch = cl.skipLearnblockSearch)

	    for lb in learnblocks
	        # init new data block in log
	        writelog(logger, InitDataBlockLog(), featureNumber(lb), sampleNumber(lb))
	        # initialize log to first level (of knowledgeBase, i.e. the input block)
	        writelog(logger, AdvanceLevelLog())
			# make FS skippable if nothing was passed
			fsmethod = isnothing(cl.featureselection) ? _skipfeatureselectiondummy : cl.featureselection
	        # run core methods for this learnblock + all higher levels in KB
	        return  deconstruct!( cl.knowledgeBase, cl.parameters, cl.construction, cl.reconstruction,
					cl.reconstruction( cl.knowledgeBase, cl.parameters,
	                fsmethod( cl.parameters,
	                cl.construction( cl.knowledgeBase, cl.parameters, lb
	                ) ) ) )
	    end # for
	end

end # module
