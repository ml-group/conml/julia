#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConML.jl and
# may not behave as expected when used on it's own

"""
Preprocesses input datablock.
Input will be sorted by timestamps if it isn't and missing feature names will be replaced by default values (Feature1 - FeatureX).
If skipLearnblockSearch is false learnblock search function is called afterwards.
"""
function preprocessLearnBlock(par::ParametersConML, kb::KnowledgeBase, block::VMS; skipLearnblockSearch::Bool = false)
	#make sure that input is sorted by timestamps
	if !issorted(block.T)
		sort!(block)
		if par.verbose @info "Sorted input by timestamps" end
	end
	if length(block.vals)==0
		error("Empty input block. Nothing to be processed here.")
	end
	checkfeaturenames(block, par)
	return skipLearnblockSearch ? [block,] : searchLearnBlocks(kb, par, block)
end

"""
	checkfeaturenames(block::VMS)

Checks that all features in a VMS are named and unique and generate default names as necessary.

If there are less names than features in the VMS, names in the form of "UnnamedFeature1" to
"UnnamedFeatureN" will be appended. Additionally, the uniqueness of names is checked and the
function will error out if redundant feature names are identified. (this may change in the future)
"""
function checkfeaturenames(block::VMS, par)
	featurenames = block.fields
	featurenumber = featureNumber(block)
	if length(featurenames)<featurenumber
		numberofmissingfeatures = featurenumber-length(featurenames)
		if par.verbose @info string(numberofmissingfeatures, " unnamed features identified. Adding defaults.") end
		for i in UnitRange(length(featurenames) + 1, featurenumber)
			push!(featurenames, string("UnnamedFeature",i))
		end
	end
	if !allunique(featurenames)
		error(string("Redundant feature names identified. Unrecoverable error. Feature names: ", featurenames))
	end
end

"""
	searchLearnBlocks(ParametersConML, block::VMS)

Select contiguous learnblocks from `block` by temporal clustering.

Perform a temporal clustering of the input block by kernel density estimation and subsequent
density threshold computation. May return one or more learnblocks depending on the settings.
See (Schmid, 2018) for more information.
Note: if only one contiguous learnblock is identified, then the whole block will be considered
a learnblock (without cutting of the bordering regions below `SigmaZetaCutoff` density).

# Arguments
All arguments are set in the corresponding `ParametersConML` object.
- `LearnBlockMinimum::Int`: Minimum number of contiguous samples to form a learnblock.
Densely sampled regions with less than this number of points will be discarded.
This has no default value and must be set for your problem instance specifically.
- `SigmaZetaCutoff::Float64 = 0.2`: Factor for the density threshold below which timepoints
will be assumed to not belong to any learnblock (sparse regions). Higher values will single
out high density regions but discard more samples (thus possibly violating `LearnBlockMinimum`),
while a value of `0` will guarantee that the whole `block` will be considered a contiguous learnblock.
- `lscvBandwidth::Bool = true`: use least squares cross validation (`true`) or Silvermans
rule (false) for bandwidth estimation. (See package: [`KernelDensity`](@ref))
- `UseLearnBlocks::Symbol = :biggest`: must be `:biggest`:return the biggest of all
identified learnblocks only (the rest will go to the pile), or `:all`: return all identified
learnblocks, allowing for the sequential processing of al suitable learnblocks in the `block`.
- `verbose::Bool = true`: Control output of status information.
"""
function searchLearnBlocks(kb::KnowledgeBase, par::ParametersConML, block)
	#TODO (specialize on input format,) maybe deal with missing values
	if par.verbose println("Checking input for learnblocks:") end
	learnblocksindices = _processBlocks(block.T, block.Σ, block.Z, par)
	numberofblocksfound = length(learnblocksindices)
	if par.verbose print("Found ", length(learnblocksindices), " blocks, ") end
	filter!( x-> length(x) >= par.LearnBlockMinimum , learnblocksindices) # filter out too small learnblocks
	if par.verbose println(length(learnblocksindices), " of which are above the minimal specified size for a learnblock.") end

	# use all or only biggest learnblock
	if par.UseLearnBlocks===:biggest
		learnblocksindices = _select_biggest(learnblocksindices) # biggest entry in learnblocks, wrapped in an array again
		#else invariants should guarantee "UseLearnBlocks === :all" -> do nothing
	end
	if par.verbose println("Learnblocks selected for further processing: ", length(learnblocksindices)) end
	# if there was only one block to begin with, take everything as a block, else make list of learn blocks as per the defined mode
	learnblocks = numberofblocksfound == 1 ? [block] : [block[inds] for inds in learnblocksindices]
	if par.enablepile # if recording of unused vector models is enabled
		# compute all unused indices
		inds = setdiff(axes(block.T, 1), learnblocksindices...)
		# merge unused vector models into existing pile
		if !isempty(inds)
			try
				kb.pile[] = combine(kb.pile[], block[inds])
			catch e
				e isa ErrorException || rethrow()
				# if feature names don't match, simply replace exising pile for now
				kb.pile[] = block[inds]
			end
		end
	end
	return learnblocks
end

function _select_biggest(learnblocksindices)
	if isempty(learnblocksindices)
		return learnblocksindices
	else
		index_of_biggest = argmax(map(b->length(b), learnblocksindices))
		return learnblocksindices[[index_of_biggest]]
	end
end

"""
Part of searchLearnBlocks function.
"""
function _processBlocks(T::Vector{Int}, Σ::Vector{String}, Z::Vector{String}, par::ParametersConML)
    #better way, should be linear in problem size if number of points with the same timestamp is bounded
    Tcounts = Dict{Int,Int}()
    sizehint!(Tcounts, length(T))
    for i in eachindex(T)
        t = T[i]
        Tcounts[t] = get(Tcounts, t, 0) + 1
    end
    duplicatetimestamps = [p.first for p in pairs(Tcounts) if p.second>1]
    if par.verbose println(length(duplicatetimestamps)," timestamps occur more than once.") end

    if !isempty(duplicatetimestamps) #skip allocations if there are no matching timestamps
        #classes of points with same timestamps, optimal for few such classes, likely slowish for a lot of points with equal timestamps
        Tclasses = Dict{Int,Vector{Int}}([ (time, findall(t->t==time, T)) for time in duplicatetimestamps ])
        exacts = Vector{Vector{Int}}()
        TΣ = Vector{Vector{Int}}()
        TZ = Vector{Vector{Int}}()

        #similar approach as above, do Σ first (arbitrary)
        for (Tclass, Tinds) in Tclasses

            # in classes with constant T, check if Σ also contains duplicates
            TΣclasses = Dict{String,Vector{Int}}()
            for i in eachindex(Tinds) #indices into the original arrays
                σ = Σ[Tinds[i]]
                if haskey(TΣclasses, σ)
                    push!(TΣclasses[σ], i)
                else
                    TΣclasses[σ] = [i]
                end
            end # TΣ
            filter!(p->length(p.second)>1, TΣclasses)

            #we now have all values of Σ that occur more than once. Write to TΣ list
            append!(TΣ, values(TΣclasses))

            #check Z for exact matchings too
            for (TΣclass, TΣinds) in TΣclasses
                TΣZclasses = Dict{String,Vector{Int}}()
                for i in eachindex(TΣinds) #indices into the original arrays
                    z = Z[TΣinds[i]]
                    if haskey(TΣZclasses, z)
                        push!(TΣZclasses[z], i)
                    else
                        TΣZclasses[z] = [i]
                    end
                end # TΣZ
                filter!(p->length(p.second)>1, TΣZclasses)

                #TΣZclasses contains classes of exact (TΣZ) matchings
                append!(exacts, values(TΣZclasses))
            end

            #done with Σ and exacts, do TZ
            TZclasses = Dict{String,Vector{Int}}()
            for i in eachindex(Tinds) #indices into the original arrays
                z = Z[Tinds[i]]
                if haskey(TZclasses, z)
                    push!(TZclasses[z], i)
                else
                    TZclasses[z] = [i]
                end
            end # TZ
            filter!(p->length(p.second)>1, TZclasses)

            #and finally write TZ classes
            append!(TZ, values(TZclasses))
        end

        # give a little summary of the findings
        if length(exacts)!==0
            @warn "A properly prepared block of data should not contain points with all matching metadata, but the given block did!"
        end
        if par.verbose
			println("Exactly matching TΣZ values occur ",length(exacts)," times in this block.")
        	println("Matching TΣ values occur ",length(TΣ)," times in this block.")
        	println("Matching TZ values exist ",length(TZ)," times in this block.")
		end

        if par.verbose @info "At the moment, nothing is actually done with TΣZ, TΣ and TZ related datapoints. Only ΣZ relations will be further processed." end
    end #if duplicate timestamps existed

    # things with matching timestamps have been processed. Now we can filter ΣZ related points
    ΣZclasses = Dict{Tuple{String,String}, Vector{Int}}()
    for i in eachindex(Z)
        sigz = (Σ[i],Z[i])
        if haskey(ΣZclasses, sigz)
            push!(ΣZclasses[sigz], i)
        else
            ΣZclasses[sigz] = [i]
        end
    end
    filter!(p->length(p.second)>1, ΣZclasses)
    if par.verbose println(length(ΣZclasses)==1 ? "Found a single time series with constant ΣZ values. Clustering for learn Blocks." : string("Found ",length(ΣZclasses)," different time series by ΣZ comparison. Searching for learn blocks in each of them.") ) end

    # now we need KDE and the Interpolation package
    # additionally, retrieve cutoff and minimal learnblock size
    SigmaZetaCutoff::Float64 = par.SigmaZetaCutoff
    # no sanity checks here because invariants should be checked in datastructures

    learnblocks = Vector{Vector{Int}}()
    for indices in values(ΣZclasses)
        timestamps = view(T, indices)
        # NOTE probably should make this into a subfunction
        kd = par.lscvBandwidth ? KernelDensity.kde_lscv(timestamps) : KernelDensity.kde(timestamps)
        cutoff = maximum(kd.density)*SigmaZetaCutoff
        densityestimator = KernelDensity.InterpKDE(kd)
        currentblock = Vector{Int}()
        for (index,t) in zip(indices, timestamps)
            if cutoff > KernelDensity.pdf(densityestimator, t) # below cutoff
                if !isempty(currentblock)
                    push!(learnblocks, currentblock)
                    currentblock = Vector{Int}() # or currentblock = empty(currentblock) TODO see if it makes a difference
                end
            else # above cutoff
                push!(currentblock, index)
            end
        end
		#one last time, flush
		if !isempty(currentblock)
			push!(learnblocks, currentblock)
		end
    end
	# leanblocks are just arrays of indices here
	return learnblocks
end #_processBlocks
#end of learn block searcher
