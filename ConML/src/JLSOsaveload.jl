#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConML.jl and
# may not behave as expected when used on it's own
#
using JLSO

function _checkclearfile(path)
    open(path, read = true, create = true) do file # this touches the file if it does not exist
        for line in readlines(file)
            isempty(line) || return false
        end
        return true
    end
end

function savekb(path, kb; overwrite = false)
    fileclear = _checkclearfile(path)
    if !fileclear
        if overwrite
            @info "Overwriting existing file $(basename(path))."
        else
            @warn "File $(basename(path)) $(isempty(dirname(path)) ? "" : string("at location ",dirname(path),Sys.iswindows() ? "\\" : "/", " "))does already exist.
If you really want to overwrite an existing file, set the keyword argument `overwrite = true`."
            return nothing
        end
    end
    # preprare bits of the knowledge domain
    # VMSs at level 0
    level0 = [] # gets narrowed later
    for (ID,vms) in kb.level_0
        push!(level0, (id=ID, t=vms.T, sigma=vms.Σ, z=vms.Z, fieldnames=vms.fields, content=vms.vals))
    end

    # all other levels that contain models
    usedmodels = Dict{Symbol,String}()
    higherlevels = []
    for currentlevel in 1:length(kb.higherLevels)
        kblvl = kb.higherLevels[currentlevel]
        modelvec = []
        for pragmaticmodel in values(kblvl)
            t::NTuple{2, Int} = pragmaticmodel.T
            sigma::String = pragmaticmodel.Σ
            z::String = pragmaticmodel.Z
            # put used machine model in dictionary if not already there
            if !haskey(usedmodels, Symbol(sigma))
                usedmodels[Symbol(sigma)] = string(typeof(pragmaticmodel.model))
            end
            # sanity check
            if currentlevel != pragmaticmodel.level
                @warn "Found a pragmatic model whose level field does not match the level of the KnowledgeBase that it resides in. Something may be wrong with your KnowledgeBase."
            end
            # extract everything else and save the NamedTuple
            targettimestamps, targets = modelpredictions(pragmaticmodel, kb, t)
            push!(modelvec, (id=pragmaticmodel.ID, t=t, sigma=sigma, z=z, level=pragmaticmodel.level,
                            source=pragmaticmodel.source, targets=targets, targettimestamps=targettimestamps))
            # model can't be saved and is dropped
        end
        if isempty(modelvec)
            break # there can be no more filled levels above an empty level, so stop here
        else
            push!(higherlevels, modelvec)
        end
    end

    # save everything
    JLSO.save(path,
                :type => "ConML_KnowledgeBase",
                :models => NamedTuple(usedmodels),
                :level0 => identity.(level0), # narrow type
                :higher => identity.(higherlevels),
                :lastUIDs => kb.lastusedUID
            )
end

function loadkb(path)
    kbfile = read(path, JLSOFile)
    infostr = """
    The current serialization of KnowledgeBases does not save the model states.
    You have to provide prototypes for the supervised models that were used
    in order to rebuild the KnowledgeBase that was found. The models need to
    support the ScikitLearnBase API (clone, fit! and predict are defined) and they
    have to be passed to the loading function. To proceed, build a Dict{Symbol,...}
    or a NamedTuple of models with the following requirements and call
    `kb = loadkb(path, models)` afterwards:

    - Keys are Symbol (trivially for a NamedTuple) and must contain all
      the model names that are listed below.
    - Values are instances of your model types that support at least `clone`,
      `fit!` and `predict` and are otherwise ready for (re)learning of the
      stored knowledge.

    The following models were found in this KnowledgeBase:
    $([string(':', k, " => ", v, '\n') for (k,v) in pairs(kbfile[:models])]...)
    The following packages were in the project file while this KnowledgeBase was
    constructed. It may help to search for these packages. If you can't reproduce
    a model, you may supply a replacement instead but be aware that this may
    be bad for the applicability and interpretability of the stored knowledge.

    Project:
    $([string(k, " => ", v, '\n') for (k,v) in kbfile.project["deps"]]...)
    """
    @info infostr
    kbfile
end

function loadkb(path, models; ignore::Bool = false)
    kbfile = read(path, JLSOFile)
    kbfile[:type] == "ConML_KnowledgeBase" || ignore || throw(ArgumentError("The file you specified does not appear to be a KnowledgeBase.
Use `ignore = true` to try loading it nontheless."))
    for kbmodel in keys(kbfile[:models])
        kbmodel ∈ keys(models) || throw(ArgumentError("KnowledgeBase contains the model key $(kbmodel) that was not found in the supplied models.
Call `loadkb(path)` for further information about the required models."))
    end
    level0 = Dict{String,VMS}()
    for dataset in kbfile[:level0]
        # rebuild all VMSs for the zeroth level
        T = dataset.t
        Sigma = dataset.sigma
        Z = dataset.z
        ID = dataset.id
        fields = dataset.fieldnames
        data = dataset.content
        vms = VMS(T, Sigma, Z, data, fields)
        level0[ID] = vms
    end
    higherlevels = Vector{Dict{String,AbstractPragmaticModel}}()
    savedhigherlvls = kbfile[:higher]
    for currentlevel in 1:length(savedhigherlvls)
        # regenerate each level from 1st to last
        modeldict = Dict{String,AbstractPragmaticModel}()
        for modeldata in savedhigherlvls[currentlevel]
            T = modeldata.t
            Sigma = modeldata.sigma
            Z = modeldata.z
            ID = modeldata.id
            modeldata.level != currentlevel && @warn "Warning while regenerating KnowledgeBase: got a model whose :level description does not match the knowledge level it appeared in. Savefile may be malformed."
            source = modeldata.source
            targets = modeldata.targets
            targettimes = modeldata.targettimestamps
            sourceT, sourceblock = _findsource(level0, higherlevels, currentlevel, T, ID, source)
            modelprototype = models[Symbol(Sigma)]
            regeneratedmodel = clone(modelprototype)
            fit!(regeneratedmodel, sourceblock, targets)
            modeldict[ID] = PragmaticMachineModel(T,Sigma,Z,ID,regeneratedmodel,currentlevel,source,targets,targettimes)
        end
        push!(higherlevels, modeldict)
    end
    lastusedUIDdict = kbfile[:lastUIDs]
    piledummy = Ref{VMS}(VMS(Int))
    wastedummy = Vector{VMS}()
    workdone = Dict{Symbol,Int}(:construction => 0, :reconstruction => 0, :deconstruction => 0)
    return KnowledgeBase(level0, higherlevels, lastusedUIDdict, piledummy, wastedummy, workdone)
end

function _findsource(level0, higherlevels, lvl, T, ID, sourcestrings)
    if lvl == 1
        return viewDataAndTimes(level0[ID])
    else # higher
        modeldict = higherlevels[lvl-1]
        # this one can rely on the fact that all models are PragmaticMachineModels
        sourcepairs = [_extractpredictions(modeldict[id], T) for id in sourcestrings]
        return _timeintersect(Iterators.flatten(sourcepairs)...)
    end
end

function _extractpredictions(mm::PragmaticMachineModel, (tmin, tmax))
    Ts = mm.targettimes
    targets = mm.targets
    mask = (x-> tmin ≤ x ≤ tmax).(Ts)
    return (Ts[mask], targets[mask])
end
