#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConML.jl and
# may not behave as expected when used on it's own

# to ease dispatching from outer to inner constructors for VMS
struct Inner end

"""
    VMS(D::DataType, featurenames::AV{String} = String[]) # empty VMS, AV stands for AbstractVector
    VMS(T::AV{Int}, Σ::AV{String}, Z::AV{String}, values::AbstractMatrix{D}, fields::AV{String} = String[]) # values a matrix
    VMS(T::AV{Int}, Σ::AV{String}, Z::AV{String}, values::AV{Vector{D}}, fields::AV{String} = String[]) # values as vector of vectors

VectorModelSpace, a structure containing a collection of vectorial models and their pragmatic properties.

# Properties
- `T`: Vector of timestamps serving as column names
- `Σ`: Vector of pragmatic subjects
- `Z`: Vector of pragmatic aims
- `vals`: Data matrix of size (n_samples × n_features) with entries of type D
- `fields`: Vector containing feature names. Will be generated during learning if omitted.
"""
struct VMS{D<:AbstractMatrix{<:Number}, I<:AbstractVector{<:Union{Signed,Unsigned}}, S<:AbstractVector{<:AbstractString}, F<:AbstractVector{<:AbstractString}}
    #column-major layout
    "Vector of timestamps"
    T::I
    "Vector of pragmatic subjects"
    Σ::S
    "Vector of pragmatic aims"
    Z::S
    "Data matrix, shape (n_samples, n_features)"
    vals::D
    "Vector containing feature names (row names). Will be generated during learning if omitted."
    fields::F

    function VMS(::Inner, T::I, Σ::S, Z::S, vals::D, fields::F) where {D<:AbstractMatrix{<:Number}, I<:AbstractVector{<:Union{Signed,Unsigned}}, S<:AbstractVector{<:AbstractString}, F<:AbstractVector{<:AbstractString}}
        if !allequal(length.([T,Σ,Z])..., sampleNumber(vals))
            throw(AssertionError("T, Σ, Z and number of samples must match in length"))
        end
        new{D,I,S,F}(T,Σ,Z,vals,fields)
    end
end
VMS(T, Σ, Z, vals::AbstractMatrix{D}, fields = String[]) where {D<:Number} = VMS(Inner(),T,Σ,Z,vals,fields)
VMS(T, Σ, Z, vals::AbstractVector{Vector{D}}, fields = String[]) where {D<:Number} = VMS(Inner(),T,Σ,Z, mapreduce(adjoint, vcat, vals) ,fields)
VMS(T::Type, fields = String[]) = VMS(Int[], String[], String[], Matrix{T}(undef,0,0), fields)
# convenience Dict => VMS
function VMS(dt::AbstractDict; T = "T", Sigma = "Sigma", Z = "Z")
    datanames = [key for key in keys(dt) if !∈(key, (T, Sigma, Z))]
    data = mapreduce(key -> getindex(dt, key), hcat, datanames)
    VMS(dt[T], dt[Sigma], dt[Z], data, datanames)
end
# and anything with appropriately named fields (intended for NamedTuples)
function VMS(ts; T = :T, Sigma = :Sigma, Z = :Z)
    datanames = [key for key in keys(ts) if !∈(key, (T, Sigma, Z))]
    data = mapreduce(key->getproperty(ts, key), hcat, datanames)
    VMS(getproperty(ts, T), getproperty(ts, Sigma), getproperty(ts, Z), data, datanames)
end

function Base.show(io::IO, v::VMS{M}) where {M}
    nfeatures = length(v.fields)
    tmin, tmax = length(v.T) == 0 ? (nothing, nothing) : extrema(v.T)
    diffSigmas = unique(v.Σ)
    diffZs = unique(v.Z)
    print(io, "VMS{$(M)}($(length(v.T)) timestamps, $(nfeatures) features, Tₘᵢₙ: $(tmin), Tₘₐₓ: $(tmax), ")
    print(io, "Σ: $(string(length(diffSigmas))) unique value$(ifelse(length(diffSigmas)==1,"","s"))$(length(diffSigmas)<=5 ? ": "*string(diffSigmas) : ""), ")
    print(io, "Z: $(string(length(diffZs))) unique value$(ifelse(length(diffZs)==1,"","s"))$(length(diffZs)<=5 ? ": "*string(diffZs) : "")")
    if nfeatures<=10 print(io, ", features: $(v.fields)") else print(io, ", $(length(v.fields)) features") end
    print(io, ")")
end

function Base.show(io::IO, ::MIME"text/plain", v::VMS{M}) where {M}
    nfeatures = length(v.fields)
    tmin, tmax = length(v.T) == 0 ? (nothing, nothing) : extrema(v.T)
    diffSigmas = unique(v.Σ)
    diffZs = unique(v.Z)
    print(io, "VMS{$(M)}:\n$(length(v.T)) timestamps, $(nfeatures) features\nTₘᵢₙ: $(tmin), Tₘₐₓ: $(tmax)\n")
    print(io, "Σ: $(string(length(diffSigmas))) unique value$(ifelse(length(diffSigmas)==1,"","s"))$(length(diffSigmas)<=20 ? ": "*string(diffSigmas) : "")\n")
    print(io, "Z: $(string(length(diffZs))) unique value$(ifelse(length(diffZs)==1,"","s"))$(length(diffZs)<=20 ? ": "*string(diffZs) : "")\n")
    print(io, "Features: $(v.fields)\n")
    print(io, "Values: ")
    printstyled(io, sprint(show, "text/plain", v.vals, context = :limit => true), color = :light_black)
    print(io, ")")
end

#TODO docstring
function combine(v1::VMS, v2::VMS; force::Bool=false)
    # TODO reordering as per fields
    if v1.fields != v2.fields && !(force)
        error("Matching feature names expected, got:  $(v1.fields)  and:  $(v2.fields)")
    end
    if eltype(v1.vals) != eltype(v2.vals)
        @warn "Element types didn't correspond and will be promoted if possible."
    end
    return VMS(vcat(v1.T, v2.T), vcat(v1.Σ, v2.Σ), vcat(v1.Z, v2.Z), vcat(v1.vals, v2.vals), v1.fields)
end

"""
    viewDataAndTimes(VMS [, (Tₘᵢₙ, Tₘₐₓ)])

Get a view of a timeslice of values from a [`VMS`](@ref) plus a flat vector of timestamps
belonging to those values. If no time range is given, return all timestamps and data.
VMS is expected to be ordered, although this is not explicitly checked!

Shape of the resulting matrix will be (n_samples, n_features) in accordance
to the format used by the Scikit API. Return format is (timestamps, values)
"""
function viewDataAndTimes(v::VMS, (tmin, tmax)::NTuple{2, I}) where {I<:Union{Signed,Unsigned}}
    irange = UnitRange(searchsortedfirst(v.T,tmin), searchsortedlast(v.T,tmax))
    return (view(v.T, irange), view(v.vals, irange, :))
end

function viewDataAndTimes(v::VMS)
    return (view(v.T, :), view(v.vals,:,:))
end

"""
    sort(VMS [; targets])

Returns a sorted [`VMS`](@ref) (by timestamps). If a vector of target values is given
as `targets` argument, apply the same permutation to it in place.
"""
function Base.sort(v::VMS; targets = nothing)
    perm = sortperm(v.T)
    if !isnothing(targets) permute!(targets, perm) end
    return VMS(v.T[perm], v.Σ[perm], v.Z[perm], v.vals[perm,:], v.fields)
end

"""
    sort!(VMS [; targets])

Sort a [`VMS`](@ref) in place (by timestamps). If a vector of target values is given
as `targets` argument, apply the same permutation to it in place.
"""
function Base.sort!(v::VMS; targets = nothing)
    perm = sortperm(v.T)
    if !isnothing(targets) permute!(targets, perm) end
    v.T .= v.T[perm]
    v.Σ .= v.Σ[perm]
    v.Z .= v.Z[perm]
    for slice in eachcol(v.vals)
        slice .= slice[perm]
    end
    return v
end

# equality, == and hash operators for comparison and collections
Base.:(==)(x::VMS, y::VMS) = (x.T==y.T) && (x.vals==y.vals) && (x.Σ==y.Σ) && (x.Z==y.Z) && (x.fields==y.fields)
Base.isequal(x::VMS, y::VMS) = isequal(x.T, y.T) && isequal(x.vals, y.vals) && isequal(x.Σ, y.Σ) && isequal(x.Z, y.Z) && isequal(x.fields, y.fields)
Base.hash(v::VMS, h::UInt) = hash(v.T, hash(v.Σ, hash(v.Z, hash(v.vals, hash(v.fields, h + 0x166098bde51b74ec)))))

"""
    timesubset(VMS, (Tₘᵢₙ, Tₘₐₓ))

Return a [`VMS`](@ref) of views containing only those samples in the timerange (Tₘᵢₙ, Tₘₐₓ) (inclusive).
VMS is expected to be ordered, although this is not explicitly checked!
"""
function timesubset(v::VMS, (tmin, tmax)::NTuple{2, I}) where {I<:Union{Signed,Unsigned}}
    irange = UnitRange(searchsortedfirst(v.T,tmin), searchsortedlast(v.T,tmax))
    return VMS(view(v.T,irange), view(v.Σ,irange), view(v.Z,irange), view(v.vals,irange,:), v.fields)
end

"""
    featureNumber(VMS)

Number of features of a [`VMS`](@ref) or it's stored data matrix.
"""
featureNumber(data::VMS) = size(data.vals)[2]
featureNumber(data::AbstractMatrix) = size(data)[2]

"""
    sampleNumber(VMS)

Number of samples of a [`VMS`](@ref) or it's stored data matrix.
"""
sampleNumber(data::VMS) = size(data.vals)[1]
sampleNumber(data::AbstractMatrix) = size(data)[1]

"""
    VMS[indices::AbstractArray{Integer}]

Subset a [`VMS`](@ref) in the time domain by providing an Array of indices.
(BitArrays, sorted Integer Arrays and Integer typed Ranges work)
"""
function Base.getindex(v::VMS, indices)
    return VMS(v.T[indices], v.Σ[indices], v.Z[indices], v.vals[indices,:], v.fields)
end

"""
    dropfeatures!(VMS, AbstractArray{<:AbstractString})

Return a [`VMS`](@ref) with the specified features removed.
"""
function dropfeatures(v::VMS, dropfeats::AbstractArray{<:AbstractString})
    mask = .!(in(dropfeats).(v.fields))
    return VMS(v.T, v.Σ, v.Z, v.vals[:,mask], v.fields[mask])
end

"""
    keepfeatures!(VMS, AbstractArray{<:AbstractString})

Return a VMS with all but the specified features removed.
Specified features that don't exist in the supplied VMS are silently ignored.
"""
function keepfeatures(v::VMS, keepfeats::AbstractArray{<:AbstractString})
    mask = in(keepfeats).(v.fields)
    return VMS(v.T, v.Σ, v.Z, v.vals[:,mask], v.fields[mask])
end

# TODO docstring
function empty(v::VMS{D}; keepfields::Bool = false) where {D}
    return VMS(eltype(D), keepfields ? v.fields : String[])
end

# end of VMS stuff

abstract type AbstractPragmaticModel end

"""
    TODO
"""
struct KnowledgeBase
    "0th level on the Knowledge Domain. This level contains the input LearnBlocks for all
    models on the 1st level. For any given model m on level 1, the associated learnblock
    is accessible as level_0[m.ID]"
    level_0::Dict{String,VMS}
    higherLevels::Vector{Dict{String,AbstractPragmaticModel}}
    lastusedUID::Dict{Int,Int}
    "If enablepile is true. the pile will receive all vector models of the input data which
    are not used in a given learning step."
    pile::Ref{VMS}
    "If enablewaste is true, the waste will receive all VMSs which failed to pass initial
    construction or reconstruction and all (feature-reduced) VMSs which drop out of the
    knowledge domain due to deconstruction."
    waste::Vector{VMS}
    "This saves the number of construction/reconstruction/deconstruction steps that were
    taken to build this knowledge base."
    workdone::Dict{Symbol,Int}
end
# defaults
function KnowledgeBase()
    KnowledgeBase(Dict{String,VMS}(), Vector{Dict{String,AbstractPragmaticModel}}(), Dict(1=>0),
        Ref{VMS}(VMS(Int)), Vector{VMS}(), Dict{Symbol,Int}(:construction => 0, :reconstruction => 0, :deconstruction => 0))
end

# indexing
Base.getindex(kb::KnowledgeBase, i) = i==0 ? kb.level_0 : kb.higherLevels[i]
#iterating over the whole lb (from bottom over all levels)
Base.iterate(kb::KnowledgeBase) = isempty(kb.higherLevels) ? nothing : (kb.higherLevels[1], 2)
Base.iterate(kb::KnowledgeBase, state) = state > length(kb.higherLevels) ? nothing : (kb.higherLevels[state], state+1)
Base.length(kb::KnowledgeBase) = length(kb.higherLevels)

function Base.show(io::IO, k::KnowledgeBase)
    print(io, "KnowledgeBase(")
    printstyled(io, "Level 0", bold = true)
    print(io, ": $(k.level_0), ")
    for (i, lvl) in enumerate(k.higherLevels)
        printstyled(io, "Level $(i)", bold = true)
        print(io, ": $(lvl), ")
    end
    printstyled(io, "lastusedUID", bold = true)
    print(io, ": $(k.lastusedUID))")
end
function Base.show(io::IO, ::MIME"text/plain", k::KnowledgeBase)
    print(io, "KnowledgeBase(\n")
    print(io, "Level 0: ")
    show(io, "text/plain", k.level_0)
    for (i, lvl) in enumerate(k.higherLevels)
        print(io, "\nLevel $(i): ")
        show(io, "text/plain", lvl)
    end
    print(io, "\nlastusedUID: ")
    show(io, "text/plain", k.lastusedUID)
    print(")")
end

function nextFreeUIDnumber(kb::KnowledgeBase, level::Int)
    new = get(kb.lastusedUID, level, 0) + 1 # get with default
    kb.lastusedUID[level] = new
    return new
end

function resetWorkdone!(kb::KnowledgeBase)
    kb.workdone[:construction] = 0
    kb.workdone[:reconstruction] = 0
    kb.workdone[:deconstruction] = 0
    kb
end

"""
    clearpile(kb::KnowledgeBase)

Return the pile of a knowledge base and empty it afterwards.
"""
function clearpile(kb::KnowledgeBase)
    pile = kb.pile[]
    kb.pile[] = empty(pile) # empty VMS of same type
    return pile
end

"""
    clearpile(kb::KnowledgeBase)

Return the waste of a knowledge base and empty it afterwards.
"""
function clearwaste(kb::KnowledgeBase)
    waste = copy(kb.waste)
    empty!(kb.waste)
    return waste
end

"""
    registerLB(KnowledgeBase, LearnBlock, ID)

Put a Learnblock into th 0th level of the given KnowledgeBase with Key: ID.
"""
function registerLB(kb::KnowledgeBase, winnerLB::VMS, ID::String)
    level0 = kb[0]
    level0[ID] = winnerLB
end

# enforce shape (n_samples, n_features) because predictors may throw out a flat array
# commented out because I think that hcat automatically deals with this
#_enforceShape(m::AbstractMatrix) = m
#_enforceShape(v::AbstractVector) = reshape(v, (length(v),1))

"""
    getsourcedata(AbstractPragmaticModel, KnowledgeBase)

Assuming the zeroth level of KnowledgeBase as source data, fetch all predictions from source models that will be input data for this model.
If the model resides on the first level in the KnowledgeBase, fetch all source vector models in this models temporal range from level zero.
"""
getsourcedata(mm::AbstractPragmaticModel, kb::KnowledgeBase) = _getsourcedata(Val(mm.level>1), mm, kb)

function _getsourcedata(::Val{true}, mm::AbstractPragmaticModel, kb::KnowledgeBase)
    # level > 1 so source are machine models
    modeldict = kb[mm.level-1]
    vals = [modelpredictions(modeldict[id], kb, mm.T) for id in mm.source] # added mm.T to
    # ensure that only values inside [Tmin, Tmax] are retrieved
    return Tuple(Iterators.flatten(vals))
end

function _getsourcedata(::Val{false}, mm::AbstractPragmaticModel, kb::KnowledgeBase)
    # level <= 1 so source is an index into the dictionary of blocks at level 0
    return viewDataAndTimes(kb[0][mm.ID])
end

# if multiple source pairs of Ts (timestamps) and Ss (source data blocks) are present, recursively intersect them
_timeintersect(T,S) = (T,S)
_timeintersect(T,S,TSx...) = _executetimeintersection(T,S,_timeintersect(TSx...))

# for the well-defined case of two time-ranges with matching source blocks, execute the time domain intersection
function _executetimeintersection(T1, S1, (T2, S2))
    # NOTE assumes sorted arrays and well-defined timestamps (same timestamps have same multiplicity and order of untracked Σ and Z metadata on both sides) (in the best case, timestamps are unique)
    imax = length(T1)
    jmax = length(T2)
    mask1 = falses(imax)
    mask2 = falses(jmax)
    i = j = 1
    while i<=imax && j<=jmax
        t1 = T1[i]
        t2 = T2[j]
        if t1==t2
            mask1[i] = true
            mask2[j] = true
            i+=1
            j+=1
        elseif t1<t2
            i+=1
        elseif t1>t2
            j+=1
        end
    end
    return ( T1[mask1] , hcat(S1[mask1,:],S2[mask2,:]) )
end

"""
    savetoKB(KnowledgeBase, AbstractPragmaticModel)

Register an AbstractPragmaticModel to a KnowledgeBase. The level into which the model is inserted is determined via model.level. Existing model/learnblock dependencies are not checked here!
"""
function savetoKB(kb::KnowledgeBase, mm::AbstractPragmaticModel)
    kb[mm.level][mm.ID] = mm
end

"""
    purgefromKB(KnowledgeBase, AbstractPragmaticModel)

Delete an AbstractPragmaticModel from a KnowledgeBase and recursively search and purge models that depended on it.
! Assumes that dependent models can only occur on the immediately higher level. (For a model on level 1, search level 2 for dependencies)
If on level 1, also delete the associated learnblock on level 0.
"""
function purgefromKB(kb::KnowledgeBase, model::AbstractPragmaticModel; enablewaste::Bool = false)
    level = model.level
    id = model.ID
    delete!(kb[level], id)
    if level==1
        if enablewaste && haskey(kb[0], id)
            push!(kb.waste, kb[0][id])
        end
        delete!(kb[0], id)
    end
    # NOTE assumes that dependent models can only occur at the immediately following level, which is true as of now but may change later (?)
    foreach( kb[level+1] ) do deppair
        dep = last(deppair) # extract model from key => value pair
        if model.ID ∈ dep.source
            purgefromKB(kb, dep; enablewaste)
        end
    end
end

# end of Knowledge Domain Methods

# Singleton Types for the different Knowledge Types for Dispatch
abstract type KnowledgeType end
struct Procedural<:KnowledgeType end
struct Conceptual<:KnowledgeType end
# and abbreviations used in UID and Z
domainSymbol(d::KnowledgeType) = error("Domain abbreviation not defined for domain: ", d)
domainSymbol(::Procedural) = "P"
domainSymbol(::Conceptual) = "C"
Base.show(io::IO, ::Procedural) = print(io, "Procedural")
Base.show(io::IO, ::Conceptual) = print(io, "Conceptual")

# error types for: predictionErrorScore
"""
    accuracyError(trues, predictions)

Return `1. - accuracy(trues, predictions)`.
"""
accuracyError(trues, predictions) = 1. - ScikitLearnBase.classifier_accuracy_score(trues, predictions)

"""
    MAPE(trues, predictions)

Return the MAPE loss function in %.
Let `X = trues`, `Y = predictions`, `n = length(X) = length(Y)`, return:
`M = 1/n * ∑ᵢ|(Xᵢ - Yᵢ)/(Xᵢ)| * 100`
"""
MAPE(trues, predictions) = sum(abs.( (trues .- predictions) ./ (trues)) ) / length(trues) * 100.

"""
    brokenMAPE(trues, predictions)

Return a broken variant of the MAPE loss function in %.
This was used in the script and is mainly used for comparison!
The problem with this is, that the absolute value is only extended over
the differences between truth and predictions and the division reintroduces
the signs of the truth vector. This results in a faulty summation! (cancellation)
Let `X = trues`, `Y = predictions`, `n = length(X) = length(Y)`, return:
`M = 1/n * ∑ᵢ|(Xᵢ - Yᵢ)|/(Xᵢ) * 100`
"""
brokenMAPE(trues, predictions) = sum(abs.(trues .- predictions) ./ (trues) ) / length(trues) * 100.

"""
maxAPE(trues, predictions)

Return the maximum absolute percentage error of corresponding values in `trues` and `predictions` in %.
Let `X = trues`, `Y = predictions`, return:
`M = max( ∀i∈{1,...,n}: |(Xᵢ - Yᵢ)/(Xᵢ)| )`
"""
maxAPE(trues, predictions) = maximum(abs.( (trues .- predictions) ./ trues) ) * 100.

"""
    RMSE(trues, predictions)

Return the root-mean-square error between `trues` and `predictions`,
i.e. let `X = trues`, `Y = predictions`, `n = length(X) = length(Y)`, return:
`√(∑ᵢ(Xᵢ - Yᵢ)² / n)`
"""
RMSE(trues, predictions) = √(sum((trues .- predictions) .^ 2) / length(trues))

"""
WIP! a structure to hold pragmatic machine models
"""
struct PragmaticMachineModel<:AbstractPragmaticModel
    "Model validity time range (Tₘᵢₙ, Tₘₐₓ)"
    T::NTuple{2, Int}
    "Model subject String"
    Σ::String
    "Model purpose String"
    Z::String
    "Model-ID (UID)"
    ID::String
    "Underlying machine model"
    model #Datentyp? Sci-Kit API hier schon referenzieren?
    "The level in the KnowledgeBase on which this model resides. (for usability purposes, it's implicitly contained in the ID and Z)"
    level::Int
    "Source model UIDs in the same KnowledgeBase on the previous level or selected Feature names of underlying vector models (if on level 1)"
    source::Vector{String}
    # XXX ggf mehr Felder, zB field-names (wie im VMS) ...
    targets
    targettimes::Vector{Int}
    """
        MachineModel((Tₘᵢₙ, Tₘₐₓ), reconstructionMethodName, domain, level, constructionMethodName, UIDNumber, model)

    Create an appropriate MachineModel from supplied metadata as per the paper standard.
    The Ts are timestamps; reconstructionMethodName and constructionMethodName are the names given to the used (re-)construction methods (as Strings).
    level refers to the level in the KnowledgeBase, UIDNumber is the next unused number on that level and model is a Scikit-API supporting object.
    """
    function PragmaticMachineModel(kb::KnowledgeBase, domain::KnowledgeType, level::Int, winnerLB::VMS, reconstructionMethodName::AbstractString, constructionMethodName::AbstractString, model, targets)
        source = winnerLB.fields
        UIDNumber = nextFreeUIDnumber(kb, level)
        T = extrema(winnerLB.T)
        id = join([domainSymbol(domain), level, UIDNumber], ".")
        if level==1
            registerLB(kb, winnerLB, id)
        end
        Σ = string(reconstructionMethodName)
        Z = join([domainSymbol(domain), level, constructionMethodName], ".")
        new(T,Σ,Z,id,model,level,source,targets,winnerLB.T)
    end
    # second constructor for regenerating models in a loaded kb
    PragmaticMachineModel(T,Σ,Z,id,model,level,source,targets,targettimes) = new(T,Σ,Z,id,model,level,source,targets,targettimes)
end

function Base.show(io::IO, m::PragmaticMachineModel)
    tmin, tmax = extrema(m.T)
    nfeatures = length(m.source)
    print(io, "PragmaticMachineModel(ID: $(repr(m.ID)), Tₘᵢₙ: $(tmin), Tₘₐₓ: $(tmax), ")
    print(io, "Σ: $(repr(m.Σ)), Z: $(repr(m.Z)), $(length(m.source)) input features")
    if nfeatures<=10 print(io, ": $(m.source)") end
    print(io, ", targets: $(summary(m.targets)), model: $(typeof(m.model)))")
end
function Base.show(io::IO, ::MIME"text/plain", m::PragmaticMachineModel)
    tmin, tmax = extrema(m.T)
    nfeatures = length(m.source)
    print(io, "PragmaticMachineModel( ID: $(repr(m.ID))\nTₘᵢₙ: $(tmin), Tₘₐₓ: $(tmax)\n")
    print(io, "Σ: $(repr(m.Σ)), Z: $(repr(m.Z))\n$(length(m.source)) input features: $(m.source)\n")
    print(io, "$(length(m.targets)) Targets: $(sprint(show, m.targets; context = :limit => true)), Model: ")
    printstyled(io, m.model, color=:light_black)
    print(io, ")")
end

"""
    modelpredictions(AbstractPragmaticModel, KnowledgeBase [, (Tmin, Tmax)])

Get model output for all valid timestamps. Optionally select a time range from all values.
"""
modelpredictions(mm::PragmaticMachineModel, _::KnowledgeBase) = (mm.targettimes, mm.targets)
function modelpredictions(mm::PragmaticMachineModel, _::KnowledgeBase, (tmin, tmax))
    Ts = mm.targettimes
    targets = mm.targets
    mask = (x-> tmin ≤ x ≤ tmax).(Ts)
    return (Ts[mask], targets[mask])
end

"""
    getsourceandtargets(mm::AbstractPragmaticModel, kb::KnowledgeBase)

#TODO
Return (T, sourceblock, targets) appropriate to the abstract pragmatic model.
"""
function getsourceandtargets(mm::PragmaticMachineModel, kb::KnowledgeBase)
    Tsource, source = _timeintersect(getsourcedata(mm, kb)...)
    Ttargets, targets = (mm.targettimes, mm.targets)
    @assert Tsource == Ttargets
    return (Ttargets, source, targets)
end

# end of PragmaticMachineModel methods

"""
    MinimalMachineModel

Like a regular [`PragmaticMachineModel`](@ref) but this one does not store it's targets from construction
and instead restores them from the source blocks and the stored supervised model if necessary.
"""
struct MinimalMachineModel<:AbstractPragmaticModel
    "Model validity time range (Tₘᵢₙ, Tₘₐₓ)"
    T::NTuple{2, Int}
    "Model subject String"
    Σ::String
    "Model purpose String"
    Z::String
    "Model-ID (UID)"
    ID::String
    "Underlying machine model"
    model #Datentyp? Sci-Kit API hier schon referenzieren?
    "The level in the KnowledgeBase on which this model resides. (for usability purposes, it's implicitly contained in the ID and Z)"
    level::Int
    "Source model UIDs in the same KnowledgeBase on the previous level or selected Feature names of underlying vector models (if on level 1)"
    source::Vector{String}
    # XXX ggf mehr Felder, zB field-names (wie im VMS) ...
    """
        MachineModel((Tₘᵢₙ, Tₘₐₓ), reconstructionMethodName, domain, level, constructionMethodName, UIDNumber, model)

    Create an appropriate MachineModel from supplied metadata as per the paper standard.
    The Ts are timestamps; reconstructionMethodName and constructionMethodName are the names given to the used (re-)construction methods (as Strings).
    level refers to the level in the KnowledgeBase, UIDNumber is the next unused number on that level and model is a Scikit-API supporting object.
    """
    function MinimalMachineModel(kb::KnowledgeBase, domain::KnowledgeType, level::Int, winnerLB::VMS, reconstructionMethodName::AbstractString, constructionMethodName::AbstractString, model, targets=nothing) # targets only here for compatibility
        source = winnerLB.fields
        UIDNumber = nextFreeUIDnumber(kb, level)
        T = extrema(winnerLB.T)
        id = join([domainSymbol(domain), level, UIDNumber], ".")
        if level==1
            registerLB(kb, winnerLB, id)
        end
        Σ = string(reconstructionMethodName)
        Z = join([domainSymbol(domain), level, constructionMethodName], ".")
        new(T,Σ,Z,id,model,level,source)
    end
end

function Base.show(io::IO, m::MinimalMachineModel)
    tmin, tmax = extrema(m.T)
    nfeatures = length(m.source)
    print(io, "MinimalMachineModel(ID: $(repr(m.ID)), Tₘᵢₙ: $(tmin), Tₘₐₓ: $(tmax), ")
    print(io, "Σ: $(repr(m.Σ)), Z: $(repr(m.Z)), $(length(m.source)) input features")
    if nfeatures<=10 print(io, ": $(m.source)") end
    print(io, ", model: $(typeof(m.model)))")
end
function Base.show(io::IO, ::MIME"text/plain", m::MinimalMachineModel)
    tmin, tmax = extrema(m.T)
    nfeatures = length(m.source)
    print(io, "MinimalMachineModel( ID: $(repr(m.ID))\nTₘᵢₙ: $(tmin), Tₘₐₓ: $(tmax)\n")
    print(io, "Σ: $(repr(m.Σ)), Z: $(repr(m.Z))\n$(length(m.source)) input features: $(m.source)\nModel: ")
    printstyled(io, m.model, color=:light_black)
    print(io, ")")
end

function modelpredictions(mm::MinimalMachineModel, kb::KnowledgeBase)
    (T,S) = _timeintersect(getsourcedata(mm, kb)...)
    return (T, predict(mm.model, S))
end

function modelpredictions(mm::MinimalMachineModel, kb::KnowledgeBase, (tmin, tmax))
    (Ts,S) = _timeintersect(getsourcedata(mm, kb)...)
    preds = predict(mm.model, S)
    mask = (x-> tmin ≤ x ≤ tmax).(Ts)
    return (Ts[mask], preds[mask])
end

function getsourceandtargets(mm::MinimalMachineModel, kb::KnowledgeBase)
    T, source = _timeintersect(getsourcedata(mm, kb)...)
    targets = predict(mm.model, source)
    return (T, source, targets)
end

# end of MinimalMachineModel methods

"""
    ConML.ParametersConML(; stuff...)

Struct holding options for a constructivist machine learning pipeline as constant fields.
Available parameters (with default value if any):
## General
- `Logger::AbstractLogger = NullLogger()`
- `verbose::Bool = true`
- `usePredictionsAsTargets::Bool = false`
- `highestLevel::Int = 9`
- `enablepile::Bool = true`
- `enablewaste::Bool = false`
- `learningDomain::KnowledgeType = Conceptual()`
## Learnblock selection
- `LearnBlockMinimum::Int`
- `SigmaZetaCutoff::Float64 = 0.2`
- `lscvBandwidth::Bool = true`: use lscv (true) or silvermans rule for bandwidth estimation in kde
- `UseLearnBlocks::Symbol = :biggest`: :all or :biggest
## Construction
- `MinCategorySize::Union{Int,Float64} = nothing`
## ComplexityReduction
- `MaxFeatures::Int = 10`
- `maxFilterFeatures::Int`: formerly MaxFilterX 
- `maxFilterSamples::Int`: formerly MaxFilterY 
- `MaxModelReduction::Bool = false`
## Reconstruction
- `testSetPercentage::Float64 = 0.33`: % of test set for train/test-split
- `MinTestAccuracy::Float64 = 0.8`
- `MaxTestErrorAvg::Float64 = 0.1`
- `MaxTestErrorMax::Float64 = 1.0`
- `KrippendorffMetric::Symbol = (learningDomain isa Conceptual ? :nominal : :interval )`
- `MinReliability::Float64 = 0.8`
## Deconstruction
- `AllowWeakReliability::Bool = false`: allow negative krippendorff values to also count as different for the purpose of TΣ deconstruction
- `deconstructionStrategy::Symbol = :conservative`: must be :conservative, :integrative or :opportunistic
- `deconstructionMode::Symbol = :minimal`: must be :minimal or :maximal
- `MaxDistanceT::Float64 = 1.0`: ΣZ
- `FullTolerance::Float64 = 0.1`: TΣZ
- `TΣTolerance::Float64 = 0.`: TΣ
- `MaxTimeConflicts::Union{Int,Float64} = 0.1`: maximum number of time conflicts in deconstruction for full and SigmaZ
"""
Base.@kwdef struct ParametersConML
    # [General]
    Logger::AbstractLogger = NullLogger()
    verbose::Bool = true
    usePredictionsAsTargets::Bool = false
    highestLevel::Int = 9
    enablepile::Bool = true
    enablewaste::Bool = false
    learningDomain::KnowledgeType = Conceptual()
    # Preprocessing and Block drawing are up to the user
    # [Learnblock selection]
    LearnBlockMinimum::Int
    SigmaZetaCutoff::Float64 = 0.2
    lscvBandwidth::Bool = true # use lscv (true) or silvermans rule for bandwidth estimation in kde
    UseLearnBlocks::Symbol = :biggest # all or biggest
    # [Construction]
    # maxCategories::Int dropped because cluster number is bound to classificators here, which is more flexible
    MinCategorySize::Union{Int,Float64} = nothing
    # maxModelTargets::Int = 1 dropped for the same reason as maxCategories
    # [ComplexityReduction]
    MaxFeatures::Int = 10
    maxFilterFeatures::Int # MaxFilterX bei Thomas
    maxFilterSamples::Int # MaxFilterY bei Thomas
    MaxModelReduction::Bool = false
    # [Reconstruction]
    #predictionErrorScore::Function = (learningDomain isa Conceptual ? accuracyError : MAPE )
    testSetPercentage::Float64 = 0.33 # % of test set for train/test-split
    MinTestAccuracy::Float64 = 0.8
    MaxTestErrorAvg::Float64 = 0.1
    MaxTestErrorMax::Float64 = 1.
    #ReliabilitySample::Float64 = 0.1 bis jetzt ignoriert, ggf später dazufügen und vor Krippendorff subsamplen
    KrippendorffMetric::Symbol = (learningDomain isa Conceptual ? :nominal : :interval )
    MinReliability::Float64 = 0.8
    # ReduceModelRedundancy::Bool = false dropped temporarily
    # [Deconstruction]
    AllowWeakReliability::Bool = false # allow negative krippendorff values to also count as different for the purpose of TΣ deconstruction
    deconstructionStrategy::Symbol = :conservative # must be conservative, integrative or opportunistic
    deconstructionMode::Symbol = :minimal # must be minimal or maximal
    MaxDistanceT::Float64 = 1. #ΣZ
    FullTolerance::Float64 = 0.1 #TΣZ
    TΣTolerance::Float64 = 0. #TΣ
    # ForceTimeExpansion::Bool = true dropped temporarily
    MaxTimeConflicts::Union{Int,Float64} = 0.1 # maximum number of time conflicts in deconstruction for full and SigmaZ

    # explicit declaration of standard constructor with invariants check
    function ParametersConML( # arguments below for readability
        Logger::AbstractLogger,
        verbose::Bool,
        usePredictionsAsTargets::Bool,
        highestLevel::Int,
        enablepile::Bool,
        enablewaste::Bool,
        learningDomain::KnowledgeType,
        LearnBlockMinimum::Int,
        SigmaZetaCutoff::Float64,
        lscvBandwidth::Bool,
        UseLearnBlocks::Symbol,
        #maxCategories::Int, dropped
        MinCategorySize::Union{Int,Float64,Nothing},
        #maxModelTargets::Int, dropped
        MaxFeatures::Int,
        maxFilterFeatures::Int,
        maxFilterSamples::Int,
        MaxModelReduction::Bool,
        #predictionErrorScore::Function,
        testSetPercentage::Float64,
        MinTestAccuracy::Float64,
        MaxTestErrorAvg::Float64,
        MaxTestErrorMax::Float64,
        #ReliabilitySample::Float64,
        KrippendorffMetric::Symbol,
        MinReliability::Float64,
        # ReduceModelRedundancy::Bool,
        AllowWeakReliability::Bool,
        deconstructionStrategy::Symbol,
        deconstructionMode::Symbol,
        MaxDistanceT::Float64,
        FullTolerance::Float64,
        TΣTolerance::Float64,
        # ForceTimeExpansion::Bool, dropped
        MaxTimeConflicts::Union{Int,Float64} ) #end of arguments

        # check if all arguments are valid
        ### Logger no check
        ### verbose no check

        ### usePredictionsAsTargets
        # until both are implemented ~ should be resolved
        # if usePredictionsAsTargets==false
        #     error("usePredictionsAsTargets = false not implemented yet")
        # end

        ### highestLevel
        if highestLevel<0
            throw(DomainError(highestLevel, "Non-negative Integer expected for highestLevel"))
        elseif highestLevel==0
            @warn "Parameter highestLevel set to 0, be aware that nothing will be learned this way!"
        end
        ### enablepile no check
        ### enablewaste no check
        ### learningDomain no check
        ### LearnBlockMinimum
        if LearnBlockMinimum<0
            throw(DomainError(LearnBlockMinimum, "Non-negative Integer expected for LearnBlockMinimum"))
        end
        ### SigmaZetaCutoff
        if SigmaZetaCutoff<0. || SigmaZetaCutoff>100.
            throw(DomainError(SigmaZetaCutoff, "Float in [0,1] expected for SigmaZetaCutoff"))
        elseif SigmaZetaCutoff>1.
            @warn "Float in [0,1] expected for SigmaZetaCutoff. Received value $(SigmaZetaCutoff). Assuming % and dividing by 100!"
            SigmaZetaCutoff = SigmaZetaCutoff/100
        end
        ### lscvBandwidth no check
        ### UseLearnBlocks
        if UseLearnBlocks ∉ (:all, :biggest) # it's a "not in" symbol
            throw(DomainError(UseLearnBlocks, "UseLearnBlocks must be :all or :biggest"))
        end
        ### maxCategories dropped
        #if maxCategories<1
        #    throw(DomainError(maxCategories, "Positive Integer expected for maxCategories"))
        #end
        ### MinCategorySize
        if learningDomain == Conceptual()
            if isnothing(MinCategorySize)
                throw(DomainError(MinCategorySize, "Parameter MinCategorySize must be specified manually in the conceptual domain."))
            elseif typeof(MinCategorySize) == Int
                if MinCategorySize<0
                    throw(DomainError(MinCategorySize, "Non-negative Integer or Float in [0,1] expected for MinCategorySize"))
                end
            else #typeof(MinCategorySize)==Float64
                if MinCategorySize<0. || MinCategorySize>1.
                    throw(DomainError(MinCategorySize, "Non-negative Integer or Float in [0,1] expected for MinCategorySize"))
                end
            end
        else  # don't check if not in conceptual domain, set anything that doesn't throw if not set
            if isnothing(MinCategorySize)
                MinCategorySize = -1
            end
        end
        ### maxModelTargets dropped
        # if maxModelTargets<1
        #     throw(DomainError(maxModelTargets, "Positive Integer expected for maxModelTargets"))
        # end
        ### MaxFeatures
        if MaxFeatures<1
            throw(DomainError(MaxFeatures, "Positive Integer expected for MaxFeatures"))
        end
        ### maxFilterFeatures
        if maxFilterFeatures<0
            throw(DomainError(maxFilterFeatures, "Non-negative Integer expected for maxFilterFeatures"))
        end
        ### maxFilterSamples
        if maxFilterSamples<0
            throw(DomainError(maxFilterSamples, "Non-negative Integer expected for maxFilterSamples"))
        end
        ### MaxModelReduction no check
        ### predictionErrorScore no check
        ### testSetPercentage
        if testSetPercentage<0. || testSetPercentage>1.
            throw(DomainError(testSetPercentage, "Float in [0,1] expected for testSetPercentage"))
        end
        ### MinTestAccuracy
        if MinTestAccuracy<0. || MinTestAccuracy>1.
            throw(DomainError(MinTestAccuracy, "Float in [0,1] expected for MinTestAccuracy"))
        end
        ### MaxTestErrorAvg
        if MaxTestErrorAvg<0.
            throw(DomainError(MaxTestErrorAvg, "Non-negative floating-point number expected for MaxTestErrorAvg"))
        end
        ### MaxTestErrorMax
        if MaxTestErrorMax<0.
            throw(DomainError(MaxTestErrorMax, "Non-negative floating-point number expected for MaxTestErrorMax"))
        end
        ### KrippendorffMetric
        if KrippendorffMetric ∉ Krippendorff.KNOWNMETRICS
            throw(DomainError(KrippendorffMetric, "KrippendorffMetric must be one of $(Krippendorff.KNOWNMETRICS)"))
        end
        ### MinReliability
        if MinReliability<0. || MinReliability>1.
            throw(DomainError(MinReliability, "Float in [0,1] expected for MinReliability"))
        end
        ### ReduceModelRedundancy no check
        ### AllowWeakReliability no check
        ### deconstructionStrategy
        if deconstructionStrategy ∉ (:conservative, :integrative, :opportunistic)
            throw(DomainError(deconstructionStrategy, "deconstructionStrategy must be one of [:conservative, :integrative, :opportunistic]"))
        end
        ### deconstructionMode
        if deconstructionMode ∉ (:minimal, :maximal)
            throw(DomainError(deconstructionMode, "deconstructionMode must be :minimal or :maximal"))
        end
        ### MaxDistanceT
        if MaxDistanceT<0. || MaxDistanceT>1.
            throw(DomainError(MaxDistanceT, "Float in [0,1] expected for MaxDistanceT"))
        end
        ### FullTolerance
        if FullTolerance<0. || FullTolerance>1.
            throw(DomainError(FullTolerance, "Float in [0,1] expected for FullTolerance"))
        end
        ### TΣTolerance
        if TΣTolerance<0. || TΣTolerance>1.
            throw(DomainError(TΣTolerance, "Float in [0,1] expected for TΣTolerance"))
        end
        ### ForceTimeExpansion no check
        ### MaxTimeConflicts
        if typeof(MaxTimeConflicts)==Int
            if MaxTimeConflicts<0
                throw(DomainError(MaxTimeConflicts, "Non-negative Integer or Float in [0,1] expected for MaxTimeConflicts"))
            end
        else #typeof(MaxTimeConflicts)==Float64
            if MaxTimeConflicts<0. || MaxTimeConflicts>1.
                throw(DomainError(MaxTimeConflicts, "Non-negative Integer or Float in [0,1] expected for MaxTimeConflicts"))
            end
        end

        # if everything is okay, instantiate normally
        new(Logger,
        verbose,
        usePredictionsAsTargets,
        highestLevel,
        enablepile,
        enablewaste,
        learningDomain,
        LearnBlockMinimum,
        SigmaZetaCutoff,
        lscvBandwidth,
        UseLearnBlocks,
        #maxCategories, dropped
        MinCategorySize,
        #maxModelTargets, dropped
        MaxFeatures,
        maxFilterFeatures,
        maxFilterSamples,
        MaxModelReduction,
        #predictionErrorScore,
        testSetPercentage,
        MinTestAccuracy,
        MaxTestErrorAvg,
        MaxTestErrorMax,
        KrippendorffMetric,
        MinReliability,
        #ReduceModelRedundancy, dropped
        AllowWeakReliability,
        deconstructionStrategy,
        deconstructionMode,
        MaxDistanceT,
        FullTolerance,
        TΣTolerance,
        #ForceTimeExpansion, dropped
        MaxTimeConflicts)
    end # std inner constructor
end

Base.show(io::IO, c::ParametersConML) = print(io, "ParametersConML(", join([string(par)*" ==> "*string(getproperty(c, par)) for par in propertynames(c)], ", "), ")")
Base.show(io::IO, ::MIME"text/plain", c::ParametersConML) = print(io, "ParametersConML:",["\n$(par) ==> $(getproperty(c, par))" for par in propertynames(c)]...)

"""
    changeparameters(ParametersConML; kwargs...)

Return a new ParametersConML struct that retains all those values from the input which are not explicitly supplied as keyword arguments.
Allows easy changing of single parameters. Keep in mind that this function returns a copy and does not modify the input ParametersConML.
"""
function changeparameters(par::ParametersConML; kwargs...)
    pars = [Pair(propname, getproperty(par, propname)) for propname in propertynames(par)]
    for kwpair in kwargs
        i = findnext(p->first(p)==first(kwpair), pars, 1)
        if isnothing(i)
            error("$(first(kwpair)) is not a valid property for ParametersConML")
        else
            pars[i] = kwpair
        end
    end
    return ParametersConML(last.(pars)...)
end

"""
    saveParameters(path::AbstractString, ParametersConML)
    saveParameters(output::IO, ParametersConML)

Write the contents of a `ParametersConML` struct to a file or IO.
"""
saveParameters(handle::Union{AbstractString, IO}, par::ParametersConML) = write(handle, join( [string(propname)*"\t\t"*string(getproperty(par, propname)) for propname in propertynames(par)], "\n"))
#TODO check that no weird things happen for e.g. Logger and KnowlegeType

# Add alternative names for config loading here, key is how it is called in julia, value is a list of alternatives. Only applies for loadParameters function
# every key is an alias of itself for case insensitive detection later
const parameterAliases = Dict{String,Vector{String}}(
                                "Logger" => ["Logger", ] ,
                                "verbose" => ["verbose", ] ,
                                "usePredictionsAsTargets" => ["usePredictionsAsTargets", ] ,
                                "highestLevel" => ["highestLevel", "highest_level","LearningLevel",] ,
                                "enablepile" => ["enablepile", ] ,
                                "enablewaste" => ["enablewaste", ] ,
                                "learningDomain" => ["learningDomain", "LearningDomain",] ,
                                "LearnBlockMinimum" => ["LearnBlockMinimum", "learn_block_minimum",] ,
                                "SigmaZetaCutoff" => ["SigmaZetaCutoff", "sigma_zeta_cutoff",] ,
                                "lscvBandwidth" => ["lscvBandwidth", ] ,
                                "UseLearnBlocks" => ["UseLearnBlocks", ] ,
                                "MinCategorySize" => ["MinCategorySize", "min_category_size",] ,
                                # "maxModelTargets" => ["max_model_targets","MaxModelTargets",] ,
                                "MaxFeatures" => ["MaxFeatures", "max_features",] ,
                                "maxFilterFeatures" => ["maxFilterFeatures", "max_filter_x","MaxFilterX",] ,
                                "maxFilterSamples" => ["maxFilterSamples", "max_filter_y","MaxFilterY",] ,
                                "MaxModelReduction" => ["MaxModelReduction", "max_model_reduction",] ,
                                #"predictionErrorScore" => ["predictionErrorScore", ] ,
                                "testSetPercentage" => ["testSetPercentage", ] ,
                                "MinTestAccuracy" => ["MinTestAccuracy", "min_test_accuracy",] ,
                                "MaxTestErrorAvg" => ["MaxTestErrorAvg", "max_test_error_avg",] ,
                                "MaxTestErrorMax" => ["MaxTestErrorMax", "max_test_error_max ",] ,
                                "KrippendorffMetric" => ["KrippendorffMetric", ] ,
                                "MinReliability" => ["MinReliability", "min_reliability",] ,
                                # "ReduceModelRedundancy" => ["reduce_model_redundancy",] ,
                                "AllowWeakReliability" => ["AllowWeakReliability", "allow_weak_reliability",] ,
                                "deconstructionStrategy" => ["deconstructionStrategy", "deconst_strategy","DeconstStrategy",] ,
                                "deconstructionMode" => ["deconstructionMode", "deconst_mode","DeconstMode",] ,
                                "MaxDistanceT" => ["MaxDistanceT", "deconst_max_distance_t","DeconstMaxDistanceT",] ,
                                "FullTolerance" => ["FullTolerance", "deconst_full_tolerance","DeconstFullTolerance",] ,
                                "TΣTolerance" => ["TΣTolerance", ],
                                # "ForceTimeExpansion" => ["force_time_expansion",] ,
                                "MaxTimeConflicts" => ["MaxTimeConflicts", ]
                                )

# here is the dict that is actually used, a reversed variant of the above for easier iteration
# also, lowercases all aliases for case insensitive detection later
function _reverseAliasDict(dic::Dict{String,Vector{String}})
    revdict = Dict{String,String}()
    for (k, vs) in dic
        for v in vs
            revdict[lowercase(v)] = k
        end
    end
    return revdict
end
const reverseAliasDict = _reverseAliasDict(parameterAliases)

"""
    loadParameters(path::AbstractString, verboseLoading::Bool = true; kwargs...)
    loadParameters(input::IO, verboseLoading::Bool = true; kwargs...)

Take a file name or IO and load its contents into a `ParametersConML` struct if possible.
`verboseLoading` controlls the output of progress and non-error messages.
Any keyword arguments will be forwarded to the `ParametersConML` constructor.
If keyword arguments and the loaded content interfere, keyword arguments take precedence.
"""
function loadParameters(handle::Union{AbstractString, IO}, verboseLoading::Bool = true; inputkwargs...)
    fields = string.(fieldnames(ParametersConML))
    types = fieldtypes(ParametersConML)
    inputs = Vector{Pair{String, String}}()
    for line in eachline(handle)
        content = strip(line)
        if startswith(content, "[")
            # settings block delimiter
            if verboseLoading println("Reading:\t\t$(content)") end
            continue
        else
            tokens = split(content)
            if length(tokens) == 0
                continue
            elseif length(tokens) == 1
                println("Malformed input line:\t$(content)")
                continue
            else
                # canonize the keys via alias lookup
                k = reverseAliasLookup(tokens[1])

                if occursin("=", tokens[2])
                    if length(tokens) >= 3
                        if k ∈ fields
                            v = lowercase(tokens[3])
                            p = Pair(k, v)
                            if verboseLoading println("Read input pair:\t✓  $(p)") end
                            push!(inputs, p)
                        else
                            if verboseLoading println("Ignored line:\t\t⨯  $(content)") end
                        end
                    else
                        println("Malformed input line:\t$(content)")
                        continue
                    end
                else
                    if k ∈ fields
                        v = lowercase(tokens[2])
                        p = Pair(k, v)
                        if verboseLoading println("Read input pair:\t✓  $(p)") end
                        push!(inputs, p)
                    else
                        if verboseLoading println("Ignored line:\t\t⨯  $(content)") end
                    end
                end
            end
        end
    end
    if verboseLoading println("Done reading input. Parsing Strings to appropriate types.") end
    kwargs = Dict{Symbol, Any}()
    for (k,v) in inputs
        sym = Symbol(k)
        val = missing
        parsetype = types[findfirst(==(k), fields)]
        if parsetype isa Union && parsetype <: Union{Integer, AbstractFloat}
            # the rationale here is to always try the int type first, because parsing an into to a float is wrong but works, whereas parsing a float as an int can be catched
            if parsetype.a <: Integer # a is int type, b is float type
                try
                    val = parse(parsetype.a, v)
                catch ArgumentError
                    val = parse(parsetype.b, v)
                end
            else # a is float type, b is int type
                try
                    val = parse(parsetype.b, v)
                catch ArgumentError
                    val = parse(parsetype.a, v)
                end
            end
        elseif parsetype == Bool
            if lowercase(v) ∈ [ "true", "yes", "1"]
                val = true
            elseif lowercase(v) ∈ [ "false", "no", "0"]
                val = false
            else
                error("Couldn't parse $(v) to Boolean for input: $(Pair(k,v))")
            end
        elseif parsetype <: Number
            val = parse(parsetype, v)
        elseif parsetype == Symbol
            val = Symbol(v)
        elseif parsetype == KnowledgeType
            if lowercase(v) ∈ ["p", "proc", "procedural"]
                val = Procedural()
            elseif lowercase(v) ∈ ["c", "con", "conceptual"]
                val = Conceptual()
            else
                error("Can't determine KnowledgeType from input $(v)")
            end
        elseif parsetype == AbstractLogger
            @warn "Logger can't be set this way (yet), value set to NullLogger. Change later if necessary."
            val = NullLogger()
        elseif parsetype == Function && k == "predictionErrorScore"
            if lowercase(v) == "mape"
                val = MAPE
            elseif lowercase(v) ∈ ["accuracyError", "accuracy", "acc"]
                val = accuracyError
            elseif startswith(v, "#")
                @warn "Can't reconstruct anonymous function, predictionErrorScore set to default value"
                continue
            else
                @warn "Unknown error function, predictionErrorScore set to default value"
                continue
            end
        end

        if ismissing(val)
            error("Could not parse $(val) as $(parsetype), type unknown or function incomplete.")
        else
            push!(kwargs, Pair(sym, val))
        end
    end
    # combine inputkwargs and parsed kwargs, explicit input takes precedence
    for (k,v) in inputkwargs
        if (verboseLoading && haskey(kwargs, k)) println("Loaded parameter $(k) overwritten by explicit argument.") end
        kwargs[k] = v
    end
    try
        if verboseLoading println("Parsing successful, checking for complete parameter list.") end
        return ParametersConML(; kwargs...)
    catch UndefKeywordError
        @warn "Some parameters without default values could not be determined.\nThis happens either because they are missing in the input or failed to parse correctly.\nYou can always supplement missing values as keyword arguments to loadParameters."
        rethrow()
    end
end

function allequal(args...)
    elem1 = first(args)
    for rec in args
        if rec != elem1
            return false
        end
    end
    return true
end

function reverseAliasLookup(str::AbstractString)
    key::String = str
    if haskey(reverseAliasDict, lowercase(key))
        key = reverseAliasDict[lowercase(key)]
    end
    return key
end
