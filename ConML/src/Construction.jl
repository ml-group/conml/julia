#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConML.jl and
# may not behave as expected when used on it's own

#TODO mention rescale
"""
WIP! a callable object holding the necessary configurational values to perform construction
initialize once with the configuration you want and call onto learning blocks (or so it is intended)
""" # TODO docstring, mention shortdefaultnames
struct Construct{T}
    classifiers::T
	names::Vector{String}
	rescale

    function Construct(cs, names::AbstractVector{<:AbstractString}; rescale = unitrescale)
        if length(cs)<=1
            throw(AssertionError(string("Two or more classifiers needed for Construction, got: ", length(cs))))
        end
		if length(cs) != length(names)
			throw(AssertionError("Number of classifiers and assigned names for Construct must match!"))
		end
		ct = Tuple(collect(cs)) # tuple for types
		namesvector = [string(name) for name in names] #enforce type Vector{String}
		new{typeof(ct)}(ct, namesvector, rescale)
    end
end
Construct(cs; shortdefaultnames::Bool = false, kwargs...) = Construct(cs, generateNamesForEstimators(cs, :construction, shortdefaultnames); kwargs...)
Construct(pairsStringEst::AbstractVector{Pair{String,E}}; kwargs...) where {E} = Construct(last.(pairsStringEst), first.(pairsStringEst); kwargs...)
Construct(pairsEstString::AbstractVector{Pair{E,String}}; kwargs...) where {E} = Construct(first.(pairsEstString), last.(pairsEstString); kwargs...)
Construct(dictStringEst::Dict{String,E}; kwargs...) where {E} = Construct([(key => value) for (key,value) in dictStringEst]; kwargs...)
Construct(dictEstString::Dict{E,String}; kwargs...) where {E} = Construct([(key => value) for (key,value) in dictEstString]; kwargs...)

"""
    myConstructor = Construct(iter_of_classifiers)
    returned_model_candidates = myConstructor(learnblock)

Execute a construction process with the specified configuration in myConstructor.
Classifiers must implement the common ScikitLearn Interface (at least fit! and transform).
Returns a Tuple(learnblock, Array_of_model_candidates).
"""
function (c::Construct)(kb, par::ParametersConML, learnblock::VMS)
	if par.verbose println("called Construction with ", length(c.classifiers), " classifiers for a learnblock.") end
	# get data field from VMS, train classifiers
	block = learnblock.vals
	modelcandidates = [clone(classifier) for classifier in c.classifiers]

	modelnames, targetsvec = _construction_loop(par.learningDomain, block, modelcandidates,
												c.names, length(learnblock.T),
												par.verbose, par.MinCategorySize, c.rescale)
	# log
	writelog(par.Logger, ConstructionLog(), featureNumber(block), sampleNumber(block), c.names)
	kb.workdone[:construction] += length(modelnames)
	# shortcut to abort if nothing passed, else wrap everything for reconstruction and return
	if isempty(modelnames)
		# if waste is enabled, record non-constructible block
		if par.enablewaste
			push!(kb.waste, nothingtuple[2])
		end
		return nothing
	end
	return (learnblock, targetsvec, modelnames)
end

# Loop for conceptual knowledge
function _construction_loop(::Conceptual, block, modelcandidates,
							candidatenames, learnblocksize,
							verbose::Bool, MinCategorySize, _)

	threshold = floor(Int, MinCategorySize isa Int ?
				MinCategorySize :
				MinCategorySize*learnblocksize)
	modelnames::Vector{String} = Vector{String}()
	targetsvectors::Vector{Vector{Int}} = Vector{Vector{Int}}()

	for (candid, candidatename) in zip(modelcandidates, candidatenames)
		# get predictions
		# NOTE: this is shitty, because there is no general rule what "transform" and
		# "predict" do for clustering (at least no rule that everyone follows)
		# example: Scikit's KMeans gives clusters on predict and cluster-distance space
		# from transform, whereas LowRankModels' KMeans tries to give clusters on
		# predict (it's broken at the time of this comment) and a one-hot encoding
		# of the clustering on transform. Other ScikitLearn clustering algos
		# accept neither fit nor predict (AgglomerativeClustering for example)
		#  but store predictions as .labels_ attributes and know only fit_predict!
		# as a proper method. So the current approach is brittle and ugly:
		# - use fit_predict! if possible
		# - use fit_transform! if this breaks
		# - unify the prediction format at the end (collapse to cluster-vector)
		# - cry in the corner

		# outsourced to separate function
		predictions = conceptual_eval_predictions(candid, candidatename, block)
		clustersizes = counts(predictions)
		keep_clusternumber = length(clustersizes) > 1
		keep_sizes = all(>(threshold), clustersizes)
		if verbose
			println("Candidate: $(candidatename)")
			show(stdout, "text/plain", clustersizes)
			println()
			if !keep_sizes println("Dropped due to not fulfilling minimal category size of ", threshold) end
			if !keep_clusternumber println("Dropped because only a single big cluster was generated.") end
		end
		if keep_clusternumber & keep_sizes
			push!(modelnames, candidatename)
			push!(targetsvectors, predictions)
		end
	end
	return (modelnames, targetsvectors)
end

function conceptual_eval_predictions(candid, candidatename, block)
	# best effort: try fit_predict!, if this doesn't work try fit_transform!, if not throw something
	# normalize the most common output formats to a simple vector of cluster labels
	rawpredictions = nothing
	try
		rawpredictions = fit_predict!(candid, block)
	catch predict_ex
		try
			rawpredictions = fit_transform!(candid, block)
		catch transform_ex
			msg_ex = ErrorException("""
			Failed to generate cluster assignments from the provided clustering method: $(candidatename)
			One of the ScikitLearnBase functions transform, fit_transform!, predict or fit_predict! needs to work with the provided method in order to proceed.
			""")
			throw(CompositeException([msg_ex, predict_ex, transform_ex]))
		end
	end
	# assuming we came this far, the remaining task is normalizing the different output formats that may occur
	if rawpredictions isa AbstractVector # label vector
		return to_canonical_cluster_labels(rawpredictions)::Vector{Int}
	elseif rawpredictions isa AbstractMatrix
		if size(rawpredictions, 1) == 1 || size(rawpredictions, 2) == 1 # vector reshaped to a matrix
			return to_canonical_cluster_labels(rawpredictions[:])::Vector{Int}
		else # probably one-hot or probabilistic encoding, both give right results here. (Anything else may give garbage results)
			return Int[argmax(r) for r in eachrow(rawpredictions)]
		end
	else
		error("""
		The clustering method: $(candidatename)
		returned an unexpected output format of size: $(size(rawpredictions)), type: $(typeof(rawpredictions))
		""")
	end
end

to_canonical_cluster_labels(rawpredictions::Vector{Int}) = rawpredictions
function to_canonical_cluster_labels(rawpredictions)
	# whatever is provided as cluster labels here, record all uniques and transform to an Int vector
	labels = unique(rawpredictions)
	mapping = Dict(reverse.(collect(pairs(labels))))
	return [mapping[r] for r in rawpredictions]
end

# Loop for procedural knowledge
function _construction_loop(::Procedural, block, modelcandidates,
							candidatenames, learnblocksize,
							verbose::Bool, _, rescale )

	modelnames::Vector{String} = Vector{String}()
	targetsvectors::Vector{Vector{Float64}} = Vector{Vector{Float64}}()
	for (candid, candidatename) in zip(modelcandidates, candidatenames)
		# get predictions
		rawpredictions = fit_transform!(candid, block)
		# MinCategorySize can't be checked here obviously.
		# Instead, models may choose (possibly via wrapper)
		# to return nothing if their prediction should not be used
		keep::Bool = !isnothing(rawpredictions)

		if keep
			if size(rawpredictions, 2) == 1
				push!(modelnames, candidatename)
				push!(targetsvectors, rawpredictions)
			else
				# Split models here, 1 for each target column
				for (i, pred) in enumerate(eachcol(rawpredictions))
					newname = string(candidatename,"_Dim",i)
					push!(modelnames, newname)
					# apply selected rescaling if any
					push!(targetsvectors, rescale(pred))
				end
			end
		else
			if verbose println("Model discarded because construction quality control failed: $(candidatename)") end
		end
	end
	return (modelnames, targetsvectors)
end

"""
	constructionlikeformatter(Learnblock, Targets)

Take an existing learnblock and existing target values and return an output suitable for Reconstruction like a full Construction would.
""" # TODO update docstring
function constructionlikeformatter(learnblock::VMS, targetsvectors, Z)
	conname = last(split(Z, ".")) # extract construction classifier name (or abbreviation) from Z string because Reconstruction reformats it again
	# wrap everything in arrays because reconstruction expects it
	return ([learnblock,], [targetsvectors,], [conname,])
end

"""
	unitrescale(input; offset = 0.001)

Default rescaling function. This will attempt to linearly map all values to the range [0,1].

If offset is not 0, the destination range will be symmetrically narrowed to [+offset, 1-offset]
in order to prevent extreme values (particularly exact 0s).
"""
function unitrescale(input; offset = 0.001)
	min, max = extrema(input)
	min == max && return input # degenerate case, would create a lot of infs otherwise
	reductionfactor = (1 - 2*offset) / (max - min)
	return ((input .- min) .* reductionfactor) .+ offset
end
