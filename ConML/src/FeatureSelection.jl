#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConML.jl and
# may not behave as expected when used on it's own

"""
    FeatureSelector(embeddedMethod, filterMethod)

A callable object holding the necessary configurational values to perform feature selection,
namely a filter method and an embedded method. Both methods should be functions which will
receive a data block, a target vector and a kwargs dictionary that is guaranteed to contain
at least the the `MaxFeatures` parameter and the `testSetPercentage` for train/test splits.
Remember that the decision which method to apply depends on the ParameterStruct in the Learner.
""" # TODO docstring revise, correct signature, make # Implementation section
struct FeatureSelector{E<:Function, F<:Function, G<:Union{Nothing,Function}}
    embeddedMethod::E
    embeddedMethodName::String
    filterMethod::F
    filterMethodName::String
    finalPass::G
    finalPassName::String
    kwargs::Dict{Symbol,Any}
    function FeatureSelector(emb::Pair{String,<:Function}, filt::Pair{String,<:Function}, final::Union{Nothing,Pair{String,<:Function}}; kwargs...)
        embeddedMethodName, embeddedMethod = emb
		E = typeof(embeddedMethod)
        filterMethodName, filterMethod = filt
		F = typeof(filterMethod)
        finalPassName, finalPass = isnothing(final) ? ("",nothing) : final
		G = typeof(finalPass)
        return new{E,F,G}(embeddedMethod, embeddedMethodName, filterMethod, filterMethodName, finalPass, finalPassName, kwargs)
    end
end
FeatureSelector(emb::Function, filt::Function; kwargs...) = FeatureSelector(Pair("embedded",emb), Pair("filter",filt); kwargs...) #Default Names
FeatureSelector(emb::Function, filt::Function, final::Function; kwargs...) = FeatureSelector(Pair("embedded",emb), Pair("filter",filt), Pair("final",final); kwargs...) #Default Names
FeatureSelector(embl, embr, filtl, filtr, finall = nothing, finalr = nothing; kwargs...) = FeatureSelector(Pair(embl, embr), Pair(filtl, filtr), isnothing(finall) ? nothing : Pair(finall, finalr); kwargs...)
FeatureSelector(emb::Pair{<:Function,String}, filt::Pair{<:Function,String}, final::Union{Nothing,Pair{<:Function,String}}; kwargs...) = FeatureSelector(reverse(emb), reverse(filt), reverse(final); kwargs...)

function FeatureSelector(; embeddedMethod::Function, embeddedMethodName::String, filterMethod::Function, filterMethodName::String, finalPass::Union{Nothing, Function} = nothing, finalPassName::String = "", kwargs...)
    FeatureSelector(Pair(embeddedMethodName,embeddedMethod), Pair(filterMethodName,filterMethod), isnothing(finalPass) ? nothing : Pair(finalPassName,finalPass); kwargs...)
end
#default values for feature reduction moved to ConMLDefaults package

"""
	_skipfeatureselectiondummy(...)

Allows to skip feature selection, does pretty much nothing.
Will inform about skipped feature selection if ´verbose == true´ in the parameters struct.
"""
function _skipfeatureselectiondummy(par::ParametersConML, tup)
	if par.verbose println("Feature selection was skipped.") end
	return tup
end

# makes copying properties from a ParametersConML to a FeatureSelector easier
macro push_to_kwargs(par, kwargs, property)
	esc(:( if haskey($kwargs, $property)
    	if $par.verbose && getproperty($par, $property) != $kwargs[$property] println("ConMLParameter `",$(property),"` overwritten by explicit kwarg in FeatureSelector.") end
   	else
		$kwargs[$property] = getproperty($par, $property)
	end))
end

"""
    (f::FeatureSelector)(KnowledgeBase, ParametersConML, tuple(LearnBlock, Vector(targets_vector), names) )
    (f::FeatureSelector)(KnowledgeBase, ParametersConML, LearnBlock, Vector(targets_vector), names)

Apply the feature selection methods stored in FeatureSelector to a data tuple:
Tuple(lb::VMS, candidates::AbstractVector{<:AbstractVector{<:Number}}, names::AbstractVector{<:AbstractString})
Each element of `candidates` should be a Vector of target values matching the length of `learnblock`
and is associated with the corresponding entry in `names`.
""" #TODO docstring revise
(f::FeatureSelector)(par::ParametersConML, tup) = f(par, tup...) # unpacking of tuple type if necessary
function (f::FeatureSelector)(par::ParametersConML, learnblock::VMS, targets, names)
    # push predictionErrorScore, MaxFeatures, testSetPercentage and the current learning domain into kwargs dict
    # just in case anyone overwrites these explicitly, issue a warning message if verbose
	#@push_to_kwargs par f.kwargs :predictionErrorScore
	@push_to_kwargs par f.kwargs :MaxFeatures
	@push_to_kwargs par f.kwargs :testSetPercentage
	@push_to_kwargs par f.kwargs :learningDomain

    modeldata = Vector{typeof(learnblock)}(undef, length(targets)) # will be filled with processed data soon
	_apply_fs!(f, modeldata, targets, names, par, learnblock)
    return (modeldata, targets, names)
end

function _apply_fs!(f::FeatureSelector, modeldata, targets, names, par::ParametersConML, learnblock::VMS)
	highSampleComplexity = sampleNumber(learnblock) > par.maxFilterSamples
	initialfeaturenames = learnblock.fields
	lbdata = learnblock.vals

	# allow deletion of blocks if feature selection fails
	deletemask = falses(length(targets))

	for (target, name, i) in zip(targets, names, 1:length(targets))
		if par.verbose println("Trying feature selection for candidate model: ", name) end

		selecteddata, selectednames = _apply_fs_loop(f, target, initialfeaturenames, lbdata, highSampleComplexity,
													par.verbose, par.MaxFeatures, par.maxFilterFeatures, par.MaxModelReduction)

		writelog(par.Logger, FeatureSelectionLog(), name, length(selectednames), sampleNumber(learnblock))
		if isempty(selecteddata) || isempty(selectednames)
			if par.verbose println("Feature selection failed for model: ", name) end
			deletemask[i] = true
		else
			modeldata[i] = VMS(learnblock.T, learnblock.Σ, learnblock.Z, selecteddata, selectednames)
        end
    end
	if any(deletemask)
		# delete failed blocks
		deleteat!(targets, deletemask)
		deleteat!(names, deletemask)
		deleteat!(modeldata, deletemask)
	end
end

function _apply_fs_loop(f, targetvalues, initialfeaturenames, lbdata::D, highSampleComplexity::Bool,
						verbose::Bool, MaxFeatures::Int, maxFilterFeatures::Int, MaxModelReduction::Bool) where {D}

	currentFeatureNumber::Int = length(initialfeaturenames)
    previousFeatureNumber::Int = currentFeatureNumber+1 # to trigger loop condition initially
    processedData::D = lbdata
    processedfeaturenames::Vector{String} = initialfeaturenames

	while (currentFeatureNumber > MaxFeatures || MaxModelReduction ) && (2 < currentFeatureNumber < previousFeatureNumber)
        # update previousFeatureNumber
        newfeatureindices::Vector{Int} = Int[]
        previousFeatureNumber = currentFeatureNumber
        # apply feature reduction
        if highSampleComplexity || currentFeatureNumber > maxFilterFeatures
            # use filter method to get indices
            if verbose println("\tapplying selected feature reduction filter method: ", f.filterMethodName) end
            newfeatureindices = f.filterMethod(processedData, targetvalues, f.kwargs)
        else
            # use embedded method
            if verbose println("\tapplying selected feature reduction embedded method: ", f.embeddedMethodName) end
            newfeatureindices = f.embeddedMethod(processedData, targetvalues, f.kwargs)
        end
        # subset data and fields
        processedData = processedData[:, newfeatureindices]
        processedfeaturenames = processedfeaturenames[newfeatureindices]
        # update currentFeatureNumber
        currentFeatureNumber = length(newfeatureindices)
        if verbose println("\treduced number of features from ", previousFeatureNumber, " to ", currentFeatureNumber) end
    end
    # if everything failed to reduce features below MaxFeatures, attempt a final pass
    if !isnothing(f.finalPass) && (currentFeatureNumber > MaxFeatures)
        if verbose println("\tapplying final pass method: ", f.finalPassName) end
        newfeatureindices = f.finalPass(processedData, targetvalues, f.kwargs)
        # subset data and fields
        processedData = processedData[:, newfeatureindices]
        processedfeaturenames = processedfeaturenames[newfeatureindices]
        # update currentFeatureNumber
        currentFeatureNumber = length(newfeatureindices)
        if verbose println("\treduced number of features from ", previousFeatureNumber, " to ", currentFeatureNumber) end
    end
	return (processedData, processedfeaturenames)
end

# shortcut if no classifier passed construction
function (f::FeatureSelector)(::ParametersConML, ::Nothing )
	# TODO is there anything loggable here?
	return nothing
end

# end of feature selection
