# Constructivist Machine Learning (conML)

This project is an implementation of the constructivist machine learning framework (conML) in Julia.

Maintainer: Florian Große

## Prerequesites
### ConML.jl
- Julia 1.5 or higher
- [Krippendorff.jl](https://github.com/FPGro/Krippendorff.jl/) (needs to be installed via the GitHub Link as long as it is not included in the general registry)

### ConMLDefaults.jl
- ConML.jl

## Installation from source

1. Get a working Julia installation of your choice. [I'll leave that to you.](https://julialang.org/downloads/)
2. Start Julia and enter package manager mode. (usually by pressing `]`)
3. (Set up a separate [environment](https://pkgdocs.julialang.org/v1/environments/) for conML if you want to.)
4. If not already done, install **Krippendorff.jl**
```
add https://github.com/FPGro/Krippendorff.jl
```
5. Install **ConML.jl**
```
add https://git.informatik.uni-leipzig.de/ml-group/conml/julia.git:ConML
```
6. (Install **ConMLDefaults.jl**)
```
add https://git.informatik.uni-leipzig.de/ml-group/conml/julia.git:ConMLDefaults
```
- If you are using a script to install these dependencies, note that you need to use `Pkg.add(PackageSpec(url = "https://git.informatik.uni-leipzig.de/ml-group/conml/julia.git", subdir = "ConML"))` (and `subdir = "ConMLDefaults"`), respectively.
- This should do the trick. If you intend to use ConMLDefaults for the first time, it will try to locate R and Python and set up the integration of the packages listed above (via [RCall.jl](https://github.com/JuliaInterop/RCall.jl) and [PyCall.jl](https://github.com/JuliaPy/PyCall.jl), respectively). If anything fails at this step, you can always try to install the required packages by hand or ignore the problems at the expense of missing out on some of the functions of ConMLDefaults.jl.
- ConML can be used without ConMLDefaults in case that you experience persistent installation issues (we are aware of troubles with PyCall and especially RCall for some users). Feel free to copy the bits that you need from the ConMLDefault source code in that case.
- Please make sure to report any problems that occur during installation including a description of your system and the installed versions of all related packages.

### Documentation
You'll find the documentation [here](https://git.informatik.uni-leipzig.de/ml-group/conml/julia/-/wikis/home).
