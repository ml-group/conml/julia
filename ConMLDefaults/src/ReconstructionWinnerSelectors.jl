#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConMLDefaults.jl and
# may not behave as expected when used on it's own

#API: winneridx = selector(; learningDomain, minReliability, reliabilities, featurenumbers, targets)

if SCIKIT_FEATURE_INSTALLED

    ## NDFS wrapper
    _ndfsdocstr = """
        Select_NDFS(n_clusters_strategy; kwargs...)

    Wraps the NDFS unsupervised feature selection scheme for use in ConML Reconstruction winner selection.

    See `?ConMLDefaults.skf_ndfs` for the original description:
    ~~~
        This function implement unsupervised feature selection using nonnegative spectral analysis, i.e.,
            min_{F,W} Tr(Fᵀ L F) + alpha*(||XW-F||_F² + beta*||W||_{2,1}) + gamma/2 * ||Fᵀ F - I||_F²
            s.t. F ≥ 0
    ~~~
    # Parameters

    `n_clusters_strategy` will determine how `n_clusters` for the NDFS algorithm is choosen.
    See [`dimensionality_options`](@ref) for possible values.

    For each cluster, the algorithm will yield the most predictive feature. After removing
    duplicates, `1 ≤ n_features_selected ≤ n_clusters` features will be selected. While setting
    `n_clusters` lower will result in fewer features being selected, setting it higher is
    generally less harmful but may introduce some noise (read: uninformative features) into the
    selection.

    The following `kwargs` will be passed to the NDFS implementation:

    - `alpha::Float64 = 1`: Parameter alpha in objective function
    - `beta::Float64 = 1`: Parameter beta in objective function
    - `gamma::Float64 = 10e8`: a very large number used to force Fᵀ F = I
    - `verbose::Bool = false`: True if user want to print out the objective function value in each iteration, false if not

    Note that parameters W, F0 and n_clusters can not be specified by the user, as they need to
    change between different calls in the ConML context. Use the parameters furter up to select
    the strategy for choosing the number of selected features.

    After n_clusters is determined, F0 will be constructed as following:

    Let Y be a one-hot encoded Kmeans label matrix of shape (n_samples, n_cluster) created with the settings:
    ~~~
    sklearn.cluster.KMeans(n_clusters=n_clusters, init='k-means++', n_init=10, max_iter=300,
                                        tol=0.0001, precompute_distances=True, verbose=0,
                                        random_state=None, copy_x=True, n_jobs=1)
    ~~~
    Then: F0 = (Y ⋅ √(A⁻¹)) .+ 0.02  with A = Yᵀ ⋅ Y

    The algorithm will then update F iteratively until some convergence criterion is satisfied.

    # References

    Li, Zechao, et al. "Unsupervised Feature Selection Using Nonnegative Spectral Analysis." AAAI. 2012.

    Li, Cheng, et al. "Feature selection: A data perspective" ACM Computing Surveys (CSUR) 50, 6, pp. 94. 2018.
    """
    struct Select_NDFS{N}
        find_n_clusters::N
        kwargs
        function Select_NDFS(find_n_clusters; W = nothing, F0 = nothing, n_clusters = nothing, kwargs...)
            # this catches overwriting of W, F0 and n_cluster
            new{typeof(find_n_clusters)}(find_n_clusters, kwargs)
        end
    end

    # application of Select_NDFS
    function (s::Select_NDFS)(; minReliability, reliabilities, targets, _...) # kwargs... gobbles all unused arguments
        _, originalindices, targetsmatrix = _filter_min_reliability(minReliability, reliabilities, targets)
        length(originalindices) ≥ 1 || return nothing # no feature passed min reliability
        length(originalindices) == 1 && return originalindices # return it if only 1 feature remained
        kwargs = Dict{Symbol, Any}(s.kwargs)
        kwargs[:n_clusters] = s.find_n_clusters(targetsmatrix)
        # BUG: skf_ndfs breaks down completely for input matrices larger than ~1200*features
        # due to fixed max_iter and tolerance, and ~O(n²) scaling (if not worse).
        # for this reason, the matrix is downsampled if it is bigger here
        # somebody should reimplement this properly in Julia
        inputmat = if size(targetsmatrix,1) > 1100
                randinds = shuffle(axes(targetsmatrix,1))[1:1000]
                targetsmatrix[randinds,:]
            else
                targetsmatrix
            end
        if kwargs[:n_clusters] == 1 # NDFS fails for 1 cluster, so do 2 and take the overal best
            kwargs[:n_clusters] = 2
            ndfs_out = skf_ndfs(inputmat; kwargs...)
            maxind = (argmax(abs.(ndfs_outndfs_out))).I
            selection = maxind[1] # features dim
            return originalindices[selection]
        else
            ndfs_out = skf_ndfs(inputmat; kwargs...)
            selection = sort(unique([argmax(abs.(col)) for col in eachcol(ndfs_out)]))
            return originalindices[selection]
        end
    end

    @doc _ndfsdocstr Select_NDFS

    ## UDFS wrapper
    _udfsdocstr = """
        Select_UDFS(n_clusters_strategy; kwargs...)

    Wraps the UDFS unsupervised feature selection scheme for use in ConML Reconstruction winner selection.

    See `?ConMLDefaults.skf_udfs` for the original description:
    ~~~
        This function implements l2,1-norm regularized discriminative feature selection for unsupervised learning, i.e.,
            min_W Tr(Wᵀ M W) + gamma * ||W||_{2,1},
            s.t. W^T W = I
    ~~~
    (note that the original docstring contains a mistake, saying that gamma has a default value of 1)
    # Parameters

    `n_clusters_strategy` will determine how `n_clusters` for the UDFS algorithm is choosen.
    See [`dimensionality_options`](@ref) for possible values.

    For each cluster, the algorithm will yield the most predictive feature. After removing
    duplicates, `1 ≤ n_features_selected ≤ n_clusters` features will be selected. While setting
    `n_clusters` lower will result in fewer features being selected, setting it higher is
    generally less harmful but may introduce some noise (read: uninformative features) into the
    selection.

    The following `kwargs` will be passed to the UDFS implementation:

    - `gamma::Float64 = 0.1`: Parameter gamma in the objective function
    - `verbose::Bool = false`: True if user want to print out the objective function value in each iteration, false if not
    - `k::Int = 5`: number of nearest neighbors to consider for the initialization

    Note that parameter `n_clusters` can not be specified by the user, as it needs to
    change between different calls in the ConML context. Use the parameters furter up to select
    the strategy for choosing the number of selected features.

    `k` will be used to construct a nearest neighbor matrix to capture the local structure of the data.

    # References

    Yang, Yi et al. "l2,1-Norm Regularized Discriminative Feature Selection for Unsupervised Learning." AAAI 2012.

    Li, Cheng, et al. "Feature selection: A data perspective" ACM Computing Surveys (CSUR) 50, 6, pp. 94. 2018.
    """
    struct Select_UDFS{N}
        find_n_clusters::N
        kwargs
        function Select_UDFS(find_n_clusters; n_clusters = nothing, kwargs...)
            # only catching n_clusters here
            new{typeof(find_n_clusters)}(find_n_clusters, kwargs)
        end
    end

    # application of Select_UDFS
    function (s::Select_UDFS)(; minReliability, reliabilities, targets, _...) # kwargs... gobbles all unused arguments
        _, originalindices, targetsmatrix = _filter_min_reliability(minReliability, reliabilities, targets)
        length(originalindices) ≥ 1 || return nothing # no feature passed min reliability
        length(originalindices) == 1 && return originalindices # return it if only 1 feature remained
        kwargs = Dict{Symbol, Any}(s.kwargs)
        kwargs[:n_clusters] = s.find_n_clusters(targetsmatrix)
        # BUG: see NDFS, the implementation scales awfully and breaks down for inputs
        # with more than ~1200 samples. So this is subsampled to a save size.
        # somebody should reimplement this properly in Julia
        inputmat = if size(targetsmatrix,1) > 1100
                randinds = shuffle(axes(targetsmatrix,1))[1:1000]
                targetsmatrix[randinds,:]
            else
                targetsmatrix
            end
        udfs_out = skf_udfs(inputmat; kwargs...)
        selection = sort(unique([argmax(abs.(col)) for col in eachcol(udfs_out)]))
        return originalindices[selection]
    end

    @doc _udfsdocstr Select_UDFS

    ## SPEC wrapper
    _specdocstr = """
        Select_SPEC(n_features_strategy; kwargs...)

    Wraps the UDFS unsupervised feature selection scheme for use in ConML Reconstruction winner selection.

    See `?ConMLDefaults.skf_spec` for the original description.

    # Parameters

    `n_features_strategy` will determine how many features will be kept after creating a feature
    ranking via SPEC. See [`dimensionality_options`](@ref) for possible values.

    The following keyword argument can be passed to the SPEC implementation directly:

    - `style::Int = 0`: Changes the behaviour of the feature ranking function:

        `style == -1`: use all eigenvalues

        `style == 0`:  use all except the first eigenvalue

        `style ≥ 2`:   use the first k except the first eigenvalue [sic]

    Note that parameter `W` can not be specified by the user, as it needs to change between
    different calls in the ConML context. W defaults to `rbf_kernel(input, gamma = 1)`.

    # References

    Zhao, Zheng and Liu, Huan. "Spectral Feature Selection for Supervised and Unsupervised Learning." ICML 2007.

    Li, Cheng, et al. "Feature selection: A data perspective" ACM Computing Surveys (CSUR) 50, 6, pp. 94. 2018.
    """
    struct Select_SPEC{N}
        find_n_features::N
        style::Int
        function Select_SPEC(find_n_features; style::Int = 0, W = nothing, kwargs...)
            # only catching W here, discard kwargs since the algorithm takes no other
            new{typeof(find_n_features)}(find_n_features, style)
        end
    end

    # application of Select_SPEC
    function (s::Select_SPEC)(; minReliability, reliabilities, targets, kwargs...) # kwargs... gobbles all unused arguments
        _, originalindices, targetsmatrix = _filter_min_reliability(minReliability, reliabilities, targets)
        length(originalindices) ≥ 1 || return nothing # no feature passed min reliability
        length(originalindices) == 1 && return originalindices # return it if only 1 feature remained
        n_features = s.find_n_features(targetsmatrix)
        spec_out = skf_spec(targetsmatrix; s.style)
        # according to the scikit-feature implementation:
        # # if style = -1 or 0, ranking features in descending order, the higher the score, the more important the feature is
        # # if style != -1 and 0, ranking features in ascending order, the lower the score, the more important the feature is
        ranking = (s.style == -1 || s.style == 0) ? sortperm(spec_out, rev=true) : sortperm(spec_out)
        return originalindices[ ranking[1:n_features] ] # first n features with best score
    end

    @doc _specdocstr Select_SPEC

    ## Laplacian Score wrapper
    #TODO write strings for dimension estimation
    _lsdocstr = """
        Select_LS(n_features_strategy; kwargs...)

    Wraps the Laplacian-Score (LS) unsupervised feature selection scheme for use in ConML Reconstruction winner selection.

    See `?ConMLDefaults.skf_ls` for the original description:
    ~~~
        This function implements the laplacian score feature selection, steps are as follows:
        1. Construct the affinity matrix W if it is not specified
        2. For the r-th feature, we define fᵣ = X(:,r), D = diag(W * ones), ones = [1,...,1]ᵀ, L = D - W
        3. Let f̂ᵣ = fᵣ - (fᵣᵀ D ones) * ones / (onesᵀ D ones)
        4. Laplacian score for the r-th feature is score = (f̂ᵣᵀ L f̂ᵣ) / (f̂ᵣᵀ D f̂ᵣ)
    ~~~
    # Parameters

    `n_features_strategy` will determine how many features will be kept after creating a feature
    ranking via LS. See [`dimensionality_options`](@ref) for possible values.

    The following kwargs will be passed to the LS implementation and controll the construction
    of the initial affinity matrix:

    - `metric::String = "cosine"`: choices for different distance measures, "cosine" or "euclidean"
    - `weight_mode::String = "binary"`: indicates how to assign weights for each edge in the graph, possible values:
        - `"binary"`: 0-1 weighting, every edge receives weight of 1
        - `"heat_kernel"`: if nodes i and j are connected, put weight W_ij = exp(-norm(x_i - x_j)/2t^2)
                            this weight mode can only be used under 'euclidean' metric and you are required
                            to provide the parameter t
        - `"cosine"`: if nodes i and j are connected, put weight cosine(x_i,x_j).
                        this weight mode can only be used under 'cosine' metric
    - `k::Int = 5`: number of nearest neighbors per node that will receive an edge
    - `t::Float = 1.`: parameter for the 'heat_kernel' weight_mode, ignored in other modes

    Compare `?ConMLDefaults.skf_construct_W` if necessary. Parameters not mentioned above are ignored
    or don't apply under unsupervised FS mode.

    Note that the affinity matrix `W` can not be directly specified by the user, as it needs to change between
    different calls in the ConML context and will be constructed using the parameters above.

    # References

    He, Xiaofei et al. "Laplacian Score for Feature Selection." NIPS 2005.

    Li, Cheng, et al. "Feature selection: A data perspective" ACM Computing Surveys (CSUR) 50, 6, pp. 94. 2018.
    """
    struct Select_LS{N}
        find_n_features::N
        kwargs
        function Select_LS(find_n_features; neighbor_mode = nothing, kwargs...)
            # catching the things that may not be changed for construct_W here (neighbor_mode)
            new{typeof(find_n_features)}(find_n_features, kwargs)
        end
    end

    # application of Select_LS
    function (s::Select_LS)(; minReliability, reliabilities, targets, kwargs...) # kwargs... gobbles all unused arguments
        _, originalindices, targetsmatrix = _filter_min_reliability(minReliability, reliabilities, targets)
        length(originalindices) ≥ 1 || return nothing # no feature passed min reliability
        length(originalindices) == 1 && return originalindices # return it if only 1 feature remained
        n_features = s.find_n_features(targetsmatrix)
        construct_W_args = Dict(s.kwargs)
        W = pycall(skf_construct_W, PyObject, targetsmatrix; construct_W_args...)
        ls_out = skf_ls(targetsmatrix; W)
        ranking = sortperm(ls_out)
        return originalindices[ ranking[1:n_features] ] # first n features with best score
    end

    @doc _lsdocstr Select_LS
end # end of scikit-feature specific code

## reliability mRMR
# TODO proper documentation
"""
    Select_reliability_mRMR(n_features_strategy)

TODO
Apply reliability-mRMR FS

property: n_features_strategy
copy!:
`n_features_strategy` will determine how many features will be kept after creating a feature
ranking
"""
struct Select_reliability_mRMR{N}
    find_n_features::N
end

# application of Select_reliability_mRMR
function (s::Select_reliability_mRMR)(; learningDomain, minReliability, reliabilities, targets, kwargs...) # kwargs... gobbles all unused arguments
    mask, originalindices, targetsmatrix = _filter_min_reliability(minReliability, reliabilities, targets)
    filtered_reliabilities = reliabilities[mask]
    l = length(originalindices)
    l ≥ 1 || return nothing # no feature passed min reliability
    n_select = s.find_n_features(targetsmatrix)
    n_select ≥ l && return originalindices # return all if more features have to be choosen than are available
    redundancy_matrix = zeros(n_select-1, l)
    free_features = trues(l) # the negation of this will be the selection mask later
    # select the first feature based on reliability alone
    previously_selected_feature = argmax(filtered_reliabilities)
    free_features[previously_selected_feature] = false
    for i_last_selected in 1:n_select-1 # because the first was decided on reliability alone
        # calculate new redundancy terms
        redundancies = map(originalindices[free_features]) do orig_ind
            Xi = reshape(targets[i_last_selected], :, 1)
            Xj = targets[orig_ind]
            return only(appropriate_mutual_information(learningDomain, Xi, Xj))
        end
        # fills the redundancy matrix "top-to-bottom" with the redundancy measure of each
        # free feature with the last selected feature.
        # What that essentially means is that after each step, for every feature not
        # already selected, the sum of the whole column corresponding to that feature
        # is the sum of the redundancies with all already selected features.
        redundancy_matrix[i_last_selected, free_features] = redundancies
        # calculate overall redundancies
        redundancy_sums = [sum(col) for (i, col) in enumerate(eachcol(redundancy_matrix)) if free_features[i]]
        next_selected = argmax(filtered_reliabilities[free_features] .- (redundancy_sums ./ i_last_selected)) # i or i-1?? TODO - or /
        # update free features and selected
        previously_selected_feature = (1:l)[free_features][next_selected]
        free_features[previously_selected_feature] = false
    end
    selection = originalindices[.!free_features]
    return selection
end

## filter min reliability helper
function _filter_min_reliability(minReliability, reliabilities, targets)
    # return targetsmatrix, originalindices for all the variants working with targets
    mask = reliabilities .>= minReliability
    return (mask, collect(eachindex(reliabilities))[mask], hcat(targets[mask]...))
end

function _filter_min_reliability(minReliability, reliabilities)
    # return mask, originalindices if no targets are used
    mask = reliabilities .>= minReliability
    return (mask, collect(eachindex(reliabilities))[mask])
end

## find_n_clusters/find_n_features helper

"""
    dimensionality_options

A list of the currently implemented options to generate estimates of intrinsic dimensionality
or an initial number of clusters for certain winner selection methods. See each methods docs
for detailed documentation. Available:

- [`fixed_n`](@ref)
- [`proportional_n`](@ref)
- [`function_of_n`](@ref)
- [`skdim_n`](@ref)
"""
const dimensionality_options = @doc dimensionality_options

"""
    fixed_n(n::Int)

Always select a fixed number of dimensions, independent of the actual data.
"""
function fixed_n(n::Int)
    return (matrix) -> n
end

"""
    proportional_n(factor)

Determine `n` as `p * factor` where p is the number of features in the input dataset.
(round to the nearest integer)
"""
function proportional_n(fac)
    return (matrix) -> round(Int, featureNumber(matrix)*fac)
end

"""
    function_of_n(f)

Determine `n` as an arbitrary function of the number of features in the input dataset: `f(n)`
(round to the nearest integer)

Example: `function_of_n(sqrt)` will always select the square root of the input dimension round
to the nearest integer as dimension estimate.
"""
function function_of_n(fun)
    return (matrix) -> round(Int, fun(featureNumber(matrix)))
end

"""
    skdim_n(instance; factor = 1.)

Use any of the intrinsic dimensionality estimators from the `Scikit-dimension` Python package:

[https://scikit-dimension.readthedocs.io/en/latest/api.html#id-estimators](https://scikit-dimension.readthedocs.io/en/latest/api.html#id-estimators)

The `skdim.id` submodule is loaded with `PyCall` and reexported as ``ConMLDefaults.ID`.
Just instantiate any of the id estimators mentioned there with whatever settings you need
and pass it to `skdim_n` and it will be fit and evaluated on the candidates matrix automatically.

`factor` can be used to artificially over- or underestimate the dimensionality by a constatnt factor.
"""
function skdim_n(instance; factor = 1.)
    return (matrix) ->  round(Int, instance.fit_predict(matrix) * factor)
end
