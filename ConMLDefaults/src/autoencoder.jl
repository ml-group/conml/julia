#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConMLDefaults.jl and
# may not behave as expected when used on it's own

# imports in ConMLDefaults.jl
#using Flux
#using Statistics: mean
#using ScikitLearnBase
#import ScikitLearnBase: predict
#import ScikitLearnBase: clone, fit!, transform

# Sparse Autoencoder with ScikitLearnBase interface
struct SparseAutoencoder{P} # P is the precision here, i.e. Float32/Float64
    # original parameters
    n_hidden_nodes::Int
    activation
    optimiser
    lambda::P
    beta::P
    rho::P
    max_iter::Int
    scale::Bool
    desired_range::Tuple{P,P}
    # additional parameters
    activation_memory_decay::P
    convergence_tolerance::P
    verbose::Bool
    # learned parameters
    minmax::Ref{Tuple}
    model::Ref{Any}
    SparseAutoencoder{P}(;  n_hidden_nodes = 5,
                            activation = sigmoid,
                            optimiser = ADAM(),
                            lambda = 0.0002,
                            beta = 6,
                            rho = 0.1,
                            max_iter = 100,
                            scale = true,
                            desired_range = (0.001,0.999),
                            activation_memory_decay = 0.999,
                            convergence_tolerance = sqrt(eps()),
                            verbose = false
                        ) where {P} =
        new{P}(n_hidden_nodes, activation, optimiser, lambda,
                beta, rho, max_iter, scale, desired_range,
                activation_memory_decay, convergence_tolerance,
                verbose, (), nothing)
end
# default for precision
SparseAutoencoder(kwargs...) = SparseAutoencoder{Float32}(kwargs...)

ScikitLearnBase.@declare_hyperparameters(SparseAutoencoder, [:n_hidden_nodes, :activation,
            :optimiser, :lambda, :beta, :rho, :max_iter, :scale, :desired_range, :activation_memory_decay, :convergence_tolerance, :verbose])

function fit!(auto::SparseAutoencoder, X, y = nothing)
    # if not already initialised, initialise model accoring to
    # the number of input features and n_hidden_nodes
    # Layout: (inputsize -> n_hidden_nodes -> inputsize)
    if isnothing(auto.model[])
        inputsize = size(X,2)
        auto.model[] = Chain(
            Dense(inputsize, auto.n_hidden_nodes, auto.activation),
            Dense(auto.n_hidden_nodes, inputsize, auto.activation)
        )
    end
    input = auto.scale ? scale(auto, X) : X
    return train!(auto, input)
end

function transform(auto::SparseAutoencoder, X)
    input = auto.scale ? scale(auto, X') : X'
    return Flux.activations(auto.model[], input)[1]
end

function predict(auto::SparseAutoencoder, X, y = nothing)
    if auto.scale
        return undo_scaling(auto, auto.model[](scale(auto, X')))
    else
        return auto.model[](X')
    end
end

function compute_mean_error(auto, X)
    Xscaled = scale(auto, X')
    scaledreconstruction = auto.model[](scale(auto, X'))
    diff = scaledreconstruction .- Xscaled
    return mean(sum(abs2, diff, dims = 1))
end

function scale(auto::SparseAutoencoder, X)
    # simple min-max scaling
    lower, upper = extrema(X)
    previously_fitted_minmax = auto.minmax[]
    if isempty(previously_fitted_minmax)
        auto.minmax[] = (lower, upper)
    else
        if (lower < first(previously_fitted_minmax) ||
            upper > last(previously_fitted_minmax) )
            @warn """
                Previously fitted scaling had a smaller range than the current data.
                This may lead to suboptimal convergence and learning results.
            """
        end
        # use original scaling if possible for consistency
        lower, upper = auto.minmax[]
    end
    tomin, tomax = auto.desired_range
    factor = (tomax - tomin) / (upper - lower)
    # subtract global min, reduce span to desired span, add desired min
    return ((X .- lower) .* factor ) .+ tomin
end

function undo_scaling(auto::SparseAutoencoder, out)
    # reverse scaling of out
    # assumes that fit! was called before
    lower, upper = auto.desired_range
    tomin, tomax = auto.minmax[]
    factor = (tomax - tomin) / (upper - lower)
    # subtract desired min, lift span to original span, add original min
    return ((out .- lower) .* factor ) .+ tomin
end

# internal autoencoder workings

function train!(auto::SparseAutoencoder, input)
    # assumes that auto.model[] exists
    # ScikitLearn API specifies columns as features and rows as samples
    dataset_iterator = eachrow(input)
    n_samples = length(dataset_iterator)
    m = auto.model[]

    cost_function = let lambda = auto.lambda
        function cost_function(input, model, weights)
            return  0.5 *           Flux.mse(model(input), input) +
                    0.5 * lambda *  sum(ws->sum(abs2,ws), weights)
        end
    end

    _train_loop!(m, dataset_iterator, cost_function, auto.optimiser, n_samples, auto.activation_memory_decay,
                auto.beta, auto.rho, auto.max_iter, auto.convergence_tolerance, auto.verbose)
    return auto
end

function _train_loop!(m, datapoints, cost_function, opt, n_samples, r, beta, rho, max_iter, ctol, verbose)
    r̄ = oftype(r, 1) - r
    parameters = params(m)
    weights = (m[1].weight, m[2].weight)
    hidden_biases = m[1].bias
    activation_tracker = fill(rho, length(m[1].bias))

    prev_loss = Inf
    for i in 1:max_iter
        if verbose println("Training...Iteration $(i) of $(max_iter)") end
        loss = 0.
        for d in datapoints
            gradients = gradient(parameters) do
                cost_function(d, m, weights)
            end
            Flux.update!(opt, parameters, gradients)
            hidden_activations = Flux.activations(m, d)[1]
            activation_tracker .=   r .* activation_tracker .+
                                    r̄ .* hidden_activations
            Flux.update!(opt, hidden_biases, beta .* (activation_tracker .- rho))
            loss = loss + cost_function(d, m, weights)
        end
        loss = loss / n_samples # mean over a whole dataset iteration
        if prev_loss-loss < ctol*(abs(loss)+ctol)
            if verbose @info "Converged with loss: $(loss)" end
            break
        end
        prev_loss = loss
        if verbose println("Loss reached: $(loss)") end
    end
    nothing
end

function hidden_activations(model::SparseAutoencoder, input; apply_undo_scaling = false)
    # ScikitLearn API specifies columns as features and rows as samples
    if apply_undo_scaling && model.scale
        return undo_scaling(model, transform(model, input)')
    else
        return transform(model, input)'
    end
end

function predict_reconstruction(model::SparseAutoencoder, input)
    # ScikitLearn API specifies columns as features and rows as samples
    return predict(model, input)'
end
