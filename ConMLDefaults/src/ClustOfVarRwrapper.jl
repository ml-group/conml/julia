## wrap this into some struct for usage

# assume package ClustOfVar has already been loaded
#kmv = ClustOfVar.kmeansvar(input, init = n)
#output = convert(Matrix, kmv[:scores])

# ClustOfVar Wrapper
#TODO docstring/explain
"""
    ClustOfVarKMeans(; init, nstart = 1, iter_max = 150)

Wraps the `kmeansvar` algorithm from the CLustOfVar R package (via RCall) with a ScikitLearnBase API.
The subset of allowed arguments is directly passed through.

# Arguments
- `init::Int`: Mandatory argument. The desired number of clusters.
- `nstart::Int = 1`: A random set of `nstart` (distinct) columns is chosen as the initial clustercenters.
- `iter_max::Int = 150`: Maximum number of iterations.
"""
struct ClustOfVarKMeans<:BaseRegressor
    init::Int
    nstart::Int
    iter_max::Int
    # model starts uninitialized
    model::Ref{Any}
    featureprefilter::Ref{Any} # remember which features had no differing values during training and remove them
    function ClustOfVarKMeans(init, nstart, iter_max)
        return new(init, nstart, iter_max, nothing, nothing)
    end
end
ClustOfVarKMeans(;init, nstart = 1, iter_max = 150) = ClustOfVarKMeans(init, nstart, iter_max)

ScikitLearnBase.@declare_hyperparameters(ClustOfVarKMeans, [:init, :nstart, :iter_max])

# this is needed because kmeansvar just throws around errors when it encounters dead featues with just one value (0) repeated
function _drop_dead_features(cov::ClustOfVarKMeans, X)
    # if not learned then do so
    if isnothing(cov.featureprefilter[])
        ranges = extrema(X, dims = 1)
        cov.featureprefilter[] = vec(map(x -> ≠(x...), ranges))
    end
    # apply
    if length(cov.featureprefilter[]) ≠ size(X,2)
        throw(DimensionMismatch("Number of input features to ClustOfVar $(size(X,2)) does not match the fitted size $(length(cov.featureprefilter[]))!"))
    end
    return X[:, cov.featureprefilter[]]
end

function ScikitLearnBase.fit!(cov::ClustOfVarKMeans, X, Y=nothing)
    input = _drop_dead_features(cov, X)
    try
        cov.model[] = ClustOfVar.kmeansvar(input, init = cov.init, nstart = cov.nstart, var"iter.max" = cov.iter_max)
    catch e
        cov.model[] = nothing # fitting failed
    end
    return cov
end

function ScikitLearnBase.transform(cov::ClustOfVarKMeans, X = nothing)
    if isnothing(cov.model[])
        return nothing # fitting failed
    else
        return convert(Matrix, cov.model[][:scores])
    end
end
# end of ClustOfVar Wrapper
