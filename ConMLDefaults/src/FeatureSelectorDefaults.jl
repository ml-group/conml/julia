#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConMLDefaults.jl and
# may not behave as expected when used on it's own

# FeatureSelector defaults
"""
    DefaultFeatureSelector(; groupingThreshold::Float64 = 0.95, absoluteCorrelation::Bool = false)

Default feature selection scheme replacing the original procedure in (Schmid, 2018). Return a `ConML.FeatureSelector` object.

This is a replacement for the original procedure because of the lack of a julia implementation
of CFS. It aims to reproduce the commonly applied feature selection scheme of identifying
highly correlated features and somehow selecting the most meaningful representative of
each correlated group of features. The embedded method (Random Forest Feature Importance)
is unaltered.

Filter method: /"Conservative Correlation Grouping/" (or however you want to call it)
This method uses the `groupingThreshold` and `absoluteCorrelation` parameters.
Compute the (Pearson) correlation between all pairs of features and group highly correlated
features by applying a kind of hierarchical clustering with complete linkage on the features.
In more detail: 2 features (or feature groups) will be grouped whenever they have the highest
of all current correlation values and their correlation is higher than `groupingThreshold`.
If `absoluteCorrelation` is true, the absolute correlation is used, so highly anti-correlated
features can be grouped, too. After a group was formed, the correlation values with features
outside that group are determined by taking the lower value of the group members for each
feature outside (similar to how complete linkage works). This guarantees that if two groups
are merged, the lowest correlation among all features in both groups is still ≥ `groupingThreshold`.
When no further grouping is possible, a representative of each group is choosen by estimating
the Mutual Information (via `ScikitLearn: mutual_info_classif`) of each feature with the target
classification and selecting the highest-scoring feature of each group.
Seletion of more than ``κ`` features is ~~possible~~ likely.

Embedded method: The Gini importance derived from a random forest is used to rank all features
for their importance in decresing order. Subsequently, for ``i ∈ [1, κ]`` (see ConML: [`ParametersConML`](@ref))
all subsets containing the ``i`` most important features are evaluated by training another
random forest and computing the accuracy for a test set. (depending on `ParametersConML.testSetPercentage`)
This is guaranteed to select at most ``κ`` features.

If the number of important features is bigger than ``κ`` and it does not change for 2
subsequent iterations, then a final pass of the embedded method is enforced to guarantee
the output of at most ``κ`` features.

# Parameters

- `groupingThreshold::Float64 = 0.95`: Used by the filter method (correlation grouping).
                                        Pearson correlation threshold above which two features may be grouped together.

- `absoluteCorrelation::Bool = false`: Used by the filter method (correlation grouping).
                                        If `true` uses the absolute value of the correlation to determine if features may be grouped
                                        together. This allows grouping of highly anti-correlated features.

- `errorscore=nothing`: Callback that is used by the default embedded method (random forests) to compare
                        targets vector with the generated prediction in order to select the best-performing feature subset.
                        Must take two (Abstract)Vectors and return a number where lower indicates better. Defaults to `nothing`
                        which will internally be translated to `1-accuracy` for conceptual and `MAPE` for procedural domain runs.
"""
function DefaultFeatureSelector(; groupingThreshold::Float64 = 0.95, absoluteCorrelation::Bool = false,
                                errorscore = nothing, n_trees_ranking = 500, n_trees_eval = 100)
    # create default methods
    defEmbeddedName = "RandomForest - Mean Decrease in Purity Importance"
    defEmbeddedMethod = _embeddedDefault_RFGini
    defFilterName = "Conservative Correlation Grouping"
    defFilterMethod = _filterCCG
    # pass to normal constructor
    FeatureSelector(defEmbeddedMethod, defEmbeddedName, defFilterMethod, defFilterName,
                    defEmbeddedMethod, defEmbeddedName; groupingThreshold, absoluteCorrelation,
                    errorscore, n_trees_ranking, n_trees_eval)
end

function _filterCCG(data, targets, kwargs)
    # data is (samples x features)
    # extract required parameters
    MaxFeatures::Int = kwargs[:MaxFeatures]
    testSetPercentage::Float64 = kwargs[:testSetPercentage]
    groupingThreshold::Float64 = get(kwargs, :groupingThreshold, 0.95) # get with default just in case
    absoluteCorrelation::Bool = get(kwargs, :absoluteCorrelation, false)

    # calculate correlation matrix and mutual information vs targets
    nfeatures = ConML.featureNumber(data)
    correlations = _uppertriagmatvector( [cor(view(data, :, i),view(data, :, j)) for i in 1:nfeatures for j in i+1:nfeatures] )
    mutinfs = appropriate_mutual_information(kwargs[:learningDomain], data, targets)

    #XXX Got struck by insight, it would be almost the same (and likely faster) to always
    # find the highest pair and immediately immediately merge the 2 entries in a complete
    # linkage fashion. I think this would get rid of recomputing correlations repeatedly
    # after all merges >groupingThreshold have been applied. ==> TBD

    # group correlated values as long as maxcor > threshold
    colgroups = [[i,] for i in 1:correlations.n]
    while true
        maxcorind::CartesianIndex = argmax(correlations)
        maxcor = correlations[maxcorind]
        if maxcor < groupingThreshold
            break
        end
        # col a and col b are the columns that have the highest correlation value
        cola, colb = maxcorind.I
        # calculate min for conservative grouping / complete linkage
        completelinks = [min(correlations[i,cola], correlations[i,colb]) for i in 1:correlations.n]
        # modify a column
        append!(colgroups[cola], colgroups[colb])
        for (i, cl) in enumerate(completelinks)
            if i != cola
                correlations[i,cola] = cl
            end
        end
        # drop b column
        deleteat!(colgroups, colb)
        dropcol!(correlations, colb)
    end
    # colgroups is now a list of grouped column indices
    chooseninds = Vector{Int}()
    # for each group, choose that column which has the highest mutual information vs the targets
    for group in colgroups
        maxind = argmax( map(x->mutinfs[x], group) )
        push!(chooseninds, group[maxind])
    end
    # return all choosen indices
    return sort(chooseninds)
end

"""
    PrototypeFeatureSelector(; errorscore = nothing)

Feature selection scheme reproducing the original process described in (Schmid, 2018) as close
as possible. Return a `ConML.FeatureSelector` object.

Embedded method: The Gini importance derived from a random forest is used to rank all features
for their importance in decresing order. Subsequently, for ``i ∈ [1, κ]`` (see ConML: [`ParametersConML`](@ref))
all subsets containing the ``i`` most important features are evaluated by training another
random forest and computing the accuracy for a test set. (depending on `ParametersConML.testSetPercentage`)
This is guaranteed to select at most ``κ`` features.

Filter method: CFS (correlation-based feature selection, see R package)
NOTE: CURRENTLY NOT IMPLEMENTED! (mocked to always return all features as important)
Seletion of more than ``κ`` features is possible.

A small change was introduced in comparison to the described procedure: if the number of important
features is bigger than ``κ`` and it does not change for 2 subsequent iterations, then a
final run of the embedded method is enforced, guaranteeing the output of at most ``κ`` features
at the expense of a potentially higher runtime.

# Parameters

- `errorscore=nothing`: Callback that is used by the default embedded method (random forests) to compare
                        targets vector with the generated prediction in order to select the best-performing feature subset.
                        Must take two (Abstract)Vectors and return a number where lower indicates better. Defaults to `nothing`
                        which will internally be translated to `1-accuracy` for conceptual and `MAPE` for procedural domain runs.
"""
function PrototypeFeatureSelector(; errorscore = nothing, n_trees_ranking = 500, n_trees_eval = 100)
    # create default methods
    defEmbeddedName = "RandomForest - Mean Decrease in Purity Importance"
    defEmbeddedMethod = _embeddedDefault_RFGini
    defFilterName = "Correlation-based Feature Selection"
    defFilterMethod = _filterDefault_CFS
    # pass to normal constructor, embedded appears twice to enable it as a final pass
    FeatureSelector(defEmbeddedMethod, defEmbeddedName, defFilterMethod, defFilterName, defEmbeddedMethod,
                    defEmbeddedName; errorscore, n_trees_ranking, n_trees_eval)
end

# TODO checktypes
function _embeddedDefault_RFGini(data, targets, kwargs)
    errorscoresetting = kwargs[:errorscore]
    domain = kwargs[:learningDomain]
    errorfun = isnothing(errorscoresetting) ? (domain == ConML.Conceptual() ? ConML.accuracyError : ConML.MAPE) : errorscoresetting
    MaxFeatures::Int = min(kwargs[:MaxFeatures],featureNumber(data))
    testSetPercentage::Float64 = kwargs[:testSetPercentage]
    n_trees_ranking::Int = get(kwargs, :n_trees_ranking, 500)
    n_trees_eval::Int = get(kwargs, :n_trees_eval, 100)
    # TODO apply cross-validation mit ~5*100 trees
    rankingrf = domain == ConML.Conceptual() ? RandomForestClassifier(n_estimators = n_trees_ranking, n_jobs = -1) :
                                                RandomForestRegressor(n_estimators = n_trees_ranking, n_jobs = -1)
    fit!(rankingrf, data, targets)
    importanceRanking = sortperm(rankingrf.feature_importances_ ; rev=true)
    minError = Inf
    minErrorI = MaxFeatures

    # apply train/test split #TODO once or once per subset
    x_train, x_test, y_train, y_test = ScikitLearn.CrossValidation.train_test_split(data, targets, test_size = testSetPercentage)
    for i in 2:MaxFeatures
        train_subset = x_train[:,importanceRanking[1:i]]
        test_subset = x_test[:,importanceRanking[1:i]]
        # TODO check if a native julia RFC is faster here
        rf = domain == ConML.Conceptual() ? RandomForestClassifier(n_estimators = n_trees_eval, n_jobs = -1) :
                                            RandomForestRegressor(n_estimators = n_trees_eval, n_jobs = -1)
        fit!(rf, train_subset, y_train)
        err = errorfun(y_test, predict(rf, test_subset))
        if err<minError
            minError = err
            minErrorI = i
        end
    end
    return sort(importanceRanking[1:minErrorI])
end

# TODO docstring
@static if SCIKIT_FEATURE_INSTALLED
    function _filterDefault_CFS(data, targets, kwargs)
        # CFS uses 0-based indices so it's off by one
        ret = collect(skf_cfs(data, targets)) .+ 1
        return ret
    end
else
    function _filterDefault_CFS(data, targets, kwargs)
        error("""
                Scikit Feature not properly installed. You can try rerunning the build script
                or install it by hand and then rerun the build script to înclude the library here.
            """)
    end
end

const featureselection_CCG = _filterCCG
const featureselection_RF = _embeddedDefault_RFGini
const featureselection_CFS = _filterDefault_CFS

# end of FeatureSelector defaults
