#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConMLDefaults.jl and
# may not behave as expected when used on it's own

# imports in ConMLDefaults.jl
# using Statistics: mean
# the following indicates that we extend the function definitions from ScikitLearnBase
# import ScikitLearnBase: clone, fit!, transform

struct PrototypeAutoencoderQC
    maximum_reconstruction_error::Float64
    autoencoder
end
# convenience constructor for a nice user interface
PrototypeAutoencoderQC(; maximum_reconstruction_error = 0.25,
                         precision = Float32,
                         kwargs...) =
    PrototypeAutoencoderQC( maximum_reconstruction_error,
                            SparseAutoencoder{precision}(;kwargs...))

# simply forward clone to the wrapped autoencoder
clone(pa::PrototypeAutoencoderQC) = PrototypeAutoencoderQC( pa.maximum_reconstruction_error,
                                                            clone(pa.autoencoder))

# fit! is also simply forwarded, since no special behaviour is needed
function fit!(pa::PrototypeAutoencoderQC, X)
    fit!(pa.autoencoder, X)
    return pa
end

function transform(pa::PrototypeAutoencoderQC, X)
    # get a reconstruction for X
    reconstruction = predict_reconstruction(pa.autoencoder, X)
    # error measure is defined as the mean (over all input samples)
    # of the sum of squared errors (sum over all features: (output-input)²)
    # this is essentially the squared euclidean distance between input and output
    # shape is samples x features here
    # Although it is dumb, I looked into the R source code and they reverse the scaling
    # for the test but not for the train set before computing the mean error, so the
    # values you get from `ae$mean.error.training.set` are mse in the scaled space!
    # Because Thomas relied on this and set the threshold top 0.25, I reproduced this
    # but it is a terrible way to ensure quality. Just try to plot the average "error"
    # of a bunch of random variables according to this formula!
    error = compute_mean_error(pa.autoencoder, X)
    if error <= pa.maximum_reconstruction_error
        # error sufficiently small
        ret = hidden_activations(pa.autoencoder, X, apply_undo_scaling = true)
        return ret
    else
        # error too large
        return nothing
    end
end

function fit_transform!(pa::PrototypeAutoencoderQC, X)
    fit!(pa.autoencoder, X)
    return transform(pa, X)
end
