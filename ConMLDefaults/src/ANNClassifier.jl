#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConMLDefaults.jl and
# may not behave as expected when used on it's own

# KNN-wrapper
# note that I don't think the big number of models + high number of epochs is a good idea for a default
"""
    PrototypeClassifierANN(rescale = true, desiredrange = (-0.8, 0.8), hiddenLayerSizeFactor = 1.5, cv = 5, noImprovementThreshold = 0., verbose = false, activation = "tanh", max_iter = 2500 [, kwargs...])

Approximately reproduces the ConML prototypes ANN classifier while respecting the ScikitLearn API.

This object will hold parameters and a single trained (ScikitLearn) `MLPClassifier` after
training, together with rescaling information.
Fitting this will do the following: let `n` be the number of input features.
For each ``k ∈ [1, ⌊n*hiddenLayerSizeFactor⌋]`` a `MLPClassifier` will be trained on the
input and the accuracy (on the training data) will be evaluated via `cv`-fold cross validation.
`k` represents the size of the single hidden layer. Only the best-performing model will be
saved (`storedmodel`) and used for prediction.
Furthermore, if `rescale == true`, all input data will be rescaled (approximately) into
the interval given by `desiredrange`. To avoid bias, the scaling transformation will be learned
on the input data and faithfully applied to prediction data, which means that if the data
used in prediction span a larger range than the trainig data, then the transformed prediction
data may lie outside of the specified range.

# Arguments
- `rescale::Bool`: Whether the input should be rescaled at all.
- `desiredrange::NTuple{2,Float64}`: Desired range of the input after rescaling.
- `hiddenLayerSizeFactor::Float64`: Controlls the maximal hidden layer size depending on the number of input dimensions.
- `cv::Int`: Number of cross validations to estimate `MLPClassifier` accuracy during training.
- `noImprovementThreshold::Union{Nothing, Float64}`: If not `nothing`, hidden layers will not be expanded while fitting if no
                                                        significant improvement in average accuracy was observed for 2 subsequent runs.
                                                        The value controlls the minimum improvement that is taken as significant.
                                                        Negative values will allow for slight drops in performance.
- `verbose::Bool`:  Whether to display verbose progress information during training.
                    Note: this shadows the `MLPClassifier` verbose keyword and is not equivalent to it.
                    You can supply `MLPClassifier` verbose explicitly via `MLPkwargs`.

- `MLPkwargs`
and
- `kwargs...`:  Every other argument will be stored in a dictionary (`MLPkwargs`) and
                supplied to each `MLPClassifier`. For convenience, these can either be
                supplied as a dictionary directly, or as normal keyword arguments. Thus,
                `MLPkwargs = Dict(:something => "setting")` is equal to `something = "setting"`
                for all `MLPClassifier` parameters except `verbose`, which needs to be
                supplied as `MLPkwargs = Dict(:verbose => true)` to distinguish it from
                this objects verbose parameter. In case of conflicting values, a keyword
                argument will take precedence over an entry in `MLPkwargs`.
                See the ScikitLearn documentation for available arguments!
                Since the ConML prototype deviates for the activation function for
                hidden layer neurons and the maximum number of epochs, `activation = "tanh"`
                and `max_iter = 500` have new default values. The default solver has been set
                to "lbfgs" in accordance with the prototype. Everything else will use
                Scikits default values unless overwritten. `hidden_layer_sizes` will also be
                ignored because of the way this wrapper works (see description above).
"""
mutable struct PrototypeClassifierANN<:BaseClassifier
    # hyperparameters
    rescale::Bool
    desiredrange::NTuple{2,Float64} # tuple of desired (min, max)
    hiddenLayerSizeFactor::Float64
    cv::Int
    noImprovementThreshold::Union{Nothing, Float64}
    verbose::Bool # note: verbose "shadows" the MLPkwargs[:verbose] parameter, which would need to be injected via MLPkwargs directly
    MLPkwargs::Dict{Symbol, Any}
    # learned parameters
    minmaxes # computed vector of (min, max) tupels, one for each feature
    storedmodel
    function PrototypeClassifierANN(; rescale::Bool = true, desiredrange::NTuple{2, Float64} = (-0.8, 0.8), hiddenLayerSizeFactor::Float64 = 1.5, cv::Int = 3, noImprovementThreshold::Union{Nothing, Float64} = 0., verbose::Bool = false, MLPkwargs = Dict(), kwargs...)
        model = new(rescale, desiredrange, hiddenLayerSizeFactor, cv, noImprovementThreshold, verbose, convert(Dict{Symbol, Any},MLPkwargs))
        # because of the convenience methods and because I use
        # ScikitLearnBase.@declare_hyperparameters, the MLPClassifier default overwrites need to happen here
        if !haskey(model.MLPkwargs, :activation) model.MLPkwargs[:activation] = "tanh" end
        if !haskey(model.MLPkwargs, :max_iter) model.MLPkwargs[:max_iter] = 500 end
        if !haskey(model.MLPkwargs, :solver) model.MLPkwargs[:solver] = "lbfgs" end
        # for convenience: merge kwargs into MLPkwargs
        foreach(kwargs) do pair
            model.MLPkwargs[pair.first] = pair.second
        end
        return model
    end
end

# this is not really optimal. the nested parameters should be hyperparameters, but alas!
# gotta fix this eventually.
ScikitLearnBase.@declare_hyperparameters(PrototypeClassifierANN, [:rescale, :desiredrange, :hiddenLayerSizeFactor, :cv, :verbose, :MLPkwargs])

ScikitLearnBase.is_classifier(::PrototypeClassifierANN) = true

function ScikitLearnBase.fit!(model::PrototypeClassifierANN, Xin, y)
    # X should be of size (n_sample, n_feature)
    # learn feature extrema
    model.minmaxes = [extrema(Xin[:,icol]) for icol in axes(Xin,2)]
    # found bug: this breaks if input data are integers and scaling is on,
    # as is the case for the conceptual domain for level 2 and higher models
    X = similar(Xin, float(eltype(Xin))) # cast to enclosing float type if not already a float
    if model.rescale
        # scale features if desired
        if model.verbose println("PrototypeClassifierANN: Rescaling input features") end
        for icol in axes(Xin, 2)
            # scaling value: (desired range)/(observed range)
            scaler = (model.desiredrange[2] - model.desiredrange[1]) / (model.minmaxes[icol][2] - model.minmaxes[icol][1])
            X[:,icol] .= (Xin[:,icol] .- model.minmaxes[icol][1]) .* scaler .+ model.desiredrange[1]
        end
    else
        X .= Xin
    end
    # try differently sized models
    bestsize = 0
    bestscore = -1.
    improved::Bool = true
    if model.verbose println("PrototypeClassifierANN: Trying out different MLP hidden layer sizes for $(size(X)[2]) features") end
    for k in 1:floor(Int, size(X)[2] * model.hiddenLayerSizeFactor ) # 1...(1.5*n_features) hidden layers
        if model.verbose println("PrototypeClassifierANN: $(k) hidden node$(ifelse(k==1,"","s"))") end
        model.MLPkwargs[:hidden_layer_sizes] = (k,)
        if model.verbose println("PrototypeClassifierANN: $(model.cv)-fold cross-valiation scores:") end
        scores = [-Inf,] # default if anything explodes
        try
            scores = cross_val_score(MLPClassifier(; model.MLPkwargs...), X, y; cv=model.cv, error_score = "raise")
        catch e
            if model.verbose print("MLP training failed, setting accuracy to -INF. Error:\n\t$(e)\n") end
        end
        if model.verbose println(scores) end
        # we just take the average score for comparison here
        newaverage = mean(scores)
        # stop early if noImprovementThreshold set and no improvement happened 2 times
        if !isnothing(model.noImprovementThreshold)
            if newaverage ≥ bestscore + model.noImprovementThreshold
                improved = true
            else
                if improved
                    improved = false
                else
                    if model.verbose print("No improvement detected for 2 runs, stopping hidden layer expansion.") end
                    if newaverage>bestscore
                        bestsize = k
                        bestscore = newaverage
                    end
                    break
                end
            end
        end
        if newaverage>bestscore
            # set new bests
            bestsize = k
            bestscore = newaverage
        end
    end
    if bestsize == 0 error("Fitting PrototypeClassifierANN failed.") end
    if model.verbose println("PrototypeClassifierANN: the best setup used $(bestsize) hidden nodes for an average accuracy of $(bestscore)") end
    model.MLPkwargs[:hidden_layer_sizes] = (bestsize,)
    model.storedmodel = fit!(MLPClassifier(; model.MLPkwargs...), X, y)
    return model
end

function ScikitLearnBase.predict(model::PrototypeClassifierANN, Xin)
    if model.rescale
        # scale features if desired
        # for prediction, make sure that the number of input features is correct
        if size(Xin)[2] != length(model.minmaxes)
            error("Shape of input ($(size(Xin)[2]) features) does not match the previously learned input shape ($(length(model.minmaxes)) features)")
        end
        X = similar(Xin, float(eltype(Xin))) # cast to enclosing float type if not already a float, same reason as in fit!
        for icol in axes(Xin, 2)
            # scaling value: (desired range)/(observed range)
            scaler = (model.desiredrange[2] - model.desiredrange[1]) / (model.minmaxes[icol][2] - model.minmaxes[icol][1])
            X[:,icol] .= (Xin[:,icol] .- model.minmaxes[icol][1]) .* scaler .+model.desiredrange[1]
        end
    else
        X = Xin
    end
    # just delegate to the stored model
    return predict(model.storedmodel, X)
end

# end of KNN-wrapper
