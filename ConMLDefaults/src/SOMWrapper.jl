#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConMLDefaults.jl and
# may not behave as expected when used on it's own

# SOM Wrapper
#TODO docstring/explain
"""
    SOM1D(; k = 5, initialcodebook = "random", kerneltype = 0, maptype = "planar",
            compactsupport = true, neighborhood = "gaussian", std_coeff = 0.5,
            random_state = nothing, verbose = 0)

# Parameters

- `k::Int = 5`: Number of neurons.
- `pca_init::Bool = false`: If `true`, the codebook is initialized from the first subspace spanned by the first two eigenvectors of the correlation matrix.
- `kerneltype::Int = 0`: Specify which kernel to use. If `0` use dense CPU kernel for `1` use dense GPU kernel if compiled with it.
- `ring:Bool = false`: Specify the map topology. If `true` use a toroidal map (ring in 1D), else use a planar map.
- `compactsupport::Bool = true`: Cut off map updates beyond the training radius with the Gaussian neighborhood.
- `gaussian_neighborhood::Bool = true`: If `true` use Gaussian neighborhood, else use bubble neighborhood function.
- `std_coeff::Float64 = 0.5`: Set the coefficient in the Gaussian neighborhood `exp(-||x-y||^2/(2*(coeff*radius)^2))`.
- `random_state::Union{Nothing,Int} = nothing`: Control the randomization of the algorithm by specifying the codebook initalization. It is ignored when `initialcodebook` is not `nothing`.
    - If int, `random_state` is the seed used by the random number generator.
    - If `nothing`, the random number generator is the `RandomState` instance used by `np.random`.
- `verbose::Int = 0`: Specify verbosity level (0, 1, or 2).

"""
struct SOM1D<:BaseClassifier
    k::Int # merges n_columns and n_rows
    pca_init::Bool # wraps initialcodebook
    kerneltype::Int
    ring::Bool # wraps maptype
    # gridtype is pointless in 1D and thus not considered
    compactsupport::Bool
    gaussian_neighborhood::Bool # wrapps neighborhood
    std_coeff::Float64
    random_state::Union{Nothing,Int}
    verbose::Int
    # model starts uninitialized
    model::Ref{Any}
    function SOM1D(k, pca_init, kerneltype, ring, compactsupport, gaussian_neighborhood,
                    std_coeff, random_state, verbose)
        return new(k, pca_init, kerneltype, ring,
                    compactsupport, gaussian_neighborhood, std_coeff, random_state, verbose,
                    Ref(0))
    end
end
SOM1D(; k = 5, pca_init = false, kerneltype = 0, ring = false, compactsupport = true,
        gaussian_neighborhood = true, std_coeff = 0.5, random_state = nothing,
        verbose = 0) = SOM1D(   k, pca_init, kerneltype, ring, compactsupport,
                                gaussian_neighborhood, std_coeff, random_state, verbose)

ScikitLearnBase.@declare_hyperparameters(SOM1D, [:k, :pca_init, :kerneltype, :ring, :compactsupport, :gaussian_neighborhood, :std_coeff, :random_state, :verbose])

function check_parameters(som::SOM1D)
    @assert som.k > 0
    @assert som.kerneltype ∈ (0,1)
    @assert som.verbose ∈ (0,1,2)
end

# fitting is unnecessary here, since this SOM implementation only supports fit_predict
ScikitLearnBase.fit!(som::SOM1D, X, Y=nothing) = return som
ScikitLearnBase.fit_transform!(som::SOM1D, X) = ScikitLearnBase.transform(som, X)
ScikitLearnBase.fit_predict!(som::SOM1D, X) = ScikitLearnBase.predict(som, X)

    # predict returns classes, transform returns the same in one-hot encoded space
function ScikitLearnBase.predict(som::SOM1D, X)
    # some parameter validation
    check_parameters(som)
    som.model[] = pySOM.SOM(;   n_columns = som.k,
                        n_rows = 1,
                        initialcodebook = som.pca_init ? "pca" : nothing,
                        kerneltype = som.kerneltype,
                        maptype = som.ring ? "toroid" : "planar",
                        compactsupport = som.compactsupport,
                        neighborhood = som.gaussian_neighborhood ? "gaussian" : "bubble",
                        std_coeff = som.std_coeff,
                        random_state = som.random_state,
                        verbose = som.verbose)
    # to match behavior of other transformers, return class predictions "one-hot encoded"
    classes = som.model[].fit_predict(X)
    return classes
end

function ScikitLearnBase.transform(som::SOM1D, X)
    classes = ScikitLearnBase.predict(som, X)
    # we could write an own function to do this, but we can also use the Dict encoder from
    # ScikitLearn to reflect the choosen number of classes correctly
    de = ScikitLearn.Preprocessing.DictEncoder()
    ScikitLearn.Preprocessing.fit!(de, reshape(collect(0:(som.k-1)),(:,1)))
    ret = ScikitLearn.Preprocessing.transform(de, reshape(classes,(:,1)))
    return ret
end

# end of SOM Wrapper
