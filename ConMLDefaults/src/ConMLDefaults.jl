#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#

module ConMLDefaults

# using SOM # this package was considered but dropped, because it is semi-abandoned and comes with a lot of unnecessary dependencies
using PyCall
using Conda
using RCall
using Statistics
using ScikitLearnBase
using ScikitLearn
using Flux
using ConML
using Random: shuffle

import Base: getindex
import ScikitLearnBase: predict, clone, fit!, transform, fit_transform!

# unconditional exports
export PrototypeClassifierANN, PrototypeRegressorANN,
        Select_reliability_mRMR, function_of_n, proportional_n, fixed_n, dimensionality_options,
        DefaultFeatureSelector, ClustOfVarKMeans, PrototypeAutoencoderQC

# see if SOM and Scikit-feature were properly installed
const deps_file = joinpath(dirname(@__FILE__), "..", "deps", "deps.jl")
# these lines and the deps-file idea are from Conda.jl
if isfile(deps_file)
    include(deps_file)
    # defines:  SCIKIT_FEATURE_INSTALLED
    #           SCIKIT_DIMENSION_INSTALLED
    #           SOM_LEARN_INSTALLED
else
    error("""
    ConMLDefaults not properly configured. Try running `Pkg.build(\"ConMLDefaults\")`
    and try to import this package again. If that doesn't help, file an issue please.
    """)
end

# wrappers for Python must be created at load-time not at compile-time
# Scikit-Learn
const RandomForestClassifier = PyNULL()
const RandomForestRegressor = PyNULL()
const MLPClassifier = PyNULL()
const MLPRegressor = PyNULL()
const mutual_info_classif = PyNULL()
const mutual_info_regression = PyNULL()
const cross_val_score = PyNULL()

# Som-Learn
if SOM_LEARN_INSTALLED
    const pySOM = PyNULL()
    export SOM1D
end

# Scikit-Dimension
if SCIKIT_DIMENSION_INSTALLED
    const ID = PyNULL()
    export ID
end

# Scikit-Feature
if SCIKIT_FEATURE_INSTALLED
    const skf_ndfs = PyNULL()
    const skf_udfs = PyNULL()
    const skf_spec = PyNULL()
    const skf_ls = PyNULL()
    const skf_construct_W = PyNULL()
    const skf_cfs = PyNULL()
    export Select_NDFS, Select_UDFS, Select_SPEC, Select_LS, PrototypeFeatureSelector
end

function __init__() # Mandatory as long as python wrappers are used in the standards
    try
        _clustofvar = rimport("ClustOfVar")
    catch e
        # copied from https://github.com/JuliaInterop/RCall.jl/pull/342
        # non-merged fix for hanging R install.packages from Julia
        function install_packages(pkg, repos = "https://cran.rstudio.com")
            run(`R -e "install.packages('$pkg', repos = '$repos')"`)
        end

        @info "Trying to install R package ClustOfVar..."
        install_packages("ClustOfVar")
        _clustofvar = rimport("ClustOfVar")
    finally
        try
            @eval const ClustOfVar = rimport("ClustOfVar")
            include(joinpath(dirname(@__FILE__),"ClustOfVarRwrapper.jl"))
        catch e
            @warn """
                Couldn't import ClustOfVar package from R via RCall.
                The associated functions will not be available in this session. Error:
                """ e
            @eval const ClustOfVarKMeans = error("""
                Couldn't import ClustOfVar package from R via RCall.
                Try to install ClustOfVar in the associated R environment and restart.
                """)
        end
    end

    @static if SCIKIT_FEATURE_INSTALLED
        copy!(skf_ndfs, pyimport("skfeature.function.sparse_learning_based.NDFS").ndfs)
        copy!(skf_udfs, pyimport("skfeature.function.sparse_learning_based.UDFS").udfs)
        copy!(skf_spec, pyimport("skfeature.function.similarity_based.SPEC").spec)
        copy!(skf_ls, pyimport("skfeature.function.similarity_based.lap_score").lap_score)
        copy!(skf_construct_W, pyimport("skfeature.utility.construct_W").construct_W)
        copy!(skf_cfs, pyimport("skfeature.function.statistical_based.CFS").cfs)
    else
        @warn """
            No Scikit-Feature installation found. Functions relying on this library will not be available
            on your machine. You can try to rerun the build script or install the necessary library
            manually in the conda env used by Julia (and rerun the build script afterwards.)
            """
    end

    @static if SCIKIT_DIMENSION_INSTALLED
        copy!(ID, pyimport("skdim").id)
    else
        @warn """
            No Scikit-Dimension installation found. Functions relying on this library will not be available
            on your machine. You can try to rerun the build script or install the necessary library
            manually in the conda env used by Julia (and rerun the build script afterwards.)
            """
    end

    @static if SOM_LEARN_INSTALLED
        copy!(pySOM, pyimport("somlearn"))
    else
        @warn """
            No SOM-Learn installation found. Functions relying on this library will not be available
            on your machine. You can try to rerun the build script or install the necessary library
            manually in the conda env used by Julia (and rerun the build script afterwards.)
            """
    end

    # Sciki-Learn
    copy!(RandomForestClassifier, pyimport("sklearn.ensemble").RandomForestClassifier)
    copy!(RandomForestRegressor, pyimport("sklearn.ensemble").RandomForestRegressor)
    copy!(MLPClassifier, pyimport("sklearn.neural_network").MLPClassifier)
    copy!(MLPRegressor, pyimport("sklearn.neural_network").MLPRegressor)
    copy!(mutual_info_classif, pyimport("sklearn.feature_selection").mutual_info_classif)
    copy!(mutual_info_regression, pyimport("sklearn.feature_selection").mutual_info_regression)
    copy!(cross_val_score, pyimport("sklearn.model_selection").cross_val_score)
end

appropriate_mutual_information(par::ConML.ParametersConML, x, y; kwargs...) = appropriate_mutual_information(par.learningDomain, x, y; kwargs...)
appropriate_mutual_information(::ConML.Procedural, x, y; kwargs...) = mutual_info_regression(x, y, kwargs...)
appropriate_mutual_information(::ConML.Conceptual, x, y; kwargs...) = mutual_info_classif(x, y, kwargs...)

include("helpers.jl")
include("ANNClassifier.jl")
include("ANNRegressor.jl")
include("FeatureSelectorDefaults.jl")
include("ReconstructionWinnerSelectors.jl")
include("autoencoder.jl")
include("autoencoderQualityControl.jl")
if SOM_LEARN_INSTALLED
    include("SOMWrapper.jl")
end
end # module
