#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Florian Große
# Licence: GNU GPLv3
#
# this file is meant to be included by ConMLDefaults.jl and
# may not behave as expected when used on it's own

# funny little helpers: upper triangular matrix stored as a vector
mutable struct _uppertriagmatvector{T} <: AbstractMatrix{T}
    n::Int
    vals::AbstractVector
    function _uppertriagmatvector(vals::AbstractVector{T}) where {T}
        n = ceil(Int, sqrt(2*length(vals)))
        n*(n-1)÷2 == length(vals) || error("Wrong length, vals must fit into a square matrix upper triangle.")
        return new{T}(n,vals)
    end
    function _uppertriagmatvector{T}(::UndefInitializer, n::Int) where {T}
        n<0 && throw(ArgumentError("Desired number of elements must be non-negative, got $(n)"))
        return new(n, Vector{T}(undef, n*(n-1)÷2))
    end
end

Base.eltype(v::_uppertriagmatvector{T}) where {T} = T
Base.size(v::_uppertriagmatvector) = (v.n,v.n)
Base.length(v::_uppertriagmatvector) = length(v.vals)

Base.getindex(mat::_uppertriagmatvector, i::Int) = getindex(mat.vals,i)
function Base.getindex(mat::_uppertriagmatvector, i::Int, j::Int)
    i==j && return zero(eltype(mat))
    ind = j>i ? _uppercarttolinindex(i, j, mat.n) : _uppercarttolinindex(j, i, mat.n)
    return mat.vals[ind]
end

Base.setindex!(mat::_uppertriagmatvector, v, i)= setindex!(mat.vals,v,i)
function Base.setindex!(mat::_uppertriagmatvector{T}, v::T, i::Int, j::Int) where {T}
    i==j && error("Diagonal is fixed to 0s")
    ind = _uppercarttolinindex(i, j, mat.n)
    setindex!(mat.vals, v, ind)
end

function _uppercarttolinindex(i::Int, j::Int, n::Int)
    i==j && return 0
    return j>i ? __uppercarttolinindex(i, j, n) : __uppercarttolinindex(j, i, n)
end
__uppercarttolinindex(i::Int, j::Int, n::Int) = ((i-1)*n)-(i*(i+1)÷2)+j

function _upperlintocartindex(ind::Int, n::Int)
    ind < 0 && error("Negative indices not supported.")
    ind == 0 && return (1,1)
    ind > n*(n-1)÷2 && error("Index $(ind) out of range for $(n)×$(n) _uppertriagmatvector")
    for i in 1:n
        colstart = ((i-1)*n)-(i*(i+1)÷2)
        j = ind - colstart
        if i<j<=n
            return (i,j)
        end
    end
end

function dropcol!(mat::_uppertriagmatvector, colind::Int)
    inds = [_uppercarttolinindex(i,colind,mat.n) for i in 1:mat.n if i!=colind]
    deleteat!(mat.vals, inds)
    mat.n -= 1
    return mat
end

# end of _uppertriagmatvector
