# the build step ensures that scikit-feature, scikit-dimension and som-learn will be
# installed if possible, but that those can also be installed manually if necessary

# If `deps.jl` file is present, this will prompt
# the build script to ask conda for available versions
# if a version is found, then ..._INSTALLED will be set to true,
# otherwise, it will be installed if possible

# If no `deps.jl` file is found and no file `initialbuild_passed` exists either,
# it is assumed that this is the first build and search for installed packages runs normally.
# If `initialbuild_passed`, it is assumed that `deps.jl` was cleared by the user.
# This will signal to the script that something is broken and it will try to remove and
# then reinstall both packages

using Conda

# renew `deps.jl` file
function write_deps(skf_installed, skdim_installed, som_installed)
    content = """
    const SCIKIT_FEATURE_INSTALLED = $(skf_installed)
    const SCIKIT_DIMENSION_INSTALLED = $(skdim_installed)
    const SOM_LEARN_INSTALLED = $(som_installed)
    """
    open("deps.jl", write = true) do depsfile
        write(depsfile, content)
    end
end

const hasdeps = isfile("deps.jl")
const isinitialbuild = !isfile("initialbuild_passed")

## functions for querying the installation status of the packages
# scikit-feature
function has_skf(had_skf_before)
    try
        scikit_feature_version = Conda.version("scikit-feature")
        @info """
            Scikit-Feature already installed. Using this version.
            To force reinstallation, delete the ConMLDefaults/deps/deps.jl file.
            """
        @show scikit_feature_version
        return true
    catch
        # try again with pip, just to be sure
        try
            Conda.pip("show","skfeature")
            @info """
                Scikit-Feature already installed. Using this version.
                To force reinstallation, delete the ConMLDefaults/deps/deps.jl file.
                """
            return true
        catch
            if had_skf_before @info """
                Scikit-Feature was once installed but appears to be missing.
                Trying to reinstall it...
                """
            end
            return false
        end
    end
end

# scikit-dimension
function has_skdim(had_skdim_before)
    try
        scikit_dim_version = Conda.version("scikit-dimension")
        @info """
            Scikit-Dimension already installed. Using this version.
            To force reinstallation, delete the ConMLDefaults/deps/deps.jl file.
            """
        @show scikit_dim_version
        return true
    catch
        # try again with pip, just to be sure
        try
            Conda.pip("show","scikit-dimension")
            @info """
                Scikit-Dimension already installed. Using this version.
                To force reinstallation, delete the ConMLDefaults/deps/deps.jl file.
                """
            return true
        catch
            if had_skdim_before @info """
                Scikit-Dimension was once installed but appears to be missing.
                Trying to reinstall it...
                """
            end
            return false
        end
    end
end

# som-learn
function has_somlearn(had_somlearn_before)
    try
        som_learn_version = Conda.version("som-learn")
        @info """
            SOM-Learn already installed. Using this version.
            To force reinstallation, delete the ConMLDefaults/deps/deps.jl file.
            """
        @show som_learn_version
        return true
    catch
        # try again with pip, just to be sure
        try
            Conda.pip("show","som-learn")
            @info """
                SOM-Learn already installed. Using this version.
                To force reinstallation, delete the ConMLDefaults/deps/deps.jl file.
                """
            return true
        catch
            if had_somlearn_before @info """
                SOM-Learn was once installed but appears to be missing.
                Trying to reinstall it...
                """
            end
            return false
        end
    end
end

## install functions
function install_skf()
    @info "Trying to install Scikit-Dimension via PyPI"
    Conda.pip("install","git+https://github.com/jundongl/scikit-feature.git")
    # because those are not in the dependencies for some reason
    @info "Installing additional dependencies of scikit-feature"
    Conda.add("pandas")
    Conda.add("numba")
end

function install_skdim()
    @info "Trying to install scikit-dimension via PyPI"
    Conda.pip("install","scikit-dimension")
end

function install_somlearn()
    # somlearn installation is borked on windows
    # som-learn requires somoclu==1.7.5.0 but windows 10
    # works properly only with 1.7.5.1
    # because of this, we force pip to just ignore the requirements, lol
    if Sys.iswindows()
        @info "Trying to install som-learn. This is slightly tricky on Windows so bear with me for a moment!"
        # install once to get all dependencies
        Conda.pip("install --no-cache-dir","som-learn")
        # remove somoclu 1.7.5.0 ant som-learn but keep all other dependencies intact
        Conda.rm("somoclu")
        # install somoclu 1.7.5.1
        Conda.pip("install", "somoclu")
        # and finally inject som-learn while ignoring dependencies
        Conda.pip("install --no-deps", "som-learn")
    else
        # installation should work normally here
        @info "Trying to install som-learn via PyPI"
        Conda.pip("install","som-learn")
    end
end

## uninstall functions

function rm_skf()
    try
        Conda.rm("scikit-feature")
    catch
        try Conda.pip("uninstall", "skfeature") catch end
    end
end

function rm_skdim()
    try
        Conda.rm("scikit-dimension")
    catch
        try Conda.pip("uninstall", "scikit-dimension") catch end
    end
end

function rm_somlearn()
    try
        Conda.rm("som-learn")
    catch
        try Conda.pip("uninstall", "som-learn") catch end
    end
    # uninstall somoclu as well
    try
        Conda.rm("somoclu")
    catch
        try Conda.pip("uninstall", "somoclu") catch end
    end
end

## start of the script

# activate Conda pip compatibility
if !Conda.pip_interop()
    @info "Setting Conda pip_interop_enabled"
    Conda.pip_interop(true)
end

if hasdeps || isinitialbuild
    if hasdeps
        include("deps.jl")
    else
        global const SCIKIT_FEATURE_INSTALLED = false
        global const SCIKIT_DIMENSION_INSTALLED = false
        global const SOM_LEARN_INSTALLED = false
    end

    # Scikit-Feature
    skf_installed = has_skf(SCIKIT_FEATURE_INSTALLED)
    if !skf_installed
        try
            @info "Trying to install Scikit-Feature from source."
            install_skf()
            global skf_installed = true
        catch install_error
            @error """
                Failed to install Scikit-Feature.
                You may still try to install it manually from source and rerun `Pkg.build(ConMLDefaults)` afterwards.
                """ install_error
            global skf_installed = false
        end
    end

    # scikit-dimension
    skdim_installed = has_skdim(SCIKIT_DIMENSION_INSTALLED)
    if !skdim_installed
        # try installing it over pip
        try
            install_skdim()
            global skdim_installed = true
        catch install_error
            @error """
                Failed to install Scikit-Dimension.
                You may still try to install it manually from source and rerun `Pkg.build(ConMLDefaults)` afterwards.
                """ install_error
            global skdim_installed = false
        end
    end

    # SOM-Learn
    som_installed = has_somlearn(SOM_LEARN_INSTALLED)
    if !som_installed
        # try installing it over pip, because the conda channel didn't work as intended
        try
            install_somlearn()
            global som_installed = true
        catch install_error
            @error """
                Failed to install SOM-Learn.
                You may still try to install it manually from source and rerun `Pkg.build(ConMLDefaults)` afterwards.
                """ install_error
            global som_installed = false
        end
    end

    write_deps(skf_installed, skdim_installed, som_installed)

else # signal to reinstall all if possible
    @info "Trying to clean up old installations with conda and pip"
    rm_skf()
    rm_skdim()
    rm_somlearn()

    skf_installed = false
    skdim_installed = false
    som_installed = false

    try
        install_skf()
        global skf_installed = true
    catch install_error
        @error """
            Failed to install Scikit-Feature.
            You may still try to install it manually from source and rerun `Pkg.build(ConMLDefaults)` afterwards.
            """ install_error
        global skf_installed = false
    end

    try
        install_skdim()
        global skdim_installed = true
    catch install_error
        @error """
            Failed to install Scikit-Dimension.
            You may still try to install it manually from source and rerun `Pkg.build(ConMLDefaults)` afterwards.
            """ install_error
        global skdim_installed = false
    end

    try
        install_somlearn()
        global som_installed = true
    catch install_error
        @error """
            Failed to install SOM-Learn.
            You may still try to install it manually from source and rerun `Pkg.build(ConMLDefaults)` afterwards.
            """ install_error
        global som_installed = false
    end

    write_deps(skf_installed, skdim_installed, som_installed)
end

if isinitialbuild
    content = """
    # I signal that an initial attempt to find installed packages was made
    # If you delete the deps.jl file now and rebuild, both packages will be removed and reinstalled if possible
    # If you want to manually fix an installation, no need to remove me, just run build again afterwards
    """
    open("initialbuild_passed", write = true) do file
        write(file, content)
    end
end
